package de.elster.eric.wrapper

import net.dankito.tax.elster.EricFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Test
import java.io.File


class EricTest {

    private val ericFactory = EricFactory()

    protected val eric: Eric = ericFactory.createEric(File("testLogs").absoluteFile)


    @After
    fun tearDown() {
        ericFactory.releaseEric(eric)
    }


    /*  makeElsterSteuernummer() - all test data is taken from Pruefung_der_Steuer_und_Steueridentifikatsnummer.pdf p. 9 - 11   */

    @Test
    fun `makeElsterSteuernummer Baden-Württemberg`() {

        // when
        val result = eric.makeElsterSteuernummer("66815/08156", "2866")

        // then
        assertThat(result).isEqualTo("2866081508156")
    }

    @Test
    fun `makeElsterSteuernummer Bayern (München)`() {

        // when
        val result = eric.makeElsterSteuernummer("198/815/08152", "9198")

        // then
        assertThat(result).isEqualTo("9198081508152")
    }

    @Test
    fun `makeElsterSteuernummer Bayern (Nürnberg)`() {

        // when
        val result = eric.makeElsterSteuernummer("296/815/08153", "9296")

        // then
        assertThat(result).isEqualTo("9296081508153")
    }

    @Test
    fun `makeElsterSteuernummer Berlin`() {

        // when
        val result = eric.makeElsterSteuernummer("97/815/08154", "1197")

        // then
        assertThat(result).isEqualTo("1197081508154")
    }

    @Test
    fun `makeElsterSteuernummer Brandenburg`() {

        // when
        val result = eric.makeElsterSteuernummer("098/815/08157", "3098")

        // then
        assertThat(result).isEqualTo("3098081508157")
    }

    @Test
    fun `makeElsterSteuernummer Bremen`() {

        // when
        val result = eric.makeElsterSteuernummer("97 123 01233", "2497")

        // then
        assertThat(result).isEqualTo("2497012301233")
    }


    @Test
    fun `makeElsterSteuernummer Hamburg`() {

        // when
        val result = eric.makeElsterSteuernummer("41/815/08154", "2241")

        // then
        assertThat(result).isEqualTo("2241081508154")
    }

    @Test
    fun `makeElsterSteuernummer Hessen`() {

        // when
        val result = eric.makeElsterSteuernummer("053 815 08158", "2653")

        // then
        assertThat(result).isEqualTo("2653081508158")
    }

    @Test
    fun `makeElsterSteuernummer Mecklenburg-Vorpommern`() {

        // when
        val result = eric.makeElsterSteuernummer("098/815/08157", "4098")

        // then
        assertThat(result).isEqualTo("4098081508157")
    }

    @Test
    fun `makeElsterSteuernummer Niedersachsen`() {

        // when
        val result = eric.makeElsterSteuernummer("88/815/08158", "2388")

        // then
        assertThat(result).isEqualTo("2388081508158")
    }


    @Test
    fun `makeElsterSteuernummer NRW (54)`() {

        // when
        val result = eric.makeElsterSteuernummer("400/8150/8159", "5400")

        // then
        assertThat(result).isEqualTo("5400081508159")
    }

    @Test
    fun `makeElsterSteuernummer NRW (55)`() {

        // when
        val result = eric.makeElsterSteuernummer("500/8150/8151", "5500")

        // then
        assertThat(result).isEqualTo("5500081508151")
    }

    @Test
    fun `makeElsterSteuernummer NRW (56)`() {

        // when
        val result = eric.makeElsterSteuernummer("600/8150/8154", "5600")

        // then
        assertThat(result).isEqualTo("5600081508154")
    }

    @Test
    fun `makeElsterSteuernummer Rheinland-Pfalz`() {

        // when
        val result = eric.makeElsterSteuernummer("99/815/08152", "2799")

        // then
        assertThat(result).isEqualTo("2799081508152")
    }


    @Test
    fun `makeElsterSteuernummer Saarland`() {

        // when
        val result = eric.makeElsterSteuernummer("096/815/08187", "1096")

        // then
        assertThat(result).isEqualTo("1096081508187")
    }

    @Test
    fun `makeElsterSteuernummer Sachsen`() {

        // when
        val result = eric.makeElsterSteuernummer("248/815/08156", "3248")

        // then
        assertThat(result).isEqualTo("3248081508156")
    }

    @Test
    fun `makeElsterSteuernummer Sachsen-Anhalt`() {

        // when
        val result = eric.makeElsterSteuernummer("198/815/08152", "3198")

        // then
        assertThat(result).isEqualTo("3198081508152")
    }

    @Test
    fun `makeElsterSteuernummer Schleswig-Holstein`() {

        // when
        val result = eric.makeElsterSteuernummer("38/815/08154", "2138")

        // then
        assertThat(result).isEqualTo("2138081508154")
    }

    @Test
    fun `makeElsterSteuernummer Thüringen`() {

        // when
        val result = eric.makeElsterSteuernummer("198/815/08152", "4198")

        // then
        assertThat(result).isEqualTo("4198081508152")
    }

}