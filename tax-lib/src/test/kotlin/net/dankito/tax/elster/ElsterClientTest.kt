package net.dankito.tax.elster

import net.dankito.tax.elster.model.Finanzamt
import net.dankito.tax.elster.model.FinanzamtsNummerType
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Test


class ElsterClientTest {

    private val underTest = ElsterClient()

    @After
    fun tearDown() {
        underTest.close()
    }


    @Test
    fun getFinanzämter() {
        val finanzaemterNachBundesland = underTest.getFinanzämter()

        assertThat(finanzaemterNachBundesland).hasSizeGreaterThanOrEqualTo(16)

        finanzaemterNachBundesland.keys.forEach { bundesland ->
            assertThat(bundesland.name.length).isGreaterThanOrEqualTo(6)
            assertThat(bundesland.elsterFinanzamtLandId).isBetween(10, 100)
        }

        finanzaemterNachBundesland.values.forEach { finanzaemter ->
            assertThat(finanzaemter).hasSizeGreaterThanOrEqualTo(3)

            finanzaemter.forEach { finanzamt ->
                assertThat(finanzamt.name.length).isGreaterThanOrEqualTo(6)
                assertThat(finanzamt.finanzamtsnummer).isBetween(1000, 9999)
            }
        }
    }

    @Test
    fun `getFinanzämter - test FinanzamtsNummerType`() {
        val finanzaemterNachBundesland = underTest.getFinanzämter()

        finanzaemterNachBundesland.keys.forEach { bundesland ->
            when (bundesland.name) {
                "Baden-Württemberg", "Berlin", "Bremen", "Hamburg", "Niedersachsen", "Rheinland-Pfalz", "Schleswig-Holstein" ->
                    assertThat(bundesland.type).isEqualByComparingTo(FinanzamtsNummerType.TwoDigits)
                "Hessen" -> assertThat(bundesland.type).isEqualByComparingTo(FinanzamtsNummerType.TwoDigitsWithALeadingZero)
                "Bayern", "Brandenburg", "Mecklenburg-Vorpommern", "Nordrhein-Westfalen", "Saarland", "Sachsen", "Sachsen-Anhalt", "Thüringen" ->
                    assertThat(bundesland.type).isEqualByComparingTo(FinanzamtsNummerType.ThreeDigits)
            }

            if (bundesland.name.startsWith("Bayern") || bundesland.name.startsWith("Nordrhein-Westfalen")) {
                assertThat(bundesland.type).isEqualByComparingTo(FinanzamtsNummerType.ThreeDigits)
            }
        }
    }


    @Test
    fun getFinanzamtsdaten() {

        val taxOfficesByTaxOfficeDistricts = underTest.getFinanzämter()

        taxOfficesByTaxOfficeDistricts.forEach { taxOfficeDistrict, taxOffices ->
            taxOffices.forEach { taxOffice ->
                testFinanzamtsdaten(taxOffice)
            }
        }
    }

    private fun testFinanzamtsdaten(taxOffice: Finanzamt) {

        // when
        val result = underTest.getFinanzamtsdaten(taxOffice)

        // then
        assertThat(result).isNotNull()
        assertThat(result.finanzamtsnummer).isEqualTo(taxOffice.finanzamtsnummer)
        assertThat(result.name).isNotNull()
        assertThat(result.addresses).isNotEmpty()
        assertThat(result.contactList).isNotEmpty()

        result.remarks.forEach { remark ->
            assertThat(remark).isNotBlank()
        }

        result.mainBranch?.let { mainBranch ->
            assertThat(mainBranch.name).isNotBlank()
            assertThat(mainBranch.name).doesNotContain("Außenstelle v. ")
        }
    }

}