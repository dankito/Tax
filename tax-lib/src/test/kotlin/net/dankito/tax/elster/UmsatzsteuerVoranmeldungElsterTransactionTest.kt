package net.dankito.tax.elster

import de.elster.eric.wrapper.Eric
import net.dankito.tax.elster.model.Steuerjahr
import net.dankito.tax.elster.model.Steuerpflichtiger
import net.dankito.tax.elster.model.UmsatzsteuerVoranmeldung
import net.dankito.tax.elster.model.Voranmeldungszeitraum
import net.dankito.tax.elster.test.TestFinanzamt
import net.dankito.tax.elster.test.TestHerstellerID
import net.dankito.tax.elster.test.Teststeuernummern
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Test
import java.io.File

class UmsatzsteuerVoranmeldungElsterTransactionTest {

    companion object {

        private val Steuerpflichtiger = Steuerpflichtiger(
            "Bayerisches Landesamt für Steuern",
            null,
            null,
            "Katharina-von-Bora-Str",
            "6",
            "80335",
            "Muenchen",
            "089/0815 0815",
            "email@adresse.de"
        )

        private val Steuernummer = Teststeuernummern.T_198_113_10010.steuernummer
        private val Finanzamt = TestFinanzamt.Bayern_9198.finanzamt
        private val HerstellerID = TestHerstellerID.T_74931.herstellerID

        private val Jahr = Steuerjahr.Jahr2020
        private val Zeitraum = Voranmeldungszeitraum.Januar

        private val CertificateFile = File(UmsatzsteuerVoranmeldungElsterTransactionTest::class.java.classLoader
            .getResource("data/test-soft-pse.pfx").toURI())

        private const val CertificatePin = "123456"

        private val DataFolder = File("test_data")

    }


    private val ericFactory = EricFactory()

    private val eric: Eric = ericFactory.createEric(DataFolder)

    private val underTest = UmsatzsteuerVoranmeldungElsterTransaction(eric)


    @After
    fun tearDown() {
        ericFactory.releaseEric(eric)
    }


    @Test
    fun makeUmsatzsteuerVoranmeldung() {

        val data = UmsatzsteuerVoranmeldung(Jahr, Zeitraum, Finanzamt,
            Steuernummer, Steuerpflichtiger, CertificateFile, CertificatePin, 21951, 0,
            null, null,
            167.83, 4002.86, HerstellerID,
            File(DataFolder, "result.pdf"), File(DataFolder, "created-xml.xml"))

        val result = underTest.makeUmsatzsteuerVoranmeldung(data)

        assertThat(result.successful).isTrue()
    }

    @Test
    fun makeUmsatzsteuerVoranmeldungMitUmsatzsteuerZuAnderenSteuersätzen() {

        val data = UmsatzsteuerVoranmeldung(Jahr, Zeitraum, Finanzamt,
            Steuernummer, Steuerpflichtiger, CertificateFile, CertificatePin, 21951, 0,
            5_000, 800.0,
            167.83, 4002.86, HerstellerID,
            File(DataFolder, "result.pdf"), File(DataFolder, "created-xml.xml"))

        val result = underTest.makeUmsatzsteuerVoranmeldung(data)

        assertThat(result.successful).isTrue()
    }

}