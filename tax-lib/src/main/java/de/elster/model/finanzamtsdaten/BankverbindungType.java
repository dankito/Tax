
package de.elster.model.finanzamtsdaten;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Beschreibung einer Bankverbindung
 * 
 * <p>Java class for BankverbindungType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankverbindungType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IBAN" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="BIC" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="Finanzinstitut" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="Kontoinhaber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="Kontonummer" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="BLZ" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankverbindungType", propOrder = {
    "iban",
    "bic",
    "finanzinstitut",
    "kontoinhaber",
    "kontonummer",
    "blz"
})
public class BankverbindungType {

    @XmlElement(name = "IBAN", required = true)
    protected Object iban;
    @XmlElement(name = "BIC")
    protected Object bic;
    @XmlElement(name = "Finanzinstitut")
    protected Object finanzinstitut;
    @XmlElement(name = "Kontoinhaber")
    protected Object kontoinhaber;
    @XmlElement(name = "Kontonummer")
    protected Object kontonummer;
    @XmlElement(name = "BLZ")
    protected Object blz;

    /**
     * Gets the value of the iban property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIBAN() {
        return iban;
    }

    /**
     * Sets the value of the iban property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIBAN(Object value) {
        this.iban = value;
    }

    /**
     * Gets the value of the bic property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getBIC() {
        return bic;
    }

    /**
     * Sets the value of the bic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setBIC(Object value) {
        this.bic = value;
    }

    /**
     * Gets the value of the finanzinstitut property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getFinanzinstitut() {
        return finanzinstitut;
    }

    /**
     * Sets the value of the finanzinstitut property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setFinanzinstitut(Object value) {
        this.finanzinstitut = value;
    }

    /**
     * Gets the value of the kontoinhaber property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getKontoinhaber() {
        return kontoinhaber;
    }

    /**
     * Sets the value of the kontoinhaber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setKontoinhaber(Object value) {
        this.kontoinhaber = value;
    }

    /**
     * Gets the value of the kontonummer property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getKontonummer() {
        return kontonummer;
    }

    /**
     * Sets the value of the kontonummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setKontonummer(Object value) {
        this.kontonummer = value;
    }

    /**
     * Gets the value of the blz property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getBLZ() {
        return blz;
    }

    /**
     * Sets the value of the blz property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setBLZ(Object value) {
        this.blz = value;
    }

}
