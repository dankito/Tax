
package de.elster.model.finanzamtsdaten;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Die Adresse eines Finanzamts (z.B. eine Grosskundenadresse)
 * 
 * <p>Java class for AdresseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdresseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PLZ" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="Ort" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="Postfachnummer" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="Strasse" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="typ">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="Großkundenadresse"/>
 *             &lt;enumeration value="Hausanschrift"/>
 *             &lt;enumeration value="Postfachadresse"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdresseType", propOrder = {
    "plz",
    "ort",
    "postfachnummer",
    "strasse"
})
public class AdresseType {

    @XmlElement(name = "PLZ", required = true)
    protected String plz;
    @XmlElement(name = "Ort", required = true)
    protected String ort;
    @XmlElement(name = "Postfachnummer")
    protected String postfachnummer;
    @XmlElement(name = "Strasse")
    protected String strasse;
    @XmlAttribute(name = "typ")
    protected String typ;

    /**
     * Gets the value of the plz property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public String getPLZ() {
        return plz;
    }

    /**
     * Sets the value of the plz property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setPLZ(String value) {
        this.plz = value;
    }

    /**
     * Gets the value of the ort property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public String getOrt() {
        return ort;
    }

    /**
     * Sets the value of the ort property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setOrt(String value) {
        this.ort = value;
    }

    /**
     * Gets the value of the postfachnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public String getPostfachnummer() {
        return postfachnummer;
    }

    /**
     * Sets the value of the postfachnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setPostfachnummer(String value) {
        this.postfachnummer = value;
    }

    /**
     * Gets the value of the strasse property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public String getStrasse() {
        return strasse;
    }

    /**
     * Sets the value of the strasse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setStrasse(String value) {
        this.strasse = value;
    }

    /**
     * Gets the value of the typ property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTyp() {
        return typ;
    }

    /**
     * Sets the value of the typ property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTyp(String value) {
        this.typ = value;
    }

}
