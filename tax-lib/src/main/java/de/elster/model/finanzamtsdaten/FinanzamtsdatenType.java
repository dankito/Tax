
package de.elster.model.finanzamtsdaten;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Definiiert die Informationen zu einem Finanzamt
 * 
 * <p>Java class for FinanzamtsdatenType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinanzamtsdatenType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BuFaNr" type="{http://www.elster.de/EricXML/1.0/EricHoleFinanzamtsdaten}BuFaNummerTyp"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="AdresseListe" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Adresse" type="{http://www.elster.de/EricXML/1.0/EricHoleFinanzamtsdaten}AdresseType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="KontaktListe" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Kontakt" type="{http://www.elster.de/EricXML/1.0/EricHoleFinanzamtsdaten}KontaktType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BankverbindungListe" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Bankverbindung" type="{http://www.elster.de/EricXML/1.0/EricHoleFinanzamtsdaten}BankverbindungType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OeffnungszeitListe" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Oeffnungszeit" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BemerkungListe" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Bemerkung" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Hauptstelle" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="BuFaNr" type="{http://www.elster.de/EricXML/1.0/EricHoleFinanzamtsdaten}BuFaNummerTyp"/>
 *                   &lt;element name="Name">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;pattern value="Außenstelle v\. .*"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Aussenstellen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinanzamtsdatenType", propOrder = {
    "buFaNr",
    "name",
    "adresseListe",
    "kontaktListe",
    "bankverbindungListe",
    "oeffnungszeitListe",
    "bemerkungListe",
    "hauptstelle",
    "aussenstellen"
})
public class FinanzamtsdatenType {

    @XmlElement(name = "BuFaNr", required = true)
    protected String buFaNr;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "AdresseListe")
    protected FinanzamtsdatenType.AdresseListe adresseListe;
    @XmlElement(name = "KontaktListe")
    protected FinanzamtsdatenType.KontaktListe kontaktListe;
    @XmlElement(name = "BankverbindungListe")
    protected FinanzamtsdatenType.BankverbindungListe bankverbindungListe;
    @XmlElement(name = "OeffnungszeitListe")
    protected FinanzamtsdatenType.OeffnungszeitListe oeffnungszeitListe;
    @XmlElement(name = "BemerkungListe")
    protected FinanzamtsdatenType.BemerkungListe bemerkungListe;
    @XmlElement(name = "Hauptstelle")
    protected FinanzamtsdatenType.Hauptstelle hauptstelle;
    @XmlElement(name = "Aussenstellen")
    protected String aussenstellen;

    /**
     * Gets the value of the buFaNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuFaNr() {
        return buFaNr;
    }

    /**
     * Sets the value of the buFaNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuFaNr(String value) {
        this.buFaNr = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the adresseListe property.
     * 
     * @return
     *     possible object is
     *     {@link FinanzamtsdatenType.AdresseListe }
     *     
     */
    public FinanzamtsdatenType.AdresseListe getAdresseListe() {
        return adresseListe;
    }

    /**
     * Sets the value of the adresseListe property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinanzamtsdatenType.AdresseListe }
     *     
     */
    public void setAdresseListe(FinanzamtsdatenType.AdresseListe value) {
        this.adresseListe = value;
    }

    /**
     * Gets the value of the kontaktListe property.
     * 
     * @return
     *     possible object is
     *     {@link FinanzamtsdatenType.KontaktListe }
     *     
     */
    public FinanzamtsdatenType.KontaktListe getKontaktListe() {
        return kontaktListe;
    }

    /**
     * Sets the value of the kontaktListe property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinanzamtsdatenType.KontaktListe }
     *     
     */
    public void setKontaktListe(FinanzamtsdatenType.KontaktListe value) {
        this.kontaktListe = value;
    }

    /**
     * Gets the value of the bankverbindungListe property.
     * 
     * @return
     *     possible object is
     *     {@link FinanzamtsdatenType.BankverbindungListe }
     *     
     */
    public FinanzamtsdatenType.BankverbindungListe getBankverbindungListe() {
        return bankverbindungListe;
    }

    /**
     * Sets the value of the bankverbindungListe property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinanzamtsdatenType.BankverbindungListe }
     *     
     */
    public void setBankverbindungListe(FinanzamtsdatenType.BankverbindungListe value) {
        this.bankverbindungListe = value;
    }

    /**
     * Gets the value of the oeffnungszeitListe property.
     * 
     * @return
     *     possible object is
     *     {@link FinanzamtsdatenType.OeffnungszeitListe }
     *     
     */
    public FinanzamtsdatenType.OeffnungszeitListe getOeffnungszeitListe() {
        return oeffnungszeitListe;
    }

    /**
     * Sets the value of the oeffnungszeitListe property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinanzamtsdatenType.OeffnungszeitListe }
     *     
     */
    public void setOeffnungszeitListe(FinanzamtsdatenType.OeffnungszeitListe value) {
        this.oeffnungszeitListe = value;
    }

    /**
     * Gets the value of the bemerkungListe property.
     * 
     * @return
     *     possible object is
     *     {@link FinanzamtsdatenType.BemerkungListe }
     *     
     */
    public FinanzamtsdatenType.BemerkungListe getBemerkungListe() {
        return bemerkungListe;
    }

    /**
     * Sets the value of the bemerkungListe property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinanzamtsdatenType.BemerkungListe }
     *     
     */
    public void setBemerkungListe(FinanzamtsdatenType.BemerkungListe value) {
        this.bemerkungListe = value;
    }

    /**
     * Gets the value of the hauptstelle property.
     * 
     * @return
     *     possible object is
     *     {@link FinanzamtsdatenType.Hauptstelle }
     *     
     */
    public FinanzamtsdatenType.Hauptstelle getHauptstelle() {
        return hauptstelle;
    }

    /**
     * Sets the value of the hauptstelle property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinanzamtsdatenType.Hauptstelle }
     *     
     */
    public void setHauptstelle(FinanzamtsdatenType.Hauptstelle value) {
        this.hauptstelle = value;
    }

    /**
     * Gets the value of the aussenstellen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAussenstellen() {
        return aussenstellen;
    }

    /**
     * Sets the value of the aussenstellen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAussenstellen(String value) {
        this.aussenstellen = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Adresse" type="{http://www.elster.de/EricXML/1.0/EricHoleFinanzamtsdaten}AdresseType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "adresse"
    })
    public static class AdresseListe {

        @XmlElement(name = "Adresse", required = true)
        protected List<AdresseType> adresse;

        /**
         * Gets the value of the adresse property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the adresse property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAdresse().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AdresseType }
         * 
         * 
         */
        public List<AdresseType> getAdresse() {
            if (adresse == null) {
                adresse = new ArrayList<AdresseType>();
            }
            return this.adresse;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Bankverbindung" type="{http://www.elster.de/EricXML/1.0/EricHoleFinanzamtsdaten}BankverbindungType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bankverbindung"
    })
    public static class BankverbindungListe {

        @XmlElement(name = "Bankverbindung", required = true)
        protected List<BankverbindungType> bankverbindung;

        /**
         * Gets the value of the bankverbindung property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the bankverbindung property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBankverbindung().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BankverbindungType }
         * 
         * 
         */
        public List<BankverbindungType> getBankverbindung() {
            if (bankverbindung == null) {
                bankverbindung = new ArrayList<BankverbindungType>();
            }
            return this.bankverbindung;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Bemerkung" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bemerkung"
    })
    public static class BemerkungListe {

        @XmlElement(name = "Bemerkung", required = true)
        protected List<String> bemerkung;

        /**
         * Gets the value of the bemerkung property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the bemerkung property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBemerkung().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getBemerkung() {
            if (bemerkung == null) {
                bemerkung = new ArrayList<String>();
            }
            return this.bemerkung;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="BuFaNr" type="{http://www.elster.de/EricXML/1.0/EricHoleFinanzamtsdaten}BuFaNummerTyp"/>
     *         &lt;element name="Name">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;pattern value="Außenstelle v\. .*"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "buFaNr",
        "name"
    })
    public static class Hauptstelle {

        @XmlElement(name = "BuFaNr", required = true)
        protected String buFaNr;
        @XmlElement(name = "Name", required = true)
        protected String name;

        /**
         * Gets the value of the buFaNr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBuFaNr() {
            return buFaNr;
        }

        /**
         * Sets the value of the buFaNr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBuFaNr(String value) {
            this.buFaNr = value;
        }

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Kontakt" type="{http://www.elster.de/EricXML/1.0/EricHoleFinanzamtsdaten}KontaktType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "kontakt"
    })
    public static class KontaktListe {

        @XmlElement(name = "Kontakt", required = true)
        protected List<KontaktType> kontakt;

        /**
         * Gets the value of the kontakt property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the kontakt property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getKontakt().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link KontaktType }
         * 
         * 
         */
        public List<KontaktType> getKontakt() {
            if (kontakt == null) {
                kontakt = new ArrayList<KontaktType>();
            }
            return this.kontakt;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Oeffnungszeit" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "oeffnungszeit"
    })
    public static class OeffnungszeitListe {

        @XmlElement(name = "Oeffnungszeit", required = true)
        protected List<String> oeffnungszeit;

        /**
         * Gets the value of the oeffnungszeit property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the oeffnungszeit property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getOeffnungszeit().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getOeffnungszeit() {
            if (oeffnungszeit == null) {
                oeffnungszeit = new ArrayList<String>();
            }
            return this.oeffnungszeit;
        }

    }

}
