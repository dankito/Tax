
package de.elster.model.finanzamtsdaten;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Ein Kontakt kann eine Telefonnummer, eine Faxnummer, E-Mail usw. sein
 * 
 * <p>Java class for KontaktType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KontaktType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Bezeichnung" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="Inhalt" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KontaktType", propOrder = {
    "bezeichnung",
    "inhalt"
})
public class KontaktType {

    @XmlElement(name = "Bezeichnung", required = true)
    protected String bezeichnung;
    @XmlElement(name = "Inhalt", required = true)
    protected String inhalt;

    /**
     * Gets the value of the bezeichnung property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Sets the value of the bezeichnung property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setBezeichnung(String value) {
        this.bezeichnung = value;
    }

    /**
     * Gets the value of the inhalt property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public String getInhalt() {
        return inhalt;
    }

    /**
     * Sets the value of the inhalt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setInhalt(String value) {
        this.inhalt = value;
    }

}
