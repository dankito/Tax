
package de.elster.model.finanzamtsdaten;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Finanzamtsdaten" type="{http://www.elster.de/EricXML/1.0/EricHoleFinanzamtsdaten}FinanzamtsdatenType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="version" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="1" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "finanzamtsdaten"
})
@XmlRootElement(name = "EricHoleFinanzamtsdaten")
public class EricHoleFinanzamtsdaten {

    @XmlElement(name = "Finanzamtsdaten", required = true)
    protected FinanzamtsdatenType finanzamtsdaten;
    @XmlAttribute(name = "version", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String version;

    /**
     * Gets the value of the finanzamtsdaten property.
     * 
     * @return
     *     possible object is
     *     {@link FinanzamtsdatenType }
     *     
     */
    public FinanzamtsdatenType getFinanzamtsdaten() {
        return finanzamtsdaten;
    }

    /**
     * Sets the value of the finanzamtsdaten property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinanzamtsdatenType }
     *     
     */
    public void setFinanzamtsdaten(FinanzamtsdatenType value) {
        this.finanzamtsdaten = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        if (version == null) {
            return "1";
        } else {
            return version;
        }
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

}
