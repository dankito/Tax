
package de.elster.model.bearbeitevorgang;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				Struktur die im Erfolgsfall zurückgegeben wird bestehend aus Telenummer und
 * 				Ordnungsbegriff.
 * 			
 * 
 * <p>Java class for ErfolgTyp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ErfolgTyp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Telenummer" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}TelenummerTyp" minOccurs="0"/>
 *         &lt;element name="Ordnungsbegriff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErfolgTyp", propOrder = {
    "telenummer",
    "ordnungsbegriff"
})
public class ErfolgTyp {

    @XmlElement(name = "Telenummer")
    protected String telenummer;
    @XmlElement(name = "Ordnungsbegriff")
    protected String ordnungsbegriff;

    /**
     * Gets the value of the telenummer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelenummer() {
        return telenummer;
    }

    /**
     * Sets the value of the telenummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelenummer(String value) {
        this.telenummer = value;
    }

    /**
     * Gets the value of the ordnungsbegriff property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdnungsbegriff() {
        return ordnungsbegriff;
    }

    /**
     * Sets the value of the ordnungsbegriff property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdnungsbegriff(String value) {
        this.ordnungsbegriff = value;
    }

}
