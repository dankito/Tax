
package de.elster.model.bearbeitevorgang;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Erfolg" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}ErfolgTyp"/>
 *         &lt;choice maxOccurs="unbounded">
 *           &lt;element name="FehlerRegelpruefung" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}FehlerRegelpruefungTyp"/>
 *           &lt;element name="Hinweis" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}HinweisTyp"/>
 *         &lt;/choice>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "erfolg",
    "fehlerRegelpruefungOrHinweis"
})
@XmlRootElement(name = "EricBearbeiteVorgang")
public class EricBearbeiteVorgang {

    @XmlElement(name = "Erfolg")
    protected ErfolgTyp erfolg;

    @XmlElements({
        @XmlElement(name = "FehlerRegelpruefung", type = FehlerRegelpruefungTyp.class),
        @XmlElement(name = "Hinweis", type = HinweisTyp.class)
    })
    protected List<HinweisOderFehlerBase> fehlerRegelpruefungOrHinweis;

    /**
     * Gets the value of the erfolg property.
     * 
     * @return
     *     possible object is
     *     {@link ErfolgTyp }
     *     
     */
    public ErfolgTyp getErfolg() {
        return erfolg;
    }

    /**
     * Sets the value of the erfolg property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErfolgTyp }
     *     
     */
    public void setErfolg(ErfolgTyp value) {
        this.erfolg = value;
    }

    /**
     * Gets the value of the fehlerRegelpruefungOrHinweis property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fehlerRegelpruefungOrHinweis property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFehlerRegelpruefungOrHinweis().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FehlerRegelpruefungTyp }
     * {@link HinweisTyp }
     * 
     * 
     */
    public List<HinweisOderFehlerBase> getFehlerRegelpruefungOrHinweis() {
        if (fehlerRegelpruefungOrHinweis == null) {
            fehlerRegelpruefungOrHinweis = new ArrayList<>();
        }
        return this.fehlerRegelpruefungOrHinweis;
    }

}
