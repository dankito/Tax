package de.elster.model.bearbeitevorgang;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


@XmlAccessorType(XmlAccessType.FIELD)
public abstract class HinweisOderFehlerBase {

    @XmlElement(name = "Nutzdatenticket", required = true)
    public String nutzdatenticket;

    @XmlElement(name = "Feldidentifikator")
    public String feldidentifikator;

    @XmlElement(name = "Mehrfachzeilenindex")
    public String mehrfachzeilenindex;

    @XmlElement(name = "LfdNrVordruck")
    public String lfdNrVordruck;

    @XmlElement(name = "Untersachbereich")
    public String untersachbereich;

    @XmlElement(name = "PrivateKennnummer")
    public String privateKennnummer;

    @XmlElement(name = "RegelName")
    public String regelName;

    @XmlElement(name = "Text")
    public String text;


    public abstract String getFachlicheId();


    public String getNutzdatenticket() {
        return nutzdatenticket;
    }

    public void setNutzdatenticket(String value) {
        this.nutzdatenticket = value;
    }


    public String getFeldidentifikator() {
        return feldidentifikator;
    }

    public void setFeldidentifikator(String value) {
        this.feldidentifikator = value;
    }


    public String getMehrfachzeilenindex() {
        return mehrfachzeilenindex;
    }

    public void setMehrfachzeilenindex(String value) {
        this.mehrfachzeilenindex = value;
    }


    public String getLfdNrVordruck() {
        return lfdNrVordruck;
    }

    public void setLfdNrVordruck(String value) {
        this.lfdNrVordruck = value;
    }


    public String getUntersachbereich() {
        return untersachbereich;
    }

    public void setUntersachbereich(String value) {
        this.untersachbereich = value;
    }


    public String getPrivateKennnummer() {
        return privateKennnummer;
    }

    public void setPrivateKennnummer(String value) {
        this.privateKennnummer = value;
    }


    public String getRegelName() {
        return regelName;
    }

    public void setRegelName(String value) {
        this.regelName = value;
    }


    public String getText() {
        return text;
    }

    public void setText(String value) {
        this.text = value;
    }

}
