
package de.elster.model.bearbeitevorgang;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


/**
 * 
 * 				Struktur, die im Falle von Hinweisen zurückgeliefert wird.
 * 			
 * 
 * <p>Java class for HinweisTyp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HinweisTyp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Nutzdatenticket" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}NutzdatenticketTyp"/>
 *         &lt;element name="Feldidentifikator" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}FeldidentifikatorTyp" minOccurs="0"/>
 *         &lt;element name="Mehrfachzeilenindex" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}MehrfachzeilenindexTyp" minOccurs="0"/>
 *         &lt;element name="LfdNrVordruck" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}LfdNrVordruckTyp" minOccurs="0"/>
 *         &lt;element name="Untersachbereich" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}UntersachbereichTyp" minOccurs="0"/>
 *         &lt;element name="PrivateKennnummer" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}PrivateKennnummerTyp" minOccurs="0"/>
 *         &lt;element name="RegelName" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}RegelNameTyp" minOccurs="0"/>
 *         &lt;element name="FachlicheHinweisId" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}FachlicheIdTyp" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://www.elster.de/EricXML/1.0/EricBearbeiteVorgang}TextTyp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
// has to be commented out for class inheritance to work
//@XmlType(name = "HinweisTyp", propOrder = {
//    "nutzdatenticket",
//    "feldidentifikator",
//    "mehrfachzeilenindex",
//    "lfdNrVordruck",
//    "untersachbereich",
//    "privateKennnummer",
//    "regelName",
//    "fachlicheHinweisId",
//    "text"
//})
public class HinweisTyp extends HinweisOderFehlerBase {

    protected String fachlicheHinweisId;

    public String getFachlicheHinweisId() {
        return fachlicheHinweisId;
    }

    public void setFachlicheHinweisId(String value) {
        this.fachlicheHinweisId = value;
    }


    @Override
    public String getFachlicheId() {
        return fachlicheHinweisId;
    }

}
