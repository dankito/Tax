package de.elster.eric.wrapper;

import com.sun.jna.ptr.IntByReference;
import de.elster.eric.wrapper.exception.EricException;
import de.elster.eric.wrapper.exception.WrapperException;
import de.elster.eric.wrapper.jna.EricapiLibrary;

import java.io.Closeable;

/**
 * Klasse fuer Zertifikat mit PIN und Zertifikatshandle
 * 
 */
public class EricCertificate implements Closeable {
	
	/** Pfad der Zertifikat-Datei */
	private final String certificatePath;
	
	/** PIN des Zertifikat */
	private final String pin;
	
	/** Handle des Zertifikat **/
	private final IntByReference certificateHandle = new IntByReference();

	/** pinSupport des Zertifikat **/
	private final IntByReference pinSupport = new IntByReference();
	
	/** ericapiLibrary **/
	private final EricapiLibrary eLib;
	
	public EricCertificate(EricapiLibrary pEricapiLib, final String pCertificatePath, final String pPin) throws WrapperException, EricException {
		this.certificatePath = pCertificatePath;
		this.pin 			 = pPin;
		this.eLib			 = pEricapiLib;
		
		if (certificatePath != null) {
			final int rc = pEricapiLib.EricGetHandleToCertificate(this.certificateHandle, pinSupport, Helper.encodePath(certificatePath));
			if(rc!=Errorcode.ERIC_OK.getErrorcode())
			{
				Helper.throwRcException(pEricapiLib, rc);
			}
		} 
	}

	public String getCertificatePath() {
		return certificatePath;
	}

	public String getPin() {
		if (pin.equalsIgnoreCase("_NULL"))
			return null;
		else
			return pin;
	}

	public IntByReference getCertificateHandle() {
		return this.certificateHandle;
	}
	
	public void close() {
		if (this.certificateHandle != null) {
			this.eLib.EricCloseHandleToCertificate (this.certificateHandle.getValue());
		}
	}
}
