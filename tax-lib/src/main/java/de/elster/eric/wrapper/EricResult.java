package de.elster.eric.wrapper;

/**
 * Klasse mit welcher bei einem erfolgreichem EricBearbeiteVorgang
 * Aufruf Telenummer und Ordnungsbegriff zurückgegeben werden.
 * 
 * Siehe XML-Schema API-Rueckgabe-Schemata/EricBearbeiteVorgang.xsd im
 * common-Paket
 */
public class EricResult {
	private final String telenummer;
	private final String ordnungsbegriff;

	protected EricResult(final String telenummer, final String ordnungsbegriff) {
		this.telenummer = telenummer;
		this.ordnungsbegriff = ordnungsbegriff;
	}

	public String getTelenummer() {
		return telenummer;
	}

	public String getOrdnungsbegriff() {
		return ordnungsbegriff;
	}

	public String toString() {
		String result = "";
		if (telenummer != null) {
			result += "Telenummer = " + telenummer + " ";
		}
		if (ordnungsbegriff != null) {
			result += "Ordnungsbegriff = " + ordnungsbegriff;
		}
		return result;
	}
}
