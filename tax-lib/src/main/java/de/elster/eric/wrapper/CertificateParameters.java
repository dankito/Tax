package de.elster.eric.wrapper;

/**
 * Klasse zur Übergabe von Zertifikatparametern.
 * 
 * Siehe eric_verschluesselungs_parameter_t in eric_types.h
 */
public class CertificateParameters {

	/** Pfad der Zertifikat-Datei */
	private final EricCertificate cert;
	
	/** PIN des Zertifikat */
	private final String pin;
	
	/** Eventuell Abrufcode */
	private final String abrufCode;

	public CertificateParameters(EricCertificate ericCert, final String pin, final String abrufCode) {
		this.cert = ericCert;
		if (pin.equalsIgnoreCase("_NULL"))
			this.pin =  null;
		else
			this.pin =  pin;
		this.abrufCode = abrufCode;
	}
	
	public CertificateParameters(EricCertificate ericCert, final String pin) {
		this(ericCert, pin, null);
	}

	public EricCertificate getCertificate() {
		return cert;
	}

	public String getPin() {
		return pin;
	}

	public String getAbrufCode() {
		return abrufCode;
	}
}
