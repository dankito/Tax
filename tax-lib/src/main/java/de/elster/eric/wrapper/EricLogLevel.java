package de.elster.eric.wrapper;

public enum EricLogLevel {

    /**
     * Sehr feingranulare Informationen über den Programmablauf und Werte.
     */
    TRACE(0),

    /**
     * Feingranulare Informationen über den Programmablauf und Werte.
     */
    DEBUG(1),

    /**
     * Grobe Informationen über den Programmablauf und Werte.
     */
    INFO(2),

    /**
     * Hinweise auf Zustände, die zu Fehlern führen können.
     */
    WARN(3),

    /**
     * Fehler, der zum Programmabbruch führt.
     */
    ERROR(4);


    public int level;

    private EricLogLevel(int level) {
        this.level = level;
    }

}
