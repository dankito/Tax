package de.elster.eric.wrapper;

/**
 * Repräsentiert einen Plausibilitätsfehler
 * 
 * Siehe XML-Schema API-Rueckgabe-Schemata/EricBearbeiteVorgang.xsd im
 * common-Paket
 */
public class ValidationError {

	public final String nutzdatenticket;
	public final String feldidentifikator;
	public final int mehrfachzeilenindex;
	public final int lfdNrVordruck;
	public final int untersachbereich;
	public final String regelName;
	public final String fachlicheFehlerId;
	public final String text;

	public ValidationError(final String nutzdatenticket, final String feldidentifikator, final int mehrfachzeilenindex,
			final int lfdNrVordruck, final int untersachbereich, final String regelName, final String fachlicheFehlerId, final String text) {
		this.nutzdatenticket = nutzdatenticket;
		this.feldidentifikator = feldidentifikator;
		this.mehrfachzeilenindex = mehrfachzeilenindex;
		this.lfdNrVordruck = lfdNrVordruck;
		this.untersachbereich = untersachbereich;
		this.regelName = regelName;
		this.fachlicheFehlerId = fachlicheFehlerId;
		this.text = text;
	}

	public String getNutzdatenticket() {
		return nutzdatenticket;
	}

	public String getFeldidentifikator() {
		return feldidentifikator;
	}

	public int getMehrfachzeilenindex() {
		return mehrfachzeilenindex;
	}

	public int getLfdNrVordruck() {
		return lfdNrVordruck;
	}

	public int getUntersachbereich() {
		return untersachbereich;
	}

	public String getRegelName() {
		return regelName;
	}

	public String getFachlicheFehlerId() {
		return fachlicheFehlerId;
	}

	public String getText() {
		return text;
	}

	public String toString() {
		StringBuilder ret = new StringBuilder(125);
		String newLine = System.getProperty("line.separator");
		return ret.append(" Nutzdatenticket = ").append(nutzdatenticket).append(newLine)
				.append(" Feldidentifikator = ").append(feldidentifikator).append(newLine)
				.append(" Mehrfachzeilenindex = ").append(mehrfachzeilenindex).append(newLine)
				.append(" Untersachbereich = ").append(untersachbereich).append(newLine)
				.append(" RegelName = ").append(regelName).append(newLine)
				.append(" FachlicheFehlerId = ").append(fachlicheFehlerId).append(newLine)
				.append(" Text = ").append(text).append(newLine).toString();
	}
}
