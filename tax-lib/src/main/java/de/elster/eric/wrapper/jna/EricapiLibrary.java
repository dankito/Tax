package de.elster.eric.wrapper.jna;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.Callback;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

import java.util.Arrays;
import java.util.List;

/**
 * JNA Wrapper für ERiC basierend auf der Header-Datei ericapi.h
 *
 */
public interface EricapiLibrary extends Library {

	int ERIC_VALIDIERE = 1 << 1;
	int ERIC_SENDE = 1 << 2;
	int ERIC_DRUCKE = 1 << 5;

	/**
	 * Druckparameter-Struktur für die EBV.
	 *
	 * Siehe eric_druck_parameter_t in ericapi.h
	 */
	static class eric_druck_parameter_t extends Structure {
		public int version;
		public int vorschau;
		public int ersteSeite;
		public int duplexDruck;
		public Pointer pdfName;
		public String fussText;

		@Override
		protected List<String> getFieldOrder() {
			return Arrays.asList(new String[] { "version", "vorschau", "ersteSeite", "duplexDruck", "pdfName", "fussText" });
		}
	}

	/**
	 * Verschlüsselungsparameter.
	 *
	 * Siehe eric_verschluesselungs_parameter_t in ericapi.h
	 */
	static class eric_verschluesselungs_parameter_t extends Structure {
		public int version;
		public int zertifikatHandle;
		public String pin;
		public String abrufCode;

		@Override
		protected List<String> getFieldOrder() {
			return Arrays.asList(new String[] { "version", "zertifikatHandle", "pin", "abrufCode" });
		}
	}

	interface EricFortschrittCallback extends Callback {
		void invoke(int id, int pos, int max, Pointer benutzerDaten);
	}

	/**
	 * Typ der Callback-Funktion zum Logging.
	 * Wenn registriert, wird diese Callback-Funktion für jeden Log-Eintrag mit folgenden
	 * Parametern aufgerufen.
	 */
	interface EricLogCallback extends Callback {

		/**
		 *
		 * @param kategorie Kategorie des Logeintrags. Beinhaltet das Modul, welches den Log-Eintrag
		 * ausgibt. Zum Beispiel "eric.ctrl2". Kann zum Filtern verwendet werden. Alle
		 * Log-Nachrichten besitzen eine Kategorie. Der Zeiger ist nur innerhalb dieser
		 * Funktion gültig.
		 * @param logLevel Log-Level des Logeintrags. Kann zum Filtern verwendet werden.
		 * @param nachricht Enthält die Log-Nachricht als Zeichenkette. Der Zeiger ist nur innerhalb dieser
		 * Funktion gültig.
		 * @param benutzerDaten Der Zeiger, der bei der Registrierung mit EricRegistriereLogCallback()
		 * übergeben worden ist, wird in diesem Parameter vom ERiC unverändert
		 * übergeben.
		 */
		void invoke(String kategorie, int logLevel, String nachricht, Pointer benutzerDaten); // TODO: add eric_log_level_t enum (see p. 38)
	}


	int EricBearbeiteVorgang(String datenpuffer, String datenartVersion, int bearbeitungsFlags, eric_druck_parameter_t druckParameter,
                             eric_verschluesselungs_parameter_t cryptoParameter, IntByReference transferHandle, PointerByReference rueckgabePuffer,
                             PointerByReference serverantwortPuffer);

	int EricEinstellungSetzen(String name, String wert);

	int EricEinstellungSetzen(String name, byte[] wert);

	int EricHoleFehlerText(int fehlerkode, PointerByReference rueckgabePuffer);

	PointerByReference EricRueckgabepufferErzeugen();

	String EricRueckgabepufferInhalt(PointerByReference handle);

	int EricRueckgabepufferLaenge(PointerByReference handle);

	int EricRueckgabepufferFreigeben(PointerByReference handle);

	int EricHoleFinanzamtLandNummern(PointerByReference handle);

	int EricHoleFinanzaemter(String finanzamtLandNummer, PointerByReference handle);

	int EricHoleFinanzamtsdaten(String bufaNr, PointerByReference handle);

	int EricPruefeSteuernummer(String steuernummer);

	/**
	 * Es wird eine Steuernummer im ELSTER-Steuernummerformat erzeugt.
	 * Die Funktion erzeugt aus einer angegebenen Steuernummer im Format des
	 * Steuerbescheides eine 13-stellige Steuernummer im ELSTER-Steuernummerformat.
	 * Die sich ergebende 13-stellige Steuernummer im ELSTER-Steuernummerformat wird
	 * von der Funktion EricMakeElsterStnr() auch auf Gültigkeit geprüft.
	 * Einer der beiden Parameter landesnr oder bundesfinanzamtsnr muss korrekt
	 * angegeben werden. Der jeweils andere Parameter darf NULL oder leer sein. Bei
	 * bayerischen und berliner Steuernummern im Format BBB/UUUUP ist die Angabe der
	 * Bundesfinanzamtsnummer zwingend erforderlich.
	 *
	 * @param steuernrBescheid Format der Steuernummer wie auch auf amtlichen Schreiben angegeben.
	 * @param landesnr 2-stellige Landesnummer (entspricht den ersten zwei Stellen der Bundesfinanzamtsnummer).
	 * @param bundesfinanzamtsnr 4-stellige Bundesfinanzamtsnummer.
	 * @param steuernrPuffer Handle auf einen Rückgabepuffer, in den die Steuernummer im ELSTER-Steuernummerformat geschrieben wird.
	 *                          Zur Erzeugung, Verwendung und Freigabe von Rückgabepuffern siehe Dokumentation zu EricRueckgabepufferHandle.
	 * @return ERIC_OK,
	 * ERIC_GLOBAL_STEUERNUMMER_UNGUELTIG,
	 * ERIC_GLOBAL_LANDESNUMMER_UNBEKANNT,
	 * ERIC_GLOBAL_NULL_PARAMETER,
	 * ERIC_GLOBAL_UNGUELTIGER_PARAMETER,
	 * ERIC_GLOBAL_NICHT_GENUEGEND_ARBEITSSPEICHER,
	 * ERIC_GLOBAL_UNKNOWN
	 */
	int EricMakeElsterStnr(String steuernrBescheid, String landesnr, String bundesfinanzamtsnr, PointerByReference steuernrPuffer);

	int EricGetAuswahlListen(String datenartVersion, String feldkennung, PointerByReference returnBuffer);

	int EricGetHandleToCertificate(IntByReference hToken, IntByReference iInfoPinSupport, byte[] pathToKeystore);

	int EricCloseHandleToCertificate(int hToken);

	/**
	 * <p>Die registrierte funktion wird als Callback-Funktion für jede Lognachricht aufgerufen. Die
	 * Ausgabe entspricht einer Zeile im eric.log.</p>
	 *
	 * <b>Bemerkungen:</b>
	 * <ul>
	 *     <li>Wenn eine zuvor registrierte Funktion nicht mehr aufgerufen werden soll, ist
	 * 	 		EricRegistriereLogCallback() mit dem Wert NULL im Parameter funktion
	 * 	 		aufzurufen (=Deregistrierung).
	 * 	   </li>
	 *     <li>Vor dem Beenden der Steueranwendung ist eine registrierte Funktion zu deregistrieren,
	 * 	 		da es sonst zu einem Absturz kommen kann.
	 * 	   </li>
	 *     <li>
	 *         Es ist nicht erlaubt eine ERiC API-Funktion aus einer Callback-Funktion aufzurufen.
	 *     </li>
	 *     <li>
	 *         Die Verarbeitung im Callback findet synchron statt. Deshalb sollte der Callback sehr
	 * 	 		schnell ausgeführt werden.
	 *     </li>
	 * </ul>
	 *
	 * @param funktion Zeiger auf die zu registrierende Funktion oder NULL.
	 * @param schreibeEricLogDatei <ul><li>1 Jede Log-Nachricht wird nach eric.log geschrieben. Der Parameter funktion kann auf eine Funktion zeigen oder NULL ein.</li>
	 * 	 <li>0 Falls funktion != NULL werden keine Log-Nachrichten nach eric.log geschrieben, andernfalls werden die Log-Nachrichten nach eric.log geschrieben.</li></ul>
	 * @param benutzerDaten Zeiger, welcher der registrierten Funktion immer mitgegeben wird. Die
	 * Anwendung kann diesen Parameter dazu verwenden, einen Zeiger auf eigene
	 * Daten oder Funktionen an die zu registrierende Funktion übergeben zu lassen.
	 * @return
	 */
	int EricRegistriereLogCallback(EricLogCallback funktion, int schreibeEricLogDatei, Pointer benutzerDaten);

	int EricRegistriereFortschrittCallback(EricFortschrittCallback funktion, Pointer benutzerDaten);

	int EricRegistriereGlobalenFortschrittCallback(EricFortschrittCallback funktion, Pointer benutzerDaten);

    int EricHoleZertifikatEigenschaften(int hToken, String pin, PointerByReference rueckgabePuffer);

	int EricInitialisiere(String pluginPfad, String logPfad);

	int EricBeende();
}
