package de.elster.eric.wrapper;


public interface LogCallback {

    void log(String ericModule, EricLogLevel logLevel, String message);

}
