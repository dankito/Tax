/*
 * Copyright © LfSt Bayern, Muenchen, IuK 111, 2011
 */

package de.elster.eric.wrapper;

/**
* Enumeration aller ERiC-Fehlercodes.
* 
*/
public enum Errorcode {

    /**
     * [0] Verarbeitung fehlerfrei.
     */
    ERIC_OK(0),

    /**
     * [610001001] Verarbeitung fehlerhaft, keine genaueren Informationen vorhanden. Details stehen ggf. im Logfile (eric.log).
     */
    ERIC_GLOBAL_UNKNOWN(610001001),

    /**
     * [610001002] Fehler w&auml;hrend der Plausibilit&auml;tspr&uuml;fung, Datensatz nicht plausibel.
     */
    ERIC_GLOBAL_PRUEF_FEHLER(610001002),

    /**
     * [610001003] Hinweise w&auml;hrend der Plausibilit&auml;tspr&uuml;fung, Datensatz ist aber plausibel.
     */
    ERIC_GLOBAL_HINWEISE(610001003),

    /**
     * [610001007] Keine Klartextfehlermeldung vorhanden.
     */
    ERIC_GLOBAL_FEHLERMELDUNG_NICHT_VORHANDEN(610001007),

    /**
     * [610001008] F&uuml;r den &uuml;bergebenen Wert sind keine Daten vorhanden.
     */
    ERIC_GLOBAL_KEINE_DATEN_VORHANDEN(610001008),

    /**
     * [610001013] Es ist nicht gen&uuml;gend Arbeitsspeicher vorhanden.
     */
    ERIC_GLOBAL_NICHT_GENUEGEND_ARBEITSSPEICHER(610001013),

    /**
     * [610001014] Datei nicht gefunden.
     */
    ERIC_GLOBAL_DATEI_NICHT_GEFUNDEN(610001014),

    /**
     * [610001016] F&uuml;r dieses Verfahren/diese Datenart ist eine Bearbeitung mit der angegebenen HerstellerID nicht erlaubt.
     */
    ERIC_GLOBAL_HERSTELLER_ID_NICHT_ERLAUBT(610001016),

    /**
     * [610001017] Ung&uuml;ltiger Zustand.
     */
    ERIC_GLOBAL_ILLEGAL_STATE(610001017),

    /**
     * [610001018] Die aufgerufene Funktion ist nicht erlaubt.
     */
    ERIC_GLOBAL_FUNKTION_NICHT_ERLAUBT(610001018),

    /**
     * [610001019] F&uuml;r dieses Verfahren/diese Datenart/diese Test-HerstellerID/diese ERiC-Einstellungen sind Echtf&auml;lle nicht erlaubt.
     */
    ERIC_GLOBAL_ECHTFALL_NICHT_ERLAUBT(610001019),

    /**
     * [610001020] Der Versand von Echtf&auml;llen (= F&auml;llen ohne gesetzten Testmerker) ist mit einer BETA-Version nicht m&ouml;glich.
     */
    ERIC_GLOBAL_NO_VERSAND_IN_BETA_VERSION(610001020),

    /**
     * [610001025] Der &uuml;bergebene Testmerker ist f&uuml;r das angegebene Verfahren nicht zul&auml;ssig.
     */
    ERIC_GLOBAL_TESTMERKER_UNGUELTIG(610001025),

    /**
     * [610001026] Der zu versendende Datensatz ist zu gro&szlig;.
     */
    ERIC_GLOBAL_DATENSATZ_ZU_GROSS(610001026),

    /**
     * [610001027] Bei der angegebenen Versandart d&uuml;rfen Verschl&uuml;sselungsparameter nicht angegeben werden.
     */
    ERIC_GLOBAL_VERSCHLUESSELUNGS_PARAMETER_NICHT_ERLAUBT(610001027),

    /**
     * [610001028] Bei der angegebenen Versandart sind nur Portal-Zertifikate erlaubt.
     */
    ERIC_GLOBAL_NUR_PORTALZERTIFIKAT_ERLAUBT(610001028),

    /**
     * [610001029] F&uuml;r die &uuml;bermittelte Datenart ist die Angabe eines Abrufcodes nicht zul&auml;ssig, daher muss f&uuml;r den Abrufcode der Wert NULL &uuml;bergeben werden.
     */
    ERIC_GLOBAL_ABRUFCODE_NICHT_ERLAUBT(610001029),

    /**
     * [610001030] Es ist ein Fehler bei der Umwandlung nach XML aufgetreten.
     */
    ERIC_GLOBAL_ERROR_XML_CREATE(610001030),

    /**
     * [610001031] Die Gr&ouml;&szlig;e des Textpuffers kann nicht ver&auml;ndert werden.
     */
    ERIC_GLOBAL_TEXTPUFFERGROESSE_FIX(610001031),

    /**
     * [610001032] Interner Fehler aufgetreten. Details stehen ggf. im Logfile (eric.log).
     */
    ERIC_GLOBAL_INTERNER_FEHLER(610001032),

    /**
     * [610001033] Bei einer arithmetischen Operation ist ein Fehler aufgetreten. Details stehen im Logile (eric.log)
     */
    ERIC_GLOBAL_ARITHMETIKFEHLER(610001033),

    /**
     * [610001034] Ung&uuml;ltige Steuernummer.
     */
    ERIC_GLOBAL_STEUERNUMMER_UNGUELTIG(610001034),

    /**
     * [610001035] Ung&uuml;ltige Steuernummer: Es werden 13 Stellen erwartet.
     */
    ERIC_GLOBAL_STEUERNUMMER_FALSCHE_LAENGE(610001035),

    /**
     * [610001036] Ung&uuml;ltige Steuernummer: Es werden nur Ziffern erwartet.
     */
    ERIC_GLOBAL_STEUERNUMMER_NICHT_NUMERISCH(610001036),

    /**
     * [610001037] Ung&uuml;ltige Landesnummer.
     */
    ERIC_GLOBAL_LANDESNUMMER_UNBEKANNT(610001037),

    /**
     * [610001038] Ung&uuml;ltige Bundesfinanzamtsnummer.
     */
    ERIC_GLOBAL_BUFANR_UNBEKANNT(610001038),

    /**
     * [610001039] Ung&uuml;ltige Bundesfinanzamtsnummer.
     */
    ERIC_GLOBAL_LANDESNUMMER_BUFANR(610001039),

    /**
     * [610001040] Ein Puffer-Handle wurde mehrfach &uuml;bergeben.
     */
    ERIC_GLOBAL_PUFFER_ZUGRIFFSKONFLIKT(610001040),

    /**
     * [610001041] Es wurde versucht, einen Puffer &uuml;ber die maximal m&ouml;gliche L&auml;nge hinaus zu beschreiben.
     */
    ERIC_GLOBAL_PUFFER_UEBERLAUF(610001041),

    /**
     * [610001042] Die &uuml;bergebene Datenartversion ist unbekannt.
     */
    ERIC_GLOBAL_DATENARTVERSION_UNBEKANNT(610001042),

    /**
     * [610001044] Die &uuml;bergebene Datenartversion passt nicht zum Eingangs-XML. Details stehen ggf. im Logfile (eric.log).
     */
    ERIC_GLOBAL_DATENARTVERSION_XML_INKONSISTENT(610001044),

    /**
     * [610001045] Das Plugin 'commonData' konnte nicht geladen werden oder bietet einen ben&ouml;tigten Service nicht an. Details stehen im Logfile (eric.log).
     */
    ERIC_GLOBAL_COMMONDATA_NICHT_VERFUEGBAR(610001045),

    /**
     * [610001046] Beim Schreiben in die Protokolldatei ist eine Ausnahme aufgetreten.
     */
    ERIC_GLOBAL_LOG_EXCEPTION(610001046),

    /**
     * [610001047] F&uuml;r diese Datenart darf im TransferHeader kein TransportSchluessel angegeben werden.
     */
    ERIC_GLOBAL_TRANSPORTSCHLUESSEL_NICHT_ERLAUBT(610001047),

    /**
     * [610001048] Der &uuml;bergebene &ouml;ffentliche Schl&uuml;ssel kann nicht eingelesen werden.
     */
    ERIC_GLOBAL_OEFFENTLICHER_SCHLUESSEL_UNGUELTIG(610001048),

    /**
     * [610001049] Der Typ des im TransferHeader angegebenen Transportschl&uuml;ssels ist f&uuml;r diese Datenart nicht erlaubt.
     */
    ERIC_GLOBAL_TRANSPORTSCHLUESSEL_TYP_FALSCH(610001049),

    /**
     * [610001050] Das &uuml;bergebene Puffer-Handle wurde nicht mit der vorliegenden Instanz erzeugt.
     */
    ERIC_GLOBAL_PUFFER_UNGLEICHER_INSTANZ(610001050),

    /**
     * [610001051] Das Element &quot;Vorsatz&quot; enth&auml;lt ung&uuml;ltige Werte, Details stehen im Logfile (eric.log).
     */
    ERIC_GLOBAL_VORSATZ_UNGUELTIG(610001051),

    /**
     * [610001080] Die &uuml;bergebene Instanz ist gleich NULL oder bereits freigegeben worden.
     */
    ERIC_GLOBAL_UNGUELTIGE_INSTANZ(610001080),

    /**
     * [610001081] Der Singlethread-ERiC wurde nicht mit EricInitialisiere initialisiert.
     */
    ERIC_GLOBAL_NICHT_INITIALISIERT(610001081),

    /**
     * [610001082] Der Singlethread-ERiC wurde bereits mit EricInitialisiere initialisiert.
     */
    ERIC_GLOBAL_MEHRFACHE_INITIALISIERUNG(610001082),

    /**
     * [610001083] Der angeforderte ERiC-Instanz konnte nicht erstellt werden. Details stehen ggf. im Logfile (eric.log).
     */
    ERIC_GLOBAL_FEHLER_INITIALISIERUNG(610001083),

    /**
     * [610001102] Unbekannter Parameterfehler.
     */
    ERIC_GLOBAL_UNKNOWN_PARAMETER_ERROR(610001102),

    /**
     * [610001108] Defekter Nutzdatensatz.
     */
    ERIC_GLOBAL_CHECK_CORRUPTED_NDS(610001108),

    /**
     * [610001206] Verschl&uuml;sselter/authentifizierter Versand gew&uuml;nscht, aber keine notwendigen Verschl&uuml;sselungsparameter angegeben.
     */
    ERIC_GLOBAL_VERSCHLUESSELUNGS_PARAMETER_NICHT_ANGEGEBEN(610001206),

    /**
     * [610001209] Es ist mehr als ein Versandflag angegeben.
     */
    ERIC_GLOBAL_SEND_FLAG_MEHR_ALS_EINES(610001209),

    /**
     * [610001218] Die &uuml;bergebene Kombination von Bearbeitungsflags ist nicht erlaubt.
     */
    ERIC_GLOBAL_UNGUELTIGE_FLAG_KOMBINATION(610001218),

    /**
     * [610001220] Der Erste-Seite-Druck ist nur f&uuml;r Steuerberater bei nicht-authentifizierten Einkommenssteuerf&auml;llen ab Veranlagungszeitraum 2010 ohne Versandanforderung erlaubt.
     */
    ERIC_GLOBAL_ERSTE_SEITE_DRUCK_NICHT_UNTERSTUETZT(610001220),

    /**
     * [610001222] Die angegebenen Parameter sind ung&uuml;ltig oder unvollst&auml;ndig.
     */
    ERIC_GLOBAL_UNGUELTIGER_PARAMETER(610001222),

    /**
     * [610001224] F&uuml;r das angegebene Verfahren wird der Druck nicht unterst&uuml;tzt.
     */
    ERIC_GLOBAL_DRUCK_FUER_VERFAHREN_NICHT_ERLAUBT(610001224),

    /**
     * [610001225] Die Versandart ist f&uuml;r die angegebene Datenartversion nicht erlaubt.
     */
    ERIC_GLOBAL_VERSAND_ART_NICHT_UNTERSTUETZT(610001225),

    /**
     * [610001226] Die Version eines der angegebenen Parameter ist ung&uuml;ltig.
     */
    ERIC_GLOBAL_UNGUELTIGE_PARAMETER_VERSION(610001226),

    /**
     * [610001227] F&uuml;r das Verfahren Datenabholung wurde ein illegales Transferhandle angegeben.
     */
    ERIC_GLOBAL_TRANSFERHANDLE(610001227),

    /**
     * [610001228] Die Initialisierung eines Plugins ist fehlgeschlagen.
     */
    ERIC_GLOBAL_PLUGININITIALISIERUNG(610001228),

    /**
     * [610001229] Die Versionen der im Logfile genannten ERiC-Dateien sind nicht kompatibel. (Siehe eric.log.)
     */
    ERIC_GLOBAL_INKOMPATIBLE_VERSIONEN(610001229),

    /**
     * [610001230] Das im XML-Element &quot;&lt;Verschluesselung&gt;&quot; angegebene Verschl&uuml;sselungsverfahren wird vom ERiC nicht unterst&uuml;tzt.
     */
    ERIC_GLOBAL_VERSCHLUESSELUNGSVERFAHREN_NICHT_UNTERSTUETZT(610001230),

    /**
     * [610001231] Der Aufruf eine API-Funktion des ERiCs darf erst dann erfolgen, wenn ein vorheriger Aufruf zur&uuml;ckgekehrt ist.
     */
    ERIC_GLOBAL_MEHRFACHAUFRUFE_NICHT_UNTERSTUETZT(610001231),

    /**
     * [610001404] Das Bundesland/Finanzamt mit der angegebenen Nummer nimmt bei der angegebenen Steuerart am ELSTER-Verfahren nicht teil.
     */
    ERIC_GLOBAL_UTI_COUNTRY_NOT_SUPPORTED(610001404),

    /**
     * [610001501] Ung&uuml;ltige IBAN: IBAN muss aus zweistelligem L&auml;ndercode gefolgt von zweistelliger Pr&uuml;fziffer gefolgt von der Basic Bank Account Number bestehen.
     */
    ERIC_GLOBAL_IBAN_FORMALER_FEHLER(610001501),

    /**
     * [610001502] Ung&uuml;ltige IBAN: Der angegebene L&auml;ndercode ist ung&uuml;ltig oder wird aktuell im ELSTER-Verfahren nicht unterst&uuml;tzt.
     */
    ERIC_GLOBAL_IBAN_LAENDERCODE_FEHLER(610001502),

    /**
     * [610001503] Ung&uuml;ltige IBAN: Die angegebene IBAN entspricht nicht dem f&uuml;r das angegebene Land definierten formalen Aufbau der IBAN.
     */
    ERIC_GLOBAL_IBAN_LANDESFORMAT_FEHLER(610001503),

    /**
     * [610001504] Ung&uuml;ltige IBAN: Die Pr&uuml;fziffernberechnung zur angegebenen IBAN f&uuml;hrt zu einer abweichenden Pr&uuml;fziffer.
     */
    ERIC_GLOBAL_IBAN_PRUEFZIFFER_FEHLER(610001504),

    /**
     * [610001510] Ung&uuml;ltiger BIC: Der formale Aufbau des angegeben BIC ist ung&uuml;ltig.
     */
    ERIC_GLOBAL_BIC_FORMALER_FEHLER(610001510),

    /**
     * [610001511] Ung&uuml;ltiger BIC: Der angegebene L&auml;ndercode ist ung&uuml;ltig oder wird aktuell im ELSTER-Verfahren nicht unterst&uuml;tzt.
     */
    ERIC_GLOBAL_BIC_LAENDERCODE_FEHLER(610001511),

    /**
     * [610001519] Die angegebene Zulassungsnummer entspricht nicht den L&auml;ngenvorgaben. Es sind maximal 6 Stellen erlaubt.
     */
    ERIC_GLOBAL_ZULASSUNGSNUMMER_ZU_LANG(610001519),

    /**
     * [610001525] Die &uuml;bergebene IDNummer ist ung&uuml;ltig.
     */
    ERIC_GLOBAL_IDNUMMER_UNGUELTIG(610001525),

    /**
     * [610001526] Es wurde der Parameter NULL &uuml;bergeben.
     */
    ERIC_GLOBAL_NULL_PARAMETER(610001526),

    /**
     * [610001851] Update des ERiC erforderlich. Starten Sie nun das Update.
     */
    ERIC_GLOBAL_UPDATE_NECESSARY(610001851),

    /**
     * [610001860] Ung&uuml;ltiger Name f&uuml;r Einstellung.
     */
    ERIC_GLOBAL_EINSTELLUNG_NAME_UNGUELTIG(610001860),

    /**
     * [610001861] Ung&uuml;ltiger Wert f&uuml;r Einstellung.
     */
    ERIC_GLOBAL_EINSTELLUNG_WERT_UNGUELTIG(610001861),

    /**
     * [610001862] Fehler beim Dekodieren.
     */
    ERIC_GLOBAL_ERR_DEKODIEREN(610001862),

    /**
     * [610001863] Die aufgerufene Funktion wird nicht unterst&uuml;tzt.
     */
    ERIC_GLOBAL_FUNKTION_NICHT_UNTERSTUETZT(610001863),

    /**
     * [610001865] Fehler im &uuml;bergebenen EDS-XML: In den Sammeldaten wurde ein Nutzdatenticket f&uuml;r mehrere Steuerf&auml;lle verwendet. F&uuml;r jeden Steuerfall muss jedoch ein eigenes Nutzdatenticket angegeben werden.
     */
    ERIC_GLOBAL_NUTZDATENTICKETS_NICHT_EINDEUTIG(610001865),

    /**
     * [610001866] Fehler im &uuml;bergebenen EDS-XML: Bei den Sammeldaten wurden unterschiedliche Versionen des Nutzdaten-Headers verwendet. Innerhalb einer Datenlieferung ist jedoch nur eine Nutzdaten-Header-Version zul&auml;ssig.
     */
    ERIC_GLOBAL_NUTZDATENHEADERVERSIONEN_UNEINHEITLICH(610001866),

    /**
     * [610001867] Fehler im &uuml;bergebenen EDS-XML: Es wurden F&auml;lle f&uuml;r mehrere Bundesl&auml;nder angegeben. Innerhalb einer Datenlieferung d&uuml;rfen jedoch nur F&auml;lle f&uuml;r ein Bundesland angegeben werden.
     */
    ERIC_GLOBAL_BUNDESLAENDER_UNEINHEITLICH(610001867),

    /**
     * [610001868] Fehler im &uuml;bergebenen EDS-XML: Es wurden F&auml;lle f&uuml;r unterschiedliche Jahre angegeben. Innerhalb einer Datenlieferung d&uuml;rfen jedoch nur F&auml;lle f&uuml;r ein und dasselbe Jahr angegeben werden.
     */
    ERIC_GLOBAL_ZEITRAEUME_UNEINHEITLICH(610001868),

    /**
     * [610001869] Fehler im &uuml;bergebenen EDS-XML: Der Inhalt des Nutzdaten-Elements &quot;&lt;Empfaenger&gt;&quot; ist f&uuml;r diese Datenart nicht korrekt.
     */
    ERIC_GLOBAL_NUTZDATENHEADER_EMPFAENGER_NICHT_KORREKT(610001869),

    /**
     * [610101200] Allgemeiner Kommunikationsfehler.
     */
    ERIC_TRANSFER_COM_ERROR(610101200),

    /**
     * [610101201] Dieser Vorgang wird von der aufgerufenen Funktion nicht unterst&uuml;tzt.
     */
    ERIC_TRANSFER_VORGANG_NICHT_UNTERSTUETZT(610101201),

    /**
     * [610101210] Fehler im Transferheader. Der ELSTER-Annahmeserver hat einen Fehler zur&uuml;ckgemeldet. Bitte werten Sie die Serverantwort aus.
     */
    ERIC_TRANSFER_ERR_XML_THEADER(610101210),

    /**
     * [610101251] Es wurden ung&uuml;ltige Parameter &uuml;bergeben.
     */
    ERIC_TRANSFER_ERR_PARAM(610101251),

    /**
     * [610101253] Im XML-String konnte der Text &quot;&lt;/DatenTeil&gt;&quot; nicht gefunden werden.
     */
    ERIC_TRANSFER_ERR_DATENTEILENDNOTFOUND(610101253),

    /**
     * [610101255] Im XML-String konnte der Text &quot;&lt;DatenLieferant&gt;&quot; nicht gefunden werden.
     */
    ERIC_TRANSFER_ERR_BEGINDATENLIEFERANT(610101255),

    /**
     * [610101256] Im XML-String konnte der Text &quot;&lt;/DatenLieferant&gt;&quot; nicht gefunden werden.
     */
    ERIC_TRANSFER_ERR_ENDDATENLIEFERANT(610101256),

    /**
     * [610101257] Im XML-String konnte der Text &quot;&lt;TransportSchluessel&gt;&quot; nicht gefunden werden.
     */
    ERIC_TRANSFER_ERR_BEGINTRANSPORTSCHLUESSEL(610101257),

    /**
     * [610101258] Im XML-String konnte der Text &quot;&lt;/TransportSchluessel&gt;&quot; nicht gefunden werden.
     */
    ERIC_TRANSFER_ERR_ENDTRANSPORTSCHLUESSEL(610101258),

    /**
     * [610101259] Im XML-String konnte der Text &quot;&lt;DatenGroesse&gt;&quot; nicht gefunden werden.
     */
    ERIC_TRANSFER_ERR_BEGINDATENGROESSE(610101259),

    /**
     * [610101260] Im XML-String konnte der Text &quot;&lt;/DatenGroesse&gt;&quot; nicht gefunden werden.
     */
    ERIC_TRANSFER_ERR_ENDDATENGROESSE(610101260),

    /**
     * [610101271] Beim Datenaustausch ist ein Fehler aufgetreten.
     */
    ERIC_TRANSFER_ERR_SEND(610101271),

    /**
     * [610101274] Die Antwortdaten waren nicht PKCS#7-verschl&uuml;sselt.
     */
    ERIC_TRANSFER_ERR_NOTENCRYPTED(610101274),

    /**
     * [610101276] Verbindung zum ProxyServer konnte nicht aufgebaut werden.
     */
    ERIC_TRANSFER_ERR_PROXYCONNECT(610101276),

    /**
     * [610101278] Zu den Servern konnte keine Verbindung aufgebaut werden.
     */
    ERIC_TRANSFER_ERR_CONNECTSERVER(610101278),

    /**
     * [610101279] Von der Clearingstelle konnte keine Antwort empfangen werden.
     */
    ERIC_TRANSFER_ERR_NORESPONSE(610101279),

    /**
     * [610101280] Der Proxyserver erwartet Anmeldedaten.
     */
    ERIC_TRANSFER_ERR_PROXYAUTH(610101280),

    /**
     * [610101282] Fehler bei der Initialisierung des Versands, Details stehen ggf. im Logfile (eric.log).
     */
    ERIC_TRANSFER_ERR_SEND_INIT(610101282),

    /**
     * [610101283] Bei der Kommunikation mit dem Server kam es zu einer Zeit&uuml;berschreitung.
     */
    ERIC_TRANSFER_ERR_TIMEOUT(610101283),

    /**
     * [610101284] Es wurde kein g&uuml;ltiger Port f&uuml;r den Proxy angegeben.
     */
    ERIC_TRANSFER_ERR_PROXYPORT_INVALID(610101284),

    /**
     * [610101291] Sonstiger, nicht definierter Fehler aufgetreten.
     */
    ERIC_TRANSFER_ERR_OTHER(610101291),

    /**
     * [610101292] Fehler im NutzdatenHeader. Der ELSTER-Annahmeserver hat einen Fehler zur&uuml;ckgemeldet. Bitte werten Sie die Serverantwort aus. Bei Sammeldaten sind alle Nutzdatenbl&ouml;cke zu pr&uuml;fen, um den fehlerhaften Datensatz identifizieren zu k&ouml;nnen.
     */
    ERIC_TRANSFER_ERR_XML_NHEADER(610101292),

    /**
     * [610101293] Das XML liegt im falschen Encoding vor.
     */
    ERIC_TRANSFER_ERR_XML_ENCODING(610101293),

    /**
     * [610101294] Im XML-String konnte der Text &quot;&lt;/SigUser&gt;&quot; nicht gefunden werden.
     */
    ERIC_TRANSFER_ERR_ENDSIGUSER(610101294),

    /**
     * [610101295] Im XML-String konnte ein Tag nicht gefunden werden.
     */
    ERIC_TRANSFER_ERR_XMLTAG_NICHT_GEFUNDEN(610101295),

    /**
     * [610101297] Das XML-Element &quot;&lt;DatenTeil&gt;&quot; konnte nicht gelesen werden.
     */
    ERIC_TRANSFER_ERR_DATENTEILFEHLER(610101297),

    /**
     * [610101500] Es konnte kein Ad Hoc-Zertifikat fuer den Personalausweis oder den Aufenthaltstitel erzeugt bzw. gefunden werden, Details stehen ggf. im Logfile (eric.log).
     */
    ERIC_TRANSFER_EID_ZERTIFIKATFEHLER(610101500),

    /**
     * [610101510] F&uuml;r die Identifikationsnummer des Benutzers existiert kein Konto bei ELSTER.
     */
    ERIC_TRANSFER_EID_KEINKONTO(610101510),

    /**
     * [610101511] Dem Benutzer konnte keine eindeutige Identifikationsnummer zugeordnet werden.
     */
    ERIC_TRANSFER_EID_IDNRNICHTEINDEUTIG(610101511),

    /**
     * [610101512] Das nPA-Servlet konnte keine Verbindung zum eID-Server aufbauen.
     */
    ERIC_TRANSFER_EID_SERVERFEHLER(610101512),

    /**
     * [610101520] Der eID-Client ist nicht erreichbar. Wahrscheinlich wurde er nicht gestartet oder die &uuml;bergebene lokale URL ist nicht korrekt
     */
    ERIC_TRANSFER_EID_KEINCLIENT(610101520),

    /**
     * [610101521] Der eID-Client hat einen Fehler gemeldet. Details zu dem Fehler finden Sie im Log des eID-Clients oder ggf. im ERiC Logfile (eric.log).
     */
    ERIC_TRANSFER_EID_CLIENTFEHLER(610101521),

    /**
     * [610101522] Es konnten nicht alle ben&ouml;tigten Datenfelder des Personalausweises ausgelesen werden. Bitte pr&uuml;fen Sie &uuml;ber die Funktion &quot;Selbstauskunft&quot; des eID-Clients, ob folgende Daten von Ihrem Personalausweis korrekt bereitgestellt werden: Familienname, Vorname(n), Geburtsdatum, Anschrift (mit Postleitzahl) und Dokumentenart.
     */
    ERIC_TRANSFER_EID_FEHLENDEFELDER(610101522),

    /**
     * [610101523] Das Auslesen der Daten aus dem Personalausweis wurde vom Anwender abgebrochen.
     */
    ERIC_TRANSFER_EID_IDENTIFIKATIONABGEBROCHEN(610101523),

    /**
     * [610101524] Der Personalausweis wird von einem anderen Vorgang blockiert. Beenden Sie den anderen Vorgang und versuchen Sie es dann erneut.
     */
    ERIC_TRANSFER_EID_NPABLOCKIERT(610101524),

    /**
     * [610201016] Fehler bei der Schl&uuml;sselerzeugung.
     */
    ERIC_CRYPT_ERROR_CREATE_KEY(610201016),

    /**
     * [610201101] eSigner: Ung&uuml;ltiges Token Handle.
     */
    ERIC_CRYPT_E_INVALID_HANDLE(610201101),

    /**
     * [610201102] eSigner: Zu viele Sessions ge&ouml;ffnet.
     */
    ERIC_CRYPT_E_MAX_SESSION(610201102),

    /**
     * [610201103] eSigner: &Uuml;berlastung.
     */
    ERIC_CRYPT_E_BUSY(610201103),

    /**
     * [610201104] eSigner: Speicherzuordnungsfehler.
     */
    ERIC_CRYPT_E_OUT_OF_MEM(610201104),

    /**
     * [610201105] eSigner: Ung&uuml;ltiger PSE Pfad.
     */
    ERIC_CRYPT_E_PSE_PATH(610201105),

    /**
     * [610201106] eSigner: Falsche PIN.
     */
    ERIC_CRYPT_E_PIN_WRONG(610201106),

    /**
     * [610201107] eSigner: PIN gesperrt.
     */
    ERIC_CRYPT_E_PIN_LOCKED(610201107),

    /**
     * [610201108] eSigner: Fehler beim Lesen des PKCS#7-Objekts.
     */
    ERIC_CRYPT_E_P7_READ(610201108),

    /**
     * [610201109] eSigner: Fehler beim PKCS#7 Dekodieren.
     */
    ERIC_CRYPT_E_P7_DECODE(610201109),

    /**
     * [610201110] eSigner: Entschl&uuml;sselungszertifikat nicht in Empf&auml;ngerliste enthalten.
     */
    ERIC_CRYPT_E_P7_RECIPIENT(610201110),

    /**
     * [610201111] eSigner: Fehler beim Lesen des PKCS#12-Objekts.
     */
    ERIC_CRYPT_E_P12_READ(610201111),

    /**
     * [610201112] eSigner: Fehler beim Dekodieren des PKCS#12-Objekts.
     */
    ERIC_CRYPT_E_P12_DECODE(610201112),

    /**
     * [610201113] eSigner: Fehler beim Zugriff auf Soft-PSE-Signaturschl&uuml;ssel.
     */
    ERIC_CRYPT_E_P12_SIG_KEY(610201113),

    /**
     * [610201114] eSigner: Fehler beim Zugriff auf Soft-PSE Entschl&uuml;sselungsschl&uuml;ssel.
     */
    ERIC_CRYPT_E_P12_ENC_KEY(610201114),

    /**
     * [610201115] eSigner: Fehler beim Zugriff auf Hard-Token Signaturschl&uuml;ssel.
     */
    ERIC_CRYPT_E_P11_SIG_KEY(610201115),

    /**
     * [610201116] eSigner: Fehler beim Zugriff auf Hard-Token Entschl&uuml;sselungsschl&uuml;ssel.
     */
    ERIC_CRYPT_E_P11_ENC_KEY(610201116),

    /**
     * [610201117] eSigner: Fehler beim Parsen der XML-Eingabedatei.
     */
    ERIC_CRYPT_E_XML_PARSE(610201117),

    /**
     * [610201118] eSigner: Fehler beim Erzeugen des XML-Signaturasts.
     */
    ERIC_CRYPT_E_XML_SIG_ADD(610201118),

    /**
     * [610201119] eSigner: XML-Signaturtag nicht vorhanden.
     */
    ERIC_CRYPT_E_XML_SIG_TAG(610201119),

    /**
     * [610201120] eSigner: Fehler bei XML-Signaturerzeugung.
     */
    ERIC_CRYPT_E_XML_SIG_SIGN(610201120),

    /**
     * [610201121] eSigner: Parameter-Fehler, unbekanntes Encoding.
     */
    ERIC_CRYPT_E_ENCODE_UNKNOWN(610201121),

    /**
     * [610201122] eSigner: Encoding-Fehler.
     */
    ERIC_CRYPT_E_ENCODE_ERROR(610201122),

    /**
     * [610201123] eSigner: XML Initialisierungsfehler.
     */
    ERIC_CRYPT_E_XML_INIT(610201123),

    /**
     * [610201124] eSigner: Fehler beim Verschl&uuml;sseln.
     */
    ERIC_CRYPT_E_ENCRYPT(610201124),

    /**
     * [610201125] eSigner: Fehler beim Entschl&uuml;sseln.
     */
    ERIC_CRYPT_E_DECRYPT(610201125),

    /**
     * [610201126] eSigner: Keine Signaturkarte eingesteckt (PKCS#11).
     */
    ERIC_CRYPT_E_P11_SLOT_EMPTY(610201126),

    /**
     * [610201127] eSigner: Keine Signatur-/Verschl&uuml;sselungs-Zertifikate/-Schl&uuml;ssel gefunden (PKCS#11).
     */
    ERIC_CRYPT_E_NO_SIG_ENC_KEY(610201127),

    /**
     * [610201128] eSigner: PKCS11 bzw. PC/SC Library fehlt oder ist nicht ausf&uuml;hrbar.
     */
    ERIC_CRYPT_E_LOAD_DLL(610201128),

    /**
     * [610201129] eSigner: Der PC/SC Dienst ist nicht gestartet.
     */
    ERIC_CRYPT_E_NO_SERVICE(610201129),

    /**
     * [610201130] eSigner: Unbekannte Ausnahme aufgetreten.
     */
    ERIC_CRYPT_E_ESICL_EXCEPTION(610201130),

    /**
     * [610201144] eSigner: CA Tokentyp und interner Tokentyp stimmen nicht &uuml;berein.
     */
    ERIC_CRYPT_E_TOKEN_TYPE_MISMATCH(610201144),

    /**
     * [610201146] eSigner: Tempor&auml;res PKCS#12-Token kann nicht erzeugt werden.
     */
    ERIC_CRYPT_E_P12_CREATE(610201146),

    /**
     * [610201147] eSigner: Zertifikatskette konnte nicht verifiziert werden.
     */
    ERIC_CRYPT_E_VERIFY_CERT_CHAIN(610201147),

    /**
     * [610201148] eSigner: PKCS#11 Engine mit anderer Bibliothek belegt.
     */
    ERIC_CRYPT_E_P11_ENGINE_LOADED(610201148),

    /**
     * [610201149] eSigner: Aktion vom Benutzer abgebrochen.
     */
    ERIC_CRYPT_E_USER_CANCEL(610201149),

    /**
     * [610201200] Fehler beim Zugriff auf Zertifikat.
     */
    ERIC_CRYPT_ZERTIFIKAT(610201200),

    /**
     * [610201201] Fehler bei Signaturerzeugung.
     */
    ERIC_CRYPT_SIGNATUR(610201201),

    /**
     * [610201203] Das Format der PSE wird nicht unterst&uuml;tzt.
     */
    ERIC_CRYPT_NICHT_UNTERSTUETZTES_PSE_FORMAT(610201203),

    /**
     * [610201205] F&uuml;r die ausgew&auml;hlte Operation muss eine PIN angegeben werden.
     */
    ERIC_CRYPT_PIN_BENOETIGT(610201205),

    /**
     * [610201206] Die gew&uuml;nschte PIN ist nicht sicher genug (z.B. zu kurz).
     */
    ERIC_CRYPT_PIN_STAERKE_NICHT_AUSREICHEND(610201206),

    /**
     * [610201208] Interner Fehler aufgetreten. Details stehen ggf. im Logfile (eric.log).
     */
    ERIC_CRYPT_E_INTERN(610201208),

    /**
     * [610201209] Der angegebene Zertifikatspfad ist kein Verzeichnis.
     */
    ERIC_CRYPT_ZERTIFIKATSPFAD_KEIN_VERZEICHNIS(610201209),

    /**
     * [610201210] Im angegebenen Verzeichnis existiert bereits ein Bestandteil eines ERiC-Zertifikats.
     */
    ERIC_CRYPT_ZERTIFIKATSDATEI_EXISTIERT_BEREITS(610201210),

    /**
     * [610201211] Die gew&uuml;nschte PIN enth&auml;lt ung&uuml;ltige Zeichen (z.B. Umlaute).
     */
    ERIC_CRYPT_PIN_ENTHAELT_UNGUELTIGE_ZEICHEN(610201211),

    /**
     * [610201212] eSigner: Der Abrufcode besitzt eine falsche Struktur oder enth&auml;lt ung&uuml;ltige Zeichen.
     */
    ERIC_CRYPT_E_INVALID_PARAM_ABC(610201212),

    /**
     * [610201213] Das &uuml;bergebene Zertifikat weist Inkonsistenzen auf und kann deswegen nicht verwendet werden. Bitte verwenden Sie ein anderes oder erzeugen und verwenden Sie ein neues Zertifikat.
     */
    ERIC_CRYPT_CORRUPTED(610201213),

    /**
     * [610201214] Die aufgerufene Funktion unterst&uuml;tzt den neuen Personalausweis (nPA) und den elektronischen Aufenthaltstitel (eAT) nicht.
     */
    ERIC_CRYPT_EIDKARTE_NICHT_UNTERSTUETZT(610201214),

    /**
     * [610201215] Es ist keine Karte/kein Stick eingesteckt
     */
    ERIC_CRYPT_E_SC_SLOT_EMPTY(610201215),

    /**
     * [610201216] Kein unterst&uuml;tztes Applet gefunden
     */
    ERIC_CRYPT_E_SC_NO_APPLET(610201216),

    /**
     * [610201217] Fehler in der Kartensession
     */
    ERIC_CRYPT_E_SC_SESSION(610201217),

    /**
     * [610201218] P11 Signaturzertifikat fehlt
     */
    ERIC_CRYPT_E_P11_NO_SIG_CERT(610201218),

    /**
     * [610201219] P11 Der initiale Tokenzugriff ist fehlgeschlagen
     */
    ERIC_CRYPT_E_P11_INIT_FAILED(610201219),

    /**
     * [610201220] P11 Verschl&uuml;sselungszertifikat fehlt
     */
    ERIC_CRYPT_E_P11_NO_ENC_CERT(610201220),

    /**
     * [610201221] P12 Signaturzertifikat fehlt
     */
    ERIC_CRYPT_E_P12_NO_SIG_CERT(610201221),

    /**
     * [610201222] P12 Verschl&uuml;sselungszertifikat fehlt
     */
    ERIC_CRYPT_E_P12_NO_ENC_CERT(610201222),

    /**
     * [610201223] PC/SC Der Zugriff auf den Entschl&uuml;sselungsschl&uuml;ssel ist fehlgeschlagen
     */
    ERIC_CRYPT_E_SC_ENC_KEY(610201223),

    /**
     * [610201224] PC/SC Signaturzertifikat fehlt
     */
    ERIC_CRYPT_E_SC_NO_SIG_CERT(610201224),

    /**
     * [610201225] PC/SC Verschl&uuml;sselungszertifikat fehlt
     */
    ERIC_CRYPT_E_SC_NO_ENC_CERT(610201225),

    /**
     * [610201226] PC/SC Der initiale Tokenzugriff ist fehlgeschlagen
     */
    ERIC_CRYPT_E_SC_INIT_FAILED(610201226),

    /**
     * [610201227] PC/SC Der Zugriff auf den Signaturschl&uuml;ssel ist fehlgeschlagen
     */
    ERIC_CRYPT_E_SC_SIG_KEY(610201227),

    /**
     * [610301001] Verarbeitung fehlerhaft, keine genaueren Informationen vorhanden.
     */
    ERIC_IO_FEHLER(610301001),

    /**
     * [610301005] Der Dateiaufbau ist nicht korrekt.
     */
    ERIC_IO_DATEI_INKORREKT(610301005),

    /**
     * [610301006] Fehler beim Parsen der Eingabedaten. Details stehen im Logfile (eric.log).
     */
    ERIC_IO_PARSE_FEHLER(610301006),

    /**
     * [610301007] Die Generierung des Nutzdatensatzes ist fehlgeschlagen.
     */
    ERIC_IO_NDS_GENERIERUNG_FEHLGESCHLAGEN(610301007),

    /**
     * [610301010] Interner Fehler, der Masterdatenservice ist nicht verf&uuml;gbar.
     */
    ERIC_IO_MASTERDATENSERVICE_NICHT_VERFUEGBAR(610301010),

    /**
     * [610301014] Es wurden ung&uuml;ltige Steuerzeichen im Nutzdatensatz gefunden.
     */
    ERIC_IO_STEUERZEICHEN_IM_NDS(610301014),

    /**
     * [610301031] Die Versionsinformationen der ERiC-Bibliotheken konnten nicht ausgelesen werden.
     */
    ERIC_IO_VERSIONSINFORMATIONEN_NICHT_GEFUNDEN(610301031),

    /**
     * [610301104] Der Wert im Transferheader-Element &quot;Verfahren&quot; wird vom verwendeten Reader nicht unterst&uuml;tzt.
     */
    ERIC_IO_FALSCHES_VERFAHREN(610301104),

    /**
     * [610301105] Es wurde mehr als ein Steuerfall in der Eingabedatei gefunden.
     */
    ERIC_IO_READER_MEHRFACHE_STEUERFAELLE(610301105),

    /**
     * [610301106] Es wurden unerwartete Elemente in der Eingabedatei gefunden, Details stehen ggf. im Logfile (eric.log).
     */
    ERIC_IO_READER_UNERWARTETE_ELEMENTE(610301106),

    /**
     * [610301107] Es wurden formale Fehler in der Eingabedatei gefunden, Details stehen ggf. im Logfile (eric.log).
     */
    ERIC_IO_READER_FORMALE_FEHLER(610301107),

    /**
     * [610301108] Die Eingabedaten lagen nicht im Encoding UTF-8 vor, oder es war kein Encoding spezifiziert.
     */
    ERIC_IO_READER_FALSCHES_ENCODING(610301108),

    /**
     * [610301109] Es wurde mehr als ein &quot;Nutzdaten&quot;-Element in der Eingabedatei gefunden.
     */
    ERIC_IO_READER_MEHRFACHE_NUTZDATEN_ELEMENTE(610301109),

    /**
     * [610301110] Es wurde mehr als ein Nutzdatenblock in der Eingabedatei gefunden.
     */
    ERIC_IO_READER_MEHRFACHE_NUTZDATENBLOCK_ELEMENTE(610301110),

    /**
     * [610301111] Der im Transferheader-Element &quot;Datenart&quot; angegebene Wert ist unbekannt.
     */
    ERIC_IO_UNBEKANNTE_DATENART(610301111),

    /**
     * [610301114] Ung&uuml;ltiger oder fehlender Wert f&uuml;r den Untersachbereich.
     */
    ERIC_IO_READER_UNTERSACHBEREICH_UNGUELTIG(610301114),

    /**
     * [610301115] Es wurden zu viele Nutzdatenbl&ouml;cke in der Eingabedatei gefunden.
     */
    ERIC_IO_READER_ZU_VIELE_NUTZDATENBLOCK_ELEMENTE(610301115),

    /**
     * [610301150] Es wurden ung&uuml;ltige Steuerzeichen im TransferHeader-Element gefunden.
     */
    ERIC_IO_READER_STEUERZEICHEN_IM_TRANSFERHEADER(610301150),

    /**
     * [610301151] Es wurden ung&uuml;ltige Steuerzeichen im NutzdatenHeader-Element gefunden.
     */
    ERIC_IO_READER_STEUERZEICHEN_IM_NUTZDATENHEADER(610301151),

    /**
     * [610301152] Es wurden ung&uuml;ltige Steuerzeichen im Nutzdaten-Element gefunden.
     */
    ERIC_IO_READER_STEUERZEICHEN_IN_DEN_NUTZDATEN(610301152),

    /**
     * [610301200] Es traten Fehler beim Validieren des XML auf. Details stehen im Logfile (eric.log).
     */
    ERIC_IO_READER_SCHEMA_VALIDIERUNGSFEHLER(610301200),

    /**
     * [610301201] Eine XML-Entity konnte nicht aufgel&ouml;st werden.
     */
    ERIC_IO_READER_UNBEKANNTE_XML_ENTITY(610301201),

    /**
     * [610301252] Im XML-String konnte der Text &quot;&lt;DatenTeil&gt;&quot; nicht gefunden werden.
     */
    ERIC_IO_DATENTEILNOTFOUND(610301252),

    /**
     * [610301253] Im XML-String konnte der Text &quot;&lt;/DatenTeil&gt;&quot; nicht gefunden werden.
     */
    ERIC_IO_DATENTEILENDNOTFOUND(610301253),

    /**
     * [610301300] Falsche &Uuml;bergabeparameter f&uuml;r die Funktion.
     */
    ERIC_IO_UEBERGABEPARAMETER_FEHLERHAFT(610301300),

    /**
     * [610301400] Der Parameter enth&auml;lt ung&uuml;ltige UTF-8 Multibytesequenzen.
     */
    ERIC_IO_UNGUELTIGE_UTF8_SEQUENZ(610301400),

    /**
     * [610301401] Der Parameter enth&auml;lt mindestens ein unzul&auml;ssiges Zeichen.
     */
    ERIC_IO_UNGUELTIGE_ZEICHEN_IN_PARAMETER(610301401),

    /**
     * [610501001] Verarbeitung fehlerhaft, keine genaueren Informationen vorhanden.
     */
    ERIC_PRINT_INTERNER_FEHLER(610501001),

    /**
     * [610501002] Keine Druckvorlage f&uuml;r die angegebene Kombination aus Unterfallart und Veranlagungszeitraum gefunden. Bitte pr&uuml;fen Sie die installierten Druckvorlagen.
     */
    ERIC_PRINT_DRUCKVORLAGE_NICHT_GEFUNDEN(610501002),

    /**
     * [610501004] Es wurde ein falscher Dateipfad angegeben, es fehlen Zugriffsrechte oder die Datei wird aktuell von einer anderen Anwendung verwendet.
     */
    ERIC_PRINT_UNGUELTIGER_DATEI_PFAD(610501004),

    /**
     * [610501007] ERiCPrint wurde nicht richtig initialisiert. Eventuell wurde ERiC nicht richtig initialisiert?
     */
    ERIC_PRINT_INITIALISIERUNG_FEHLERHAFT(610501007),

    /**
     * [610501008] Das zu verwendende Format bzw. der Zielklient sind nicht bekannt.
     */
    ERIC_PRINT_AUSGABEZIEL_UNBEKANNT(610501008),

    /**
     * [610501009] Der Beginn des Ausdruckprozesses schlug fehl. Eventuell konnten notwendige Ressourcen nicht allokiert werden.
     */
    ERIC_PRINT_ABBRUCH_DRUCKVORBEREITUNG(610501009),

    /**
     * [610501010] W&auml;hrend der Ausgabe der Inhalte ist ein Fehler aufgetreten.
     */
    ERIC_PRINT_ABBRUCH_GENERIERUNG(610501010),

    /**
     * [610501011] Die Kombination aus Unterfallart und Veranlagungszeitraum wird nicht unterst&uuml;tzt.
     */
    ERIC_PRINT_STEUERFALL_NICHT_UNTERSTUETZT(610501011),

    /**
     * [610501012] Der &uuml;bergebene Fu&szlig;text ist zu lang.
     */
    ERIC_PRINT_FUSSTEXT_ZU_LANG(610501012);

    private static final Errorcode[] ALL_ERRORCODES = Errorcode.values();
    
    private final int errorcode;
    
    Errorcode(final int errorcode) {
        this.errorcode = errorcode;
    }
    
    /**
    * Gibt den Fehlercode zu einem Intergerwert zurück
    *
    * @param rc Fehlercode-Nummer
    * @return Errorcode Klasseninstanz
    */
    public static Errorcode getErrorcode(final int rc) {
        for (final Errorcode curr : ALL_ERRORCODES) {
            if (curr.getErrorcode() == rc) {
                return curr;
            }
         }
         
         throw new IllegalArgumentException("Unbekannter Fehlercode: "+rc);
    }
    
    /**
    * Gibt die Integerrepräsentation des Fehlercodes zurück
    *
    * @return Fehlercode als Integer
    */
    public int getErrorcode() {
        return errorcode;
    }
    
    public String toString() {
        return name();
    }
}
