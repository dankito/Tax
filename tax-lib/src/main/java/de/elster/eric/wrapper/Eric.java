package de.elster.eric.wrapper;

import com.sun.jna.*;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.win32.StdCallLibrary;
import de.elster.eric.wrapper.exception.EricException;
import de.elster.eric.wrapper.exception.WrapperException;
import de.elster.eric.wrapper.jna.EricapiLibrary;
import de.elster.eric.wrapper.jna.EricapiLibrary.eric_druck_parameter_t;
import de.elster.eric.wrapper.jna.EricapiLibrary.eric_verschluesselungs_parameter_t;

import java.io.Closeable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Eric implements Closeable {
	private EricapiLibrary ericapiLib;

	/**
	 * Den ERiC laden und initialisieren.
	 *
	 * @param ericPath
	 *            In diesem Verzeichnis werden die ERiC Bibliotheken und die
	 *            Unterverzeichnisse „plugins“ und „plugins2“ erwartet.
	 * @param logPath
	 *            Das Verzeichnis für ERiC Log-Dateien. Es muss existieren und
	 *            beschreibbar sein. ERiC erzeugt keine Log-Dateien falls dieses
	 *            Verzeichnis nicht schreibbar ist.
	 * @throws EricException
	 *             Falls ein Fehler beim Konfigurieren von ERiC auftritt.
	 * @throws WrapperException
	 *             Falls ein Fehler im Java-Wrapper auftritt, z.B. wenn ERiC im
	 *             Verzeichnis {@code ericPath} nicht gefunden werden kann.
	 */
	public Eric(final String ericPath, final String logPath) throws EricException, WrapperException {
		final String newEncoding = "UTF-8";
		final String prevEncoding = System.setProperty("jna.encoding", newEncoding);

		if (prevEncoding != null && !prevEncoding.equalsIgnoreCase(newEncoding)) {
			throw new WrapperException("Das Java-System-property jna.encoding war auf " + prevEncoding
					+ " gesetzt. Eine andere Komponente, die dieses Encoding benötigt, wird beeinträchtigt");
		}

		// So verhindern wir, dass der JNA sich die jnidispatch
		// Bibliothek nicht von anderswo her als dem mitgelieferten
		// Repository holt:
		System.setProperty("jna.nosys", "true");

		loadEricapi(ericPath, logPath);
	}

	@Override
	public void close() {
		if (ericapiLib != null) {
			unregisterLogCallback();
			ericapiLib.EricBeende();
			ericapiLib = null;
		}
	}

	/**
	 * Validiert einen Steuerfall.
	 *
	 * @param datenartVersion
	 *            DatenartVersion des Steuerfalls. Siehe
	 *            common\Datenartversionmatrix.xml und
	 *            ERiC-Entwicklerhandbuch.pdf.
	 * @param edsXml
	 *            Enthält die zu verarbeitenden XML-Daten.
	 * @return Ergebnis-Klasse
	 *
	 * @throws EricException
	 *             Bei einem Fehler wird eine EricException geworfen, z.B. wenn
	 *             ERiC Fehler in dem übergebenen Steuerfall findet.
	 * @throws WrapperException
	 *             Bei Fehlern in der Wrapperschicht.
	 */
	public SendResponse validate(final String datenartVersion, final String edsXml) throws EricException, WrapperException {
		return processTaxCase(datenartVersion, edsXml, EricapiLibrary.ERIC_VALIDIERE, null, null, null);
	}

	/**
	 * Validiert und druckt einen Steuerfall.
	 *
	 * @param datenartVersion
	 *            DatenartVersion des Steuerfalls. Siehe
	 *            common\Datenartversionmatrix.xml und
	 *            ERiC-Entwicklerhandbuch.pdf.
	 * @param edsXml
	 *            Enthält die zu verarbeitenden XML-Daten.
	 * @param printParams
	 *            Parameter zum Druck. Unter anderem Angabe des
	 *            Ausgabe-PDF-Pfads.
	 * @return Ergebnis-Klasse
	 *
	 * @throws EricException
	 *             Bei einem Fehler wird eine EricException geworfen, z.B. wenn
	 *             ERiC Fehler in dem übergebenen Steuerfall findet.
	 * @throws WrapperException
	 *             Bei Fehlern in der Wrapperschicht.
	 */
	public String print(final String datenartVersion, final String edsXml, final PrintParameters printParams) throws EricException,
			WrapperException {
		return processTaxCase(datenartVersion, edsXml, EricapiLibrary.ERIC_DRUCKE, null, null, printParams).getEricResult();
	}

	/**
	 * Validiert und sendet einen Steuerfall.
	 *
	 * @param datenartVersion
	 *            DatenartVersion des Steuerfalls. Siehe
	 *            common\Datenartversionmatrix.xml und
	 *            ERiC-Entwicklerhandbuch.pdf.
	 * @param edsXml
	 *            Enthält die zu verarbeitenden XML-Daten.
	 * @param certParams
	 *            Wenn nicht null Parameter bzgl. des bei authentifizertem
	 *            Versand zu benutzenden Zertifikats. Unter andem Pfad und PIN
	 *            des Zertifikats.
	 * @return Antwort des Annahmeservers
	 *
	 * @throws EricException
	 *             Bei einem Fehler wird eine EricException geworfen, z.B. wenn
	 *             ERiC Fehler in dem übergebenen Steuerfall findet.
	 * @throws WrapperException
	 *             Bei Fehlern in der Wrapperschicht.
	 */
	public SendResponse send(final String datenartVersion, final String edsXml, final CertificateParameters certParams)
			throws EricException, WrapperException {
		return processTaxCase(datenartVersion, edsXml, EricapiLibrary.ERIC_SENDE, null, certParams, null);
	}

	/**
	 * Validiert, sendet und druckt einen Steuerfall.
	 *
	 * @param datenartVersion
	 *            DatenartVersion des Steuerfalls. Siehe
	 *            common\Datenartversionmatrix.xml und
	 *            ERiC-Entwicklerhandbuch.pdf.
	 * @param edsXml
	 *            Enthält die zu verarbeitenden XML-Daten.
	 * @param certParams
	 *            Wenn nicht null Parameter bzgl. des bei authentifizertem
	 *            Versand zu benutzenden Zertifikats. Unter andem Pfad und PIN
	 *            des Zertifikats.
	 * @param printParams
	 *            Parameter zum Druck. Unter anderem Angabe des
	 *            Ausgabe-PDF-Pfads.
	 * @return Antwort des Annahmeservers
	 *
	 * @throws EricException
	 *             Bei einem Fehler wird eine EricException geworfen, z.B. wenn
	 *             ERiC Fehler in dem übergebenen Steuerfall findet.
	 * @throws WrapperException
	 *             Bei Fehlern in der Wrapperschicht.
	 */
	public SendResponse sendAndPrint(final String datenartVersion, final String edsXml, final CertificateParameters certParams,
			final PrintParameters printParams) throws EricException, WrapperException {
		return processTaxCase(datenartVersion, edsXml, EricapiLibrary.ERIC_SENDE | EricapiLibrary.ERIC_DRUCKE, null, certParams,
				printParams);
	}

	public EricapiLibrary getEricApiLib() {
		return this.ericapiLib;
	}

	/**
	 * Siehe EricHoleFinanzamtLandNummern in ericapi.h
	 *
	 * @return Gibt die Liste aller Finanzamtlandnummern zurück.
	 * @throws EricException
	 *             Falls ERiC einen Fehler zurückgibt.
	 * @throws WrapperException
	 *             Bei Fehlern in der Wrapperschicht. Z.B. beim Auswerten der
	 *             XML-Antwort von ERiC
	 */
	public List<FinanzamtLand> getFinanzamtLaender() throws EricException, WrapperException {
		final PointerByReference returnBuffer = ericapiLib.EricRueckgabepufferErzeugen();
		try {
			final int rc = ericapiLib.EricHoleFinanzamtLandNummern(returnBuffer);
			if (rc != Errorcode.ERIC_OK.getErrorcode()) {
				Helper.throwRcException(ericapiLib, rc);
			}

			final String xmlData = ericapiLib.EricRueckgabepufferInhalt(returnBuffer);
			return EricXml.parseFinanzamtLaender(xmlData);
		} finally {
			ericapiLib.EricRueckgabepufferFreigeben(returnBuffer);
		}
	}

	/**
	 * Siehe EricHoleFinanzaemter in ericapi.h
	 *
	 * @param finanzamtLandId
	 *            Länder-ID, für welche die Finanzämter zurückgegeben werden
	 * @return Gibt die Finanzamtliste für eine bestimmte Finanzamtlandnummer
	 *         zurück.
	 * @throws EricException
	 *             Falls ERiC einen Fehler zurückgibt.
	 * @throws WrapperException
	 *             Bei Fehlern in der Wrapperschicht. Z.B. beim Auswerten der
	 *             XML-Antwort von ERiC
	 */
	public List<Finanzamt> getFinanzaemter(final int finanzamtLandId) throws EricException, WrapperException {
		final PointerByReference returnBuffer = ericapiLib.EricRueckgabepufferErzeugen();
		try {
			final int rc = ericapiLib.EricHoleFinanzaemter("" + finanzamtLandId, returnBuffer);
			if (rc != Errorcode.ERIC_OK.getErrorcode()) {
				Helper.throwRcException(ericapiLib, rc);
			}

			final String xmlData = ericapiLib.EricRueckgabepufferInhalt(returnBuffer);
			return EricXml.parseFinanzaemter(xmlData);
		} finally {
			ericapiLib.EricRueckgabepufferFreigeben(returnBuffer);
		}
	}

	/**
	 * Holt die Eigenschaften eines Finanzamts.
	 *
	 * Siehe EricHoleFinanzamtsdaten in ericapi.h
	 *
	 * @param bufaNr
	 *            Bundesfinanzamtnummer des Finanzamts.
	 * @return Eigenschaften des Finanzamts als XML
	 * @throws EricException
	 *             Falls das Holen der Daten fehlschlägt
	 */
	public String getFinanzamtsdaten(final int bufaNr) throws EricException {
		final PointerByReference returnBuffer = ericapiLib.EricRueckgabepufferErzeugen();
		try {
			final int rc = ericapiLib.EricHoleFinanzamtsdaten(String.valueOf(bufaNr), returnBuffer);
			if (rc != Errorcode.ERIC_OK.getErrorcode()) {
				Helper.throwRcException(ericapiLib, rc);
			}

			final String xmlData = ericapiLib.EricRueckgabepufferInhalt(returnBuffer);
			return xmlData;
		} finally {
			ericapiLib.EricRueckgabepufferFreigeben(returnBuffer);
		}
	}

	/**
	 * Setzt eine ERiC-Einstellung.
	 *
	 * Siehe EricEinstellungSetzen in ericapi.h
	 *
	 * @param name
	 *            Name der Einstellung
	 * @param value
	 *            Neuer Wert der Einstellung
	 * @throws EricException
	 *             Falls ERiC einen Fehler zurückgibt.
	 */
	public void setSetting(final String name, final String value) throws EricException {
		final int rc = ericapiLib.EricEinstellungSetzen(name, value);
		if (rc != Errorcode.ERIC_OK.getErrorcode()) {
			Helper.throwRcException(ericapiLib, rc);
		}
	}

	/**
	 * Setzt eine ERiC-Pfadeinstellung
	 *
	 * @param name
	 *            Name der Einstellung
	 * @param path
	 *            Neuer Wert der Einstellung
	 * @throws EricException
	 *             Falls ERiC einen Fehler zurückgibt.
	 * @throws WrapperException
	 *             Bei Fehlern in der Wrapperschicht
	 */
	public void setPath(final String name, final String path) throws WrapperException, EricException {
		final int rc = ericapiLib.EricEinstellungSetzen(name, Helper.encodePath(path));
		if (rc != Errorcode.ERIC_OK.getErrorcode()) {
			Helper.throwRcException(ericapiLib, rc);
		}
	}

	/**
	 * Prüft eine ELSTER-Steuernummer auf Korrektheit.
	 *
	 * Siehe EricPruefeSteuernummer in ericapi.h
	 *
	 * @param stNr
	 *            ELSTER-Steuernummer
	 * @return ERIC_OK oder Fehlercode
	 */
	public Errorcode checkStNr(final String stNr) {
		return Errorcode.getErrorcode(ericapiLib.EricPruefeSteuernummer(stNr));
	}

	/**
	 * Es wird eine Steuernummer im ELSTER-Steuernummerformat erzeugt.
	 * Die Funktion erzeugt aus einer angegebenen Steuernummer im Format des
	 * Steuerbescheides eine 13-stellige Steuernummer im ELSTER-Steuernummerformat.
	 * Die sich ergebende 13-stellige Steuernummer im ELSTER-Steuernummerformat wird
	 * von der Funktion EricMakeElsterStnr() auch auf Gültigkeit geprüft.
	 * Einer der beiden Parameter landesnr oder bundesfinanzamtsnr muss korrekt
	 * angegeben werden. Der jeweils andere Parameter darf NULL oder leer sein. Bei
	 * bayerischen und berliner Steuernummern im Format BBB/UUUUP ist die Angabe der
	 * Bundesfinanzamtsnummer zwingend erforderlich.
	 *
	 * @param steuernrBescheid Format der Steuernummer wie auch auf amtlichen Schreiben angegeben.
	 * @param bundesfinanzamtsnr 4-stellige Bundesfinanzamtsnummer.
	 * @return Die Steuernummer im ELSTER-Steuernummerformat oder eine Exception mit diesen Fehlercodes:
	 * ERIC_GLOBAL_STEUERNUMMER_UNGUELTIG,
	 * ERIC_GLOBAL_LANDESNUMMER_UNBEKANNT,
	 * ERIC_GLOBAL_NULL_PARAMETER,
	 * ERIC_GLOBAL_UNGUELTIGER_PARAMETER,
	 * ERIC_GLOBAL_NICHT_GENUEGEND_ARBEITSSPEICHER,
	 * ERIC_GLOBAL_UNKNOWN
	 */
	public String makeElsterSteuernummer(String steuernrBescheid, String bundesfinanzamtsnr) throws EricException {
		final PointerByReference returnBuffer = ericapiLib.EricRueckgabepufferErzeugen();
		try {
			final int rc = ericapiLib.EricMakeElsterStnr(steuernrBescheid, null, bundesfinanzamtsnr, returnBuffer);
			if (rc != Errorcode.ERIC_OK.getErrorcode()) {
				Helper.throwRcException(ericapiLib, rc);
			}

			return ericapiLib.EricRueckgabepufferInhalt(returnBuffer);
		} finally {
			ericapiLib.EricRueckgabepufferFreigeben(returnBuffer);
		}
	}


	/**
	 * Gibt die Zertifikateigenschaften als XML zurück.
	 *
	 * Siehe EricHoleZertifikatEigenschaften in ericapi.h
	 *
     * @param  ericCert
     *             Certificate zu dem die Eigenschaften ermittelt werden
	 * @throws EricException
	 *             Falls ERiC einen Fehler zurückgibt.
	 * @throws WrapperException
	 * 			   Bei Fehlern in der Wrapperschicht.
	 * @return Zertifikateigenschaften als XML
	 *
	 */
	public String getCertInfoXml(EricCertificate ericCert) throws WrapperException, EricException
	{
		final PointerByReference responseBuffer = ericapiLib.EricRueckgabepufferErzeugen();

		try
		{
			ericapiLib.EricHoleZertifikatEigenschaften (ericCert.getCertificateHandle().getValue(), ericCert.getPin(), responseBuffer);

			return ericapiLib.EricRueckgabepufferInhalt(responseBuffer);
		}
		finally
		{
			ericapiLib.EricRueckgabepufferFreigeben(responseBuffer);
		}
	}

	/**
	 * Gibt den Fehlertext zu einem ERiC-Statuscode zurück.
	 *
	 * Siehe EricHoleFehlerText in ericapi.h
	 *
   * @param  ericStatusCode
   *         Statuscode, zu dem der passende Text geholt werden soll
	 * @throws EricException
	 *         Falls ERiC einen Fehler zurückgibt.
	 * @throws WrapperException
	 * 			   Bei Fehlern in der Wrapperschicht.
	 * @return Fehlertext
	 *
	 */
	public String getErrorMessage(int ericStatusCode) throws WrapperException, EricException
	{
		final PointerByReference responseBuffer = ericapiLib.EricRueckgabepufferErzeugen();

		try
		{
			ericapiLib.EricHoleFehlerText (ericStatusCode, responseBuffer);
			return ericapiLib.EricRueckgabepufferInhalt(responseBuffer);
		}
		finally
		{
			ericapiLib.EricRueckgabepufferFreigeben(responseBuffer);
		}
	}

	public void registerLogCallback(final LogCallback logCallback, boolean alsoWriterToEricLogFile) {
		EricapiLibrary.EricLogCallback ericLogCallback = new EricapiLibrary.EricLogCallback() {
			@Override
			public void invoke(String kategorie, int logLevel, String nachricht, Pointer benutzerDaten) {
				logCallback.log(kategorie, mapLogLevel(logLevel), nachricht);
			}
		};

		ericapiLib.EricRegistriereLogCallback(ericLogCallback, alsoWriterToEricLogFile ? 1 : 0, null);
	}

	private EricLogLevel mapLogLevel(int logLevelInt) {
		for (EricLogLevel logLevel : EricLogLevel.values()) {
			if (logLevel.level == logLevelInt) {
				return logLevel;
			}
		}

		return EricLogLevel.INFO; // should never came to this
	}

	public void unregisterLogCallback() {
		ericapiLib.EricRegistriereLogCallback(null, 1, null);
	}


	/**
	 * Verarbeitung eines Steuerfalls via EricBearbeiteVorgang.
	 *
	 * Siehe EricBearbeiteVorgang in ericapi.h
	 *
	 * @param datenartVersion
	 *            Datenartversion der Elster-XML
	 * @param edsXml
	 *            Elster-XML
	 * @param flags
	 *            Verarbeitungsflags
	 * @param transferHandle
	 *            Handle um Transfers zu bündeln
	 * @param certificateParams
	 *            Parameter bzgl. Zertifikat für authentifizierten Versand
	 * @param printParams
	 *            Parameter bzgl. PDF-Druck
	 * @return ERiC- und Serverrückgabe
	 * @throws EricException
	 *             Bei einem Fehler wird eine EricException geworfen, z.B. wenn
	 *             ERiC Fehler in dem übergebenen Datensatz findet.
	 * @throws WrapperException
	 *             Bei Fehlern in der Wrapperschicht.
	 */
	private synchronized SendResponse processTaxCase(final String datenartVersion, final String edsXml, final int flags,
			final IntByReference transferHandle, final CertificateParameters certificateParams, final PrintParameters printParams)
			throws EricException, WrapperException {
		final PointerByReference ericResponseBuffer = ericapiLib.EricRueckgabepufferErzeugen();
		final PointerByReference serverResponseBuffer = ericapiLib.EricRueckgabepufferErzeugen();

		try (final CallbackHandler handler = new CallbackHandler(ericapiLib)) {
			eric_druck_parameter_t druckParams = createDruckParameter(printParams);

			eric_verschluesselungs_parameter_t cryptoParams = null;
			if (certificateParams != null) {
				cryptoParams = createVerschluesselungsParameter(certificateParams);
			}

			final int rc = ericapiLib.EricBearbeiteVorgang(edsXml, datenartVersion, flags, druckParams, cryptoParams, transferHandle,
					ericResponseBuffer, serverResponseBuffer);

			if (rc != Errorcode.ERIC_OK.getErrorcode()) {
				if (    (rc == Errorcode.ERIC_GLOBAL_PRUEF_FEHLER.getErrorcode())
                     || (rc == Errorcode.ERIC_TRANSFER_ERR_XML_THEADER.getErrorcode())
                     || (rc == Errorcode.ERIC_TRANSFER_ERR_XML_NHEADER.getErrorcode()) )
                {
					Helper.throwResponseException(ericapiLib, rc, ericResponseBuffer, serverResponseBuffer);
				} else {
					Helper.throwRcException(ericapiLib, rc);
				}
			}

			return new SendResponse(rc, ericapiLib.EricRueckgabepufferInhalt(ericResponseBuffer),
					ericapiLib.EricRueckgabepufferInhalt(serverResponseBuffer));
		} finally {
			ericapiLib.EricRueckgabepufferFreigeben(ericResponseBuffer);
			ericapiLib.EricRueckgabepufferFreigeben(serverResponseBuffer);
		}
	}

	private eric_verschluesselungs_parameter_t createVerschluesselungsParameter(final CertificateParameters certificateParams
			) throws WrapperException, EricException {
		if (certificateParams.getCertificate().getCertificateHandle().getValue() != 0) {
			eric_verschluesselungs_parameter_t cryptoParams = new eric_verschluesselungs_parameter_t();
			cryptoParams.version = 2;
			cryptoParams.abrufCode = certificateParams.getAbrufCode();
			cryptoParams.pin = certificateParams.getPin();
			cryptoParams.zertifikatHandle = certificateParams.getCertificate().getCertificateHandle().getValue();
			return cryptoParams;
		} else {
			return null;
		}
	}

	private eric_druck_parameter_t createDruckParameter(final PrintParameters printParams) throws WrapperException {
		eric_druck_parameter_t druckParams = null;
		if (printParams != null) {
			druckParams = new eric_druck_parameter_t();
			druckParams.version = 2;
			druckParams.ersteSeite = printParams.getErsteSeite();
			druckParams.duplexDruck = printParams.getDuplexDruck();
			druckParams.fussText = printParams.getFussText();
			druckParams.vorschau = printParams.getVorschau();
			if (printParams.getPdfName() != null) {
				final byte[] pdfName = Helper.encodePath(printParams.getPdfName());
				druckParams.pdfName = new Memory(pdfName.length + 1);
				for (int i = 0; i < pdfName.length; ++i) {
					druckParams.pdfName.setByte(i, pdfName[i]);
				}
				druckParams.pdfName.setByte(pdfName.length, (byte) 0);
			}
		}
		return druckParams;
	}

	/**
	 * Lädt die ericapi.dll/libericapi.so/libericapi.dylib und erstellt das
	 * JNA-Wrapperobjekt
	 *
	 * @param ericPath
	 *            Pfad in dem sich die ericapi.dll befindet
	 * @throws WrapperException
	 *             Bei einem Fehler, wenn die ericapi.dll nicht geladen werden
	 *             kann.
	 */
	private void loadEricapi(final String ericPath, final String logPath) throws WrapperException {
		final String libraryName = Helper.getOsSharedLibraryName("ericapi");
		final Path ericapiPath = Paths.get(ericPath, libraryName);

		if (!Files.exists(ericapiPath)) {
			throw new WrapperException(libraryName + " wurde in Pfad \"" + ericPath + "\" nicht gefunden");
		}

		final Map<String, String> options = new HashMap<>();
		// Funktioniert nicht vollständig mit JNA 4.0.0
		options.put(Library.OPTION_STRING_ENCODING, "UTF-8");
		if ("32".equals(System.getProperty("sun.arch.data.model"))) {
			options.put(Library.OPTION_CALLING_CONVENTION, "" + StdCallLibrary.STDCALL_CONVENTION);
		}

		try {
			ericapiLib = (EricapiLibrary) Native.loadLibrary(ericapiPath.normalize().toString(), EricapiLibrary.class, options);
		} catch (UnsatisfiedLinkError e) {
			throw new WrapperException("ericapi konnte nicht geladen werden! "
					+ "Bitte stellen Sie sicher, dass ihre Java-Installation in "
					+ "der gleichen CPU-Architektur vorliegt wie ERiC (x86/AMD64).", e);
		}

		if (ericapiLib == null) {
			throw new WrapperException("Fehler beim Laden des JNA-Wrapperobjekts");
		}

		ericapiLib.EricInitialisiere(ericPath, logPath);
	}

	/**
	 * Klasse zum Kapseln der Server- und ERiC Antwort eines
	 * EricBearbeiteVorgang Aufrufs.
	 *
	 */
	public static class SendResponse {
		private final String ericResult;

		/** Antwort des Annahmeservers */
		private final String serverResponse;

    /** Statuscode des Versands */
    private final int ericStatusCode;

		protected SendResponse(final int ericStatusCode, final String ericResult, final String serverResponse) {
			this.ericResult = ericResult;
			this.serverResponse = serverResponse;
			this.ericStatusCode = ericStatusCode;
		}

		public String getEricResult() {
			return ericResult;
		}

		public String getServerResponse() {
			return serverResponse;
		}

		public int getStatus() {
			return ericStatusCode;
		}
	}
}
