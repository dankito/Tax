package de.elster.eric.wrapper.exception;

/**
 * Exception-Klasse für Fehlermeldungen, bei denen eine Serverantwort vorhanden ist.
 */
public class ResponseException extends EricException {

	private static final long serialVersionUID = 1766161277337242446L;

	private final String ericResult;
	private final String serverResponse;
	
	public ResponseException(final int rc, final String errorMessage, final String ericResult, final String serverResponse) {
		super(rc, errorMessage);
        this.ericResult = ericResult;
		this.serverResponse = serverResponse;
	}

	public String getEricResult() {
		return ericResult;
	}

	public String getServerResponse() {
		return serverResponse;
	}

	public String toString() {
	    final String eR = ericResult.isEmpty() ? "[leer]" : ericResult;
		final String sR = serverResponse.isEmpty() ? "[leer]" : serverResponse;
		return "Rückgabe:\n" + eR + "\n\nServerantwort:\n" + sR;
	}
}
