package de.elster.eric.wrapper;

import java.util.Arrays;

import com.sun.jna.Pointer;

import de.elster.eric.wrapper.jna.EricapiLibrary;
import de.elster.eric.wrapper.jna.EricapiLibrary.EricFortschrittCallback;

public class CallbackHandler implements AutoCloseable {
	private static final int SCREEN_WIDTH = 78;

	private static final int ID_EINLESEN    = 10;
	private static final int ID_VORBEREITEN = 20;
	private static final int ID_VALIDIEREN  = 30;
	private static final int ID_SENDEN      = 40;
	private static final int ID_DRUCKEN     = 50;

	private EricapiLibrary ericapiLib;
	private int lastId;
	private int lastColumn;

	public CallbackHandler(EricapiLibrary ericapiLib) {
		this.ericapiLib = ericapiLib;
		registerCallbacks();
	}

	public void close() {
		closeFortschrittCallback();
		unregisterCallbacks();
	}

	private void registerCallbacks() {
		ericapiLib.EricRegistriereGlobalenFortschrittCallback(new EricFortschrittCallback() {
			public void invoke(int id, int pos, int max, Pointer benutzerDaten) {
				callGlobalenFortschrittCallback(id, pos, max);
			}
		}, null);
		ericapiLib.EricRegistriereFortschrittCallback(new EricFortschrittCallback() {
			public void invoke(int id, int pos, int max, Pointer benutzerDaten) {
				callFortschrittCallback(id, pos, max);
			}
		}, null);
	}

	private void unregisterCallbacks() {
		ericapiLib.EricRegistriereGlobalenFortschrittCallback(null, null);
		ericapiLib.EricRegistriereFortschrittCallback(null, null);
	}

	private void callGlobalenFortschrittCallback(int id, int pos, int max) {
		System.out.printf("%d/%d: %s\n", pos, max, mapIdToString(id));
	}

	private void callFortschrittCallback(int id, int pos, int max) {
		if (lastId != 0 && lastId != id) {
			closeFortschrittCallback();
			callFortschrittCallback(id, pos, max);
		} else {
			if (pos == 0) {
				System.out.print("[");
				lastId = id;
				lastColumn = 0;
			} else if (pos == max) {
				System.out.println(repeat('#', SCREEN_WIDTH - lastColumn) + "]");
				lastId = 0;
				lastColumn = 0;
			} else {
				int column = (pos * SCREEN_WIDTH) / max;
				System.out.print(repeat('#', column - lastColumn));
				lastColumn = column;
			}
		}
	}

	private void closeFortschrittCallback() {
		if (lastId != 0) {
			System.out.println(repeat('#', SCREEN_WIDTH - lastColumn) + "]");
			lastId = 0;
			lastColumn = 0;
		}
	}
	
	private static String mapIdToString(int id) {
		switch (id) {
		case ID_EINLESEN:
			return "XML einlesen";
		case ID_VORBEREITEN:
			return "Versand vorbereiten";
		case ID_VALIDIEREN:
			return "Validieren";
		case ID_SENDEN:
			return "Versenden";
		case ID_DRUCKEN:
			return "Drucken";
		default:
			return "Unbekannter Verarbeitungsschritt";
		}
	}

	private static String repeat(char c, int num) {
		char[] cs = new char[num];
		Arrays.fill(cs, c);
		return new String(cs);
	}
}
