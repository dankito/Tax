package de.elster.eric.wrapper;

/**
 * Klasse zur Repräsentation eines Finanzamt-Lands
 */
public class FinanzamtLand {

	public final String land;

	public final int id;

	public FinanzamtLand(final String land, final int id) {
		this.land = land;
		this.id = id;
	}

	public String getLand() {
		return land;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return " ID=" + id + " Land=" + land;
	}
}
