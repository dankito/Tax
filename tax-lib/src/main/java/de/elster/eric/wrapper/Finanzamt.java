package de.elster.eric.wrapper;

/**
 * Klasse zur Repräsentation eines Finanzamts
 * 
 */
public class Finanzamt {
	
	private final String name;

	private final int id;

	protected Finanzamt(final String name, final int id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

    /** @return Bundesfinanzamtnummer */
	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return " ID=" + id + " Name=" + name;
	}
}
