package de.elster.eric.wrapper.exception;

/**
 * Exceptionklasse für Fehler die im Java-Wrapper auftreten. Z.B. wenn die
 * ericapi-Bibliothek nicht gefunden werden kann.
 */
public class WrapperException extends Exception {

	private static final long serialVersionUID = 6262168713530251181L;

	public WrapperException(final String msg) {
		super(msg);
	}

	public WrapperException(final String msg, final Throwable cause) {
		super(msg, cause);
	}

}
