package de.elster.eric.wrapper;

import com.sun.jna.ptr.PointerByReference;
import de.elster.eric.wrapper.exception.EricException;
import de.elster.eric.wrapper.exception.ResponseException;
import de.elster.eric.wrapper.exception.WrapperException;
import de.elster.eric.wrapper.jna.EricapiLibrary;

import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Arrays;

/**
 * Klasse zum Bestimmen des aktuellen Betriebssystems
 *
 */
public final class Helper {

	private Helper() {
		// Hilfsklasse
	}

	/**
	 * Gibt betriebssystemabhängig den Namen einer Shared-Library zurück
	 *
	 * @param name
	 *            Basis-Name der Shared-Library
	 * @return betriebssystemabhängiger Name
	 * @throws WrapperException
	 *             Falls das Betriebssystem nicht unterstützt ist
	 */
	public static String getOsSharedLibraryName(final String name) throws WrapperException {
		if (Helper.isWindows()) {
			return name + ".dll";
		} else if (Helper.isLinux() || Helper.isAIX()) {
			return "lib" + name + ".so";
		} else if (Helper.isMacOs()) {
			return "lib" + name + ".dylib";
		} else {
			throw new WrapperException("Betriebssystem nicht unterstuetzt: " + name);
		}
	}

	public static boolean isWindows() {
		return osStartsWith("windows");
	}

	static boolean isLinux() {
		return osStartsWith("linux");
	}

	static boolean isMacOs() {
		return osStartsWith("mac");
	}

	static boolean isAIX() {
		return osStartsWith("aix");
	}

	private static boolean osStartsWith(String text) {
		final String os = System.getProperty("os.name").toLowerCase();

		return os.startsWith(text);
	}

	/**
	 * Wandelt eine Pfadangabe in das von ERiC verlangte Encoding um
	 *
	 * @param path
	 *            Pfadangabe
	 * @return Richtig kodierter Pfad als Bytes
	 * @throws WrapperException
	 *             Bei Problemen beim umkodieren
	 */
	public static byte[] encodePath(String path) throws WrapperException {
		String encoding;
		String finalPath;

		if (isMacOs()) {
			// Pfade auf macOS benötigen immer UTF-8
			encoding = "UTF-8";

			// macOS benutzt bei HFS+ die "decomposed form"
			// von UTF-8
			finalPath = Normalizer.normalize(path, Form.NFC);
		} else {
			// Pfade sind in der momentanen Codepage/Locale kodiert
			encoding = System.getProperty("sun.jnu.encoding");
			finalPath = path;
		}

		try {

			final byte[] pathBytes = finalPath.getBytes(encoding);
			return Arrays.copyOf(pathBytes, pathBytes.length + 1);
		} catch (UnsupportedEncodingException e) {
			throw new WrapperException("Encoding nicht gefunden: " + encoding, e);
		}
	}

	/**
	 * Wandelt einen ERiC-Fehlercode in eine EricException um
	 *
	 * @param ericapiLib
	 *          ericapiLibrary
	 * @param rc
	 *          Der umzuwandelnde Fehlercode
	 * @throws EricException
	 *          Die passende Exception (mit Fehlertext) zum Fehlercode
	 */
	public static void throwRcException(EricapiLibrary ericapiLib, int rc) throws EricException {
		final PointerByReference buffer = ericapiLib.EricRueckgabepufferErzeugen();
		try {
			ericapiLib.EricHoleFehlerText(rc, buffer);

			throw new EricException(rc, ericapiLib.EricRueckgabepufferInhalt(buffer));
		} finally {
			ericapiLib.EricRueckgabepufferFreigeben(buffer);
		}
	}

	/**
	 * Wirft eine Exception mit ERiC- und Serverantwort
	 *
	 * @param ericapiLib
	 *          ericapiLibrary
	 * @param rc
	 *            ERiC-Fehlercode
	 * @param ericResultBuffer
	 *            Validierungsergebnis
	 * @param serverResponseBuffer
	 *            Serverantwort
	 * @throws ResponseException
	 *             Die passende Exception mit Fehlertext, ERiC- und
	 *             Serverantwort
	 */
	public static void throwResponseException(EricapiLibrary ericapiLib, int rc, PointerByReference ericResultBuffer, PointerByReference serverResponseBuffer) throws ResponseException {
		final PointerByReference buffer = ericapiLib.EricRueckgabepufferErzeugen();
		try {
			ericapiLib.EricHoleFehlerText(rc, buffer);

			throw new ResponseException(rc, ericapiLib.EricRueckgabepufferInhalt(buffer),
                                            ericapiLib.EricRueckgabepufferInhalt(ericResultBuffer),
                                            ericapiLib.EricRueckgabepufferInhalt(serverResponseBuffer) );
		} finally {
			ericapiLib.EricRueckgabepufferFreigeben(buffer);
		}
	}

}
