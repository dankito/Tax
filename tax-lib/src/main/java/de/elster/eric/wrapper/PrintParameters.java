package de.elster.eric.wrapper;

/**
 * Parameter bzgl. PDF-Druck
 * 
 * Siehe eric_druck_parameter_t in eric_types.h
 */
public class PrintParameters {

	private final int vorschau;

	private final int ersteSeite;

	private final int duplexDruck;

	private final String pdfName;

	private final String fussText;

	public PrintParameters(final int vorschau, final int ersteSeite, final int duplexDruck, final String pdfName, final String fussText) {
		this.vorschau = vorschau;
		this.ersteSeite = ersteSeite;
		this.duplexDruck = duplexDruck;
		this.pdfName = pdfName;
		this.fussText = fussText;
	}

	public int getVorschau() {
		return vorschau;
	}

	public int getErsteSeite() {
		return ersteSeite;
	}

	public int getDuplexDruck() {
		return duplexDruck;
	}

	public String getPdfName() {
		return pdfName;
	}

	public String getFussText() {
		return fussText;
	}

}
