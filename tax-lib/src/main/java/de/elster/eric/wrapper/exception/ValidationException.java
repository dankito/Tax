package de.elster.eric.wrapper.exception;

import de.elster.eric.wrapper.ValidationError;

import java.util.List;

/**
 * Exception-Klasse für eventuell auftretende Plausibilitätsfehler
 * 
 */
public class ValidationException extends EricException {

	private static final long serialVersionUID = 2451630325484885296L;

	private final List<ValidationError> validationErrors;

	public ValidationException(final int rc, final String errorMessage, final List<ValidationError> validationErrors) {
		super(rc, errorMessage);

		this.validationErrors = validationErrors;
	}

	public List<ValidationError> getValidationErros() {
		return validationErrors;
	}

	public String toString() {
		final StringBuilder ret = new StringBuilder(58);

		final String newLine = System.getProperty("line.separator");

		ret.append("Validierungsfehler sind aufgetreten: " + newLine);

		int idx = 0;
		for (final ValidationError err : validationErrors) {
			ret.append(idx + 1).append(". Validierungsfehler: ").append(newLine)
					.append(err.toString());

			++idx;
		}

		return ret.toString();
	}

}
