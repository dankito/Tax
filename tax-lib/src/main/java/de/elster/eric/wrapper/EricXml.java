package de.elster.eric.wrapper;

import de.elster.eric.wrapper.exception.WrapperException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public final class EricXml {

	/**
	 * Parst XML-Daten und gibt ein DOM-Dokument zurück
	 */
	private static Document parseXmlString(String xmlData) throws WrapperException {
		Document doc;
		try (InputStream in = new ByteArrayInputStream(xmlData.getBytes("UTF-8"))) {
			final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(in);
			doc.getDocumentElement().normalize();
		} catch (ParserConfigurationException | IOException | SAXException e) {
			throw new WrapperException("Fehler beim Lesen des ERiC-XML", e);
		}
		return doc;
	}

	/**
	 * Gibt den Inhalt eines XML-Kindelements als String zurück
	 */
	private static String getElementChildText(final Element element, final String name) {
		final NodeList list = element.getElementsByTagName(name);
		if (list.getLength() > 0) {
			return list.item(0).getTextContent();
		}
		return null;
	}

	/**
	 * Gibt den Inhalt eines XML-Kindelements als Integer zurück
	 */
	private static int getElementChildInt(final Element element, final String name) {
		final String str = getElementChildText(element, name);
		return str != null ? Integer.parseInt(str) : 0; 
	}

	/**
	 * Wertet eine Plausibilitätsfehler Rückgabe-XML der ERiC-API-Funktion
	 * EricBearbeiteVorgang aus.
	 * 
	 * @param ericXml
	 *            XML-Daten nach Schema EricBearbeiteVorgang.xsd
	 * @return Liste an Plausibilitätsfehlern
	 * @throws WrapperException
	 *             Bei unerwarteten Fehlern in der XML-Verarbeitung
	 */
	public static List<ValidationError> parseValidationErrors(String ericXml) throws WrapperException {
		final Document doc = parseXmlString(ericXml);

		final List<ValidationError> ret = new ArrayList<>();

		final NodeList errorList = doc.getDocumentElement().getElementsByTagName("FehlerRegelpruefung");
		for (int i = 0; i < errorList.getLength(); ++i) {
			final Element land = (Element) errorList.item(i);
			ValidationError checkError = new ValidationError(
					getElementChildText(land, "Nutzdatenticket"),
					getElementChildText(land, "Feldidentifikator"),
					getElementChildInt(land, "Mehrfachzeilenindex"),
					getElementChildInt(land, "LfdNrVordruck"),
					getElementChildInt(land, "Untersachbereich"),
					getElementChildText(land, "RegelName"),
					getElementChildText(land, "FachlicheFehlerId"),
					getElementChildText(land, "Text"));
			ret.add(checkError);
		}

		return ret;
	}

	/**
	 * Wertet eine Erfolg Rückgabe-XML der ERiC-API-Funktion
	 * EricBearbeiteVorgang aus.
	 * 
	 * @param ericXml
	 *            XML-Daten nach Schema EricBearbeiteVorgang.xsd
	 * @return Erfolgobjekt. Evtl. mit Ordnungsbegriff und Telenummer
	 * @throws WrapperException
	 *             Bei unerwarteten Fehlern in der XML-Verarbeitung
	 */
	public static EricResult parseEricResult(String ericXml) throws WrapperException {
		final Document doc = parseXmlString(ericXml);

		final NodeList erfolgList = doc.getDocumentElement().getElementsByTagName("Erfolg");

		if (erfolgList.getLength() != 1) {
			throw new WrapperException("Nicht genau ein Erfolg-Element in der ERiC-Rueckgabe");
		}

		final Element erfolg = (Element) erfolgList.item(0);

		final EricResult result = new EricResult(
				getElementChildText(erfolg, "Telenummer"),
				getElementChildText(erfolg,	"Ordnungsbegriff"));

		return result;
	}

	/**
	 * Wertet eine Rückgabe-XML der ERiC-API-Funktion
	 * EricHoleFinanzamtLandNummern aus.
	 * 
	 * @param ericXml
	 *            XML-Daten nach Schema EricHoleFinanzamtLandNummern.xsd
	 * @return Liste an FinanzamtLaendern
	 * @throws WrapperException
	 *             Bei unerwarteten Fehlern in der XML-Verarbeitung
	 */
	public static List<FinanzamtLand> parseFinanzamtLaender(String ericXml) throws WrapperException {
		final Document doc = parseXmlString(ericXml);

		final List<FinanzamtLand> ret = new ArrayList<>();

		final NodeList laenderList = doc.getDocumentElement().getElementsByTagName("FinanzamtLand");
		for (int i = 0; i < laenderList.getLength(); ++i) {
			final Element land = (Element) laenderList.item(i);
			final FinanzamtLand finanzamtLand = new FinanzamtLand(
					getElementChildText(land, "Name"),
					getElementChildInt(land, "FinanzamtLandNummer"));
			ret.add(finanzamtLand);
		}

		return ret;
	}

	/**
	 * Wertet eine Rückgabe-XML der ERiC-API-Funktion EricHoleFinanzaemter aus.
	 * 
	 * @param ericXml
	 *            XML-Daten nach Schema EricHoleFinanzaemter.xsd
	 * @return Liste an Finanzämtern
	 * @throws WrapperException
	 *             Bei unerwarteten Fehlern in der XML-Verarbeitung
	 */
	public static List<Finanzamt> parseFinanzaemter(String ericXml) throws WrapperException {
		final Document doc = parseXmlString(ericXml);

		final List<Finanzamt> ret = new ArrayList<>();

		final NodeList finanzamtList = doc.getDocumentElement().getElementsByTagName("Finanzamt");
		for (int i = 0; i < finanzamtList.getLength(); ++i) {
			final Element finanzamtElement = (Element) finanzamtList.item(i);
			final Finanzamt finanzamt = new Finanzamt(
					getElementChildText(finanzamtElement, "Name"),
					getElementChildInt(finanzamtElement, "BuFaNummer"));
			ret.add(finanzamt);
		}

		return ret;
	}
}
