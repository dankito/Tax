package de.elster.eric.wrapper;

import de.elster.eric.wrapper.exception.WrapperException;

/**
 * Klasse zum Bestimmen des aktuellen Betriebssystems
 * 
 */
public final class OsHelper {

	private OsHelper() {
		// Hilfsklasse
	}
	
	/**
	 * Gibt betriebssystemabhängig den Namen einer Shared-Library zurück
	 * 
	 * @param name
	 *            Basis-Name der Shared-Library
	 * @return betriebssystemabhängiger Name
	 * @throws WrapperException
	 *             Falls das Betriebssystem nicht unterstützt ist
	 */
	public static String getOsSharedLibraryName(final String name) throws WrapperException {
		if (OsHelper.isWindows()) {
			return name + ".dll";
		} else if (OsHelper.isLinux()) {
			return "lib" + name + ".so";
		} else if (OsHelper.isMacOsX()) {
			return "lib" + name + ".dylib";
		} else {
			throw new WrapperException("Betriebssystem nicht unterstuetzt: " + name);
		}
	}

	static boolean isWindows() {
		return osStartsWith("windows");
	}

	static boolean isLinux() {
		return osStartsWith("linux");
	}

	static boolean isMacOsX() {
		return osStartsWith("mac os x");
	}

	private static boolean osStartsWith(String text) {
		final String os = System.getProperty("os.name").toLowerCase();

		return os.startsWith(text);		
	}
}
