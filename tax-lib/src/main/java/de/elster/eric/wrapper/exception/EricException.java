package de.elster.eric.wrapper.exception;

import de.elster.eric.wrapper.Errorcode;

/**
 * Exception-Klasse zur Rückgabe von ERiC-Fehlern.
 * 
 */
public class EricException extends Exception {
	private static final long serialVersionUID = -3756339869987293751L;

	private final int rc;

	private final String errorMessage;

	public EricException(final int rc, final String errorMessage) {
		super(errorMessage);

		this.rc = rc;
		this.errorMessage = errorMessage;
	}

	/**
	 * @return Numerischer ERiC-Fehlercode - siehe eric_fehlercodes.h
	 */
	public Errorcode getErrorCode() {
		return Errorcode.getErrorcode(rc);
	}

	/**
	 * @return ERiC Fehlernachricht
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @return Fehlercode und Nachricht als String
	 */
	public String toString() {
		return "ERiC-Exception: " + errorMessage + " (Fehlercode: " + Errorcode.getErrorcode(rc) + ")";
	}
}
