
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VorgangSType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VorgangSType">
 *   &lt;restriction base="{http://www.elster.de/elsterxml/schema/headerbasis/v3}BaseStringSType">
 *     &lt;enumeration value="send-Auth"/>
 *     &lt;enumeration value="send-NoSig"/>
 *     &lt;enumeration value="send-Auth-Part"/>
 *     &lt;enumeration value="send-NoSig-Part"/>
 *     &lt;enumeration value="send-Auth-Continue"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "VorgangSType", namespace = "http://www.elster.de/elsterxml/schema/headerbasis/v3")
@XmlEnum
public enum VorgangSType {


    /**
     * Senden von authentifizierten Daten (Signatur im THeader)
     * 
     */
    @XmlEnumValue("send-Auth")
    SEND_AUTH("send-Auth"),

    /**
     * Senden von Daten ohne Authentifizierung
     * 
     */
    @XmlEnumValue("send-NoSig")
    SEND_NO_SIG("send-NoSig"),

    /**
     * Senden von Daten die partielle verarbeitet werden, d.h. wenn bei Sammellieferungen ein Fehler enthalten ist, wird nicht die gesamte Datenlieferung abgelehnt, sondern nur der fehlerhafte Teil.
     * 
     */
    @XmlEnumValue("send-Auth-Part")
    SEND_AUTH_PART("send-Auth-Part"),

    /**
     * Senden von Daten die partielle verarbeitet werden, d.h. wenn bei Sammellieferungen ein Fehler enthalten ist, wird nicht die gesamte Datenlieferung abgelehnt, sondern nur der fehlerhafte Teil.
     * 
     */
    @XmlEnumValue("send-NoSig-Part")
    SEND_NO_SIG_PART("send-NoSig-Part"),

    /**
     * Wird aktuell nur bei internen Datenlieferungen verwendet.
     * 
     */
    @XmlEnumValue("send-Auth-Continue")
    SEND_AUTH_CONTINUE("send-Auth-Continue");
    private final String value;

    VorgangSType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VorgangSType fromValue(String v) {
        for (VorgangSType c: VorgangSType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
