
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VerfahrenSType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VerfahrenSType">
 *   &lt;restriction base="{http://www.elster.de/elsterxml/schema/headerbasis/v3}BaseStringSType">
 *     &lt;enumeration value="DummyWorker"/>
 *     &lt;enumeration value="ElsterAnmeldung"/>
 *     &lt;enumeration value="ElsterAuswertung"/>
 *     &lt;enumeration value="ElsterBackupSystem"/>
 *     &lt;enumeration value="ElsterBereitstellung"/>
 *     &lt;enumeration value="ElsterBilanz"/>
 *     &lt;enumeration value="ElsterBRM"/>
 *     &lt;enumeration value="ElsterBRMOrg"/>
 *     &lt;enumeration value="ElsterBrief"/>
 *     &lt;enumeration value="ElsterDatenuebernahmeEOP"/>
 *     &lt;enumeration value="ElsterDeMail"/>
 *     &lt;enumeration value="ElsterDVEtoLKS"/>
 *     &lt;enumeration value="ElsterDAME"/>
 *     &lt;enumeration value="ElsterDatenabholung"/>
 *     &lt;enumeration value="ElsterDatenbereitstellung"/>
 *     &lt;enumeration value="ElsterDatenversand"/>
 *     &lt;enumeration value="ElsterDIVA"/>
 *     &lt;enumeration value="ElsterDRVBund"/>
 *     &lt;enumeration value="ElsterEARL"/>
 *     &lt;enumeration value="ElsterEGVP"/>
 *     &lt;enumeration value="ElsterElfe"/>
 *     &lt;enumeration value="ElsterELStAM"/>
 *     &lt;enumeration value="ElsterEMail"/>
 *     &lt;enumeration value="ElsterErklaerung"/>
 *     &lt;enumeration value="ElsterExtern"/>
 *     &lt;enumeration value="ElsterFSE"/>
 *     &lt;enumeration value="ElsterFT"/>
 *     &lt;enumeration value="ElsterFTintern"/>
 *     &lt;enumeration value="ElsterGeCoSYN"/>
 *     &lt;enumeration value="ElsterIDRecherche"/>
 *     &lt;enumeration value="ElsterKapESt"/>
 *     &lt;enumeration value="ElsterKMV"/>
 *     &lt;enumeration value="ElsterKDialog"/>
 *     &lt;enumeration value="ElsterKMVZentraleAuswertung"/>
 *     &lt;enumeration value="ElsterKMVZentral"/>
 *     &lt;enumeration value="ElsterKMVIdNr"/>
 *     &lt;enumeration value="ElsterKMVProtokoll"/>
 *     &lt;enumeration value="ElsterKMVIntern"/>
 *     &lt;enumeration value="ElsterKontakt"/>
 *     &lt;enumeration value="ElsterKontoabfrage"/>
 *     &lt;enumeration value="ElsterKontoablage"/>
 *     &lt;enumeration value="ElsterKStoKS"/>
 *     &lt;enumeration value="ElsterLanguste"/>
 *     &lt;enumeration value="ElsterLohn"/>
 *     &lt;enumeration value="ElsterLohn2"/>
 *     &lt;enumeration value="ElsterLohnIntern"/>
 *     &lt;enumeration value="ElsterLohnProtokoll"/>
 *     &lt;enumeration value="ElsterLKStoDVE"/>
 *     &lt;enumeration value="ElsterLKStoZF"/>
 *     &lt;enumeration value="ElsterLStZerlegung"/>
 *     &lt;enumeration value="ElsterLStZerlegungProtokoll"/>
 *     &lt;enumeration value="ElsterMAG"/>
 *     &lt;enumeration value="ElsterMOSS"/>
 *     &lt;enumeration value="ElsterNachricht"/>
 *     &lt;enumeration value="ElsterPdf"/>
 *     &lt;enumeration value="ElsterPortal"/>
 *     &lt;enumeration value="ElsterPortalAdapter"/>
 *     &lt;enumeration value="ElsterSignatur"/>
 *     &lt;enumeration value="ElsterSignaturNachricht"/>
 *     &lt;enumeration value="ElsterSmart"/>
 *     &lt;enumeration value="ElsterSuch"/>
 *     &lt;enumeration value="ElsterSuchExtern"/>
 *     &lt;enumeration value="ElsterTransferAsynchron"/>
 *     &lt;enumeration value="ElsterTransferSynchron"/>
 *     &lt;enumeration value="ElsterVaSt"/>
 *     &lt;enumeration value="ElsterVerarbeitungsMeldung"/>
 *     &lt;enumeration value="ElsterVollmachtDB"/>
 *     &lt;enumeration value="ElsterGinsterVDB"/>
 *     &lt;enumeration value="ElsterZANS"/>
 *     &lt;enumeration value="ElsterZebra"/>
 *     &lt;enumeration value="ElsterZFtoLKS"/>
 *     &lt;enumeration value="ElsterZfaZivit"/>
 *     &lt;enumeration value="ElsterZObEL"/>
 *     &lt;enumeration value="ElsterGINSTER"/>
 *     &lt;enumeration value="OpenZoll"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "VerfahrenSType", namespace = "http://www.elster.de/elsterxml/schema/headerbasis/v3")
@XmlEnum
public enum VerfahrenSType {


    /**
     * internes Verfahren der Finanzverwaltung
     * 
     */
    @XmlEnumValue("DummyWorker")
    DUMMY_WORKER("DummyWorker"),

    /**
     * Verfahren zur Übermittlung von Anmeldesteuern (Lohnsteuer-Anmeldung und Umsatzsteuer-Voranmeldung
     * 
     */
    @XmlEnumValue("ElsterAnmeldung")
    ELSTER_ANMELDUNG("ElsterAnmeldung"),

    /**
     * Auswertungen (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterAuswertung")
    ELSTER_AUSWERTUNG("ElsterAuswertung"),

    /**
     * Bereitstellung von Daten aus dem Backup (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterBackupSystem")
    ELSTER_BACKUP_SYSTEM("ElsterBackupSystem"),

    /**
     * Bereitstellung von Daten zur Abholung (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterBereitstellung")
    ELSTER_BEREITSTELLUNG("ElsterBereitstellung"),

    /**
     * Verfahren fuer Bilanz
     * 
     */
    @XmlEnumValue("ElsterBilanz")
    ELSTER_BILANZ("ElsterBilanz"),

    /**
     * Berechtigungsmanagement (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterBRM")
    ELSTER_BRM("ElsterBRM"),

    /**
     * Berechtigungsmanagement (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterBRMOrg")
    ELSTER_BRM_ORG("ElsterBRMOrg"),
    @XmlEnumValue("ElsterBrief")
    ELSTER_BRIEF("ElsterBrief"),

    /**
     * Internes Verfahren
     * 
     */
    @XmlEnumValue("ElsterDatenuebernahmeEOP")
    ELSTER_DATENUEBERNAHME_EOP("ElsterDatenuebernahmeEOP"),

    /**
     * Internes Verfahren
     * 
     */
    @XmlEnumValue("ElsterDeMail")
    ELSTER_DE_MAIL("ElsterDeMail"),

    /**
     * Datenlieferungen vom Statistischen Bundesamt  DESTATIS (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterDVEtoLKS")
    ELSTER_DV_ETO_LKS("ElsterDVEtoLKS"),

    /**
     * Verfahren DAME
     * 
     */
    @XmlEnumValue("ElsterDAME")
    ELSTER_DAME("ElsterDAME"),

    /**
     * Abholen von Daten (Bsp. Bescheiddaten)
     * 
     */
    @XmlEnumValue("ElsterDatenabholung")
    ELSTER_DATENABHOLUNG("ElsterDatenabholung"),

    /**
     * Bereitstellen von Daten (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterDatenbereitstellung")
    ELSTER_DATENBEREITSTELLUNG("ElsterDatenbereitstellung"),

    /**
     * Versand von Daten (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterDatenversand")
    ELSTER_DATENVERSAND("ElsterDatenversand"),

    /**
     * ElsterDIVA
     * 
     */
    @XmlEnumValue("ElsterDIVA")
    ELSTER_DIVA("ElsterDIVA"),

    /**
     * ElsterDRVBund
     * 
     */
    @XmlEnumValue("ElsterDRVBund")
    ELSTER_DRV_BUND("ElsterDRVBund"),

    /**
     * Datenübertragung für EARL
     * 
     */
    @XmlEnumValue("ElsterEARL")
    ELSTER_EARL("ElsterEARL"),

    /**
     * Gerichts- und Verwaltungspostfach
     * 
     */
    @XmlEnumValue("ElsterEGVP")
    ELSTER_EGVP("ElsterEGVP"),

    /**
     * ElsterElfe	Internes Verfahren
     * 
     */
    @XmlEnumValue("ElsterElfe")
    ELSTER_ELFE("ElsterElfe"),

    /**
     * ElsterELStAM	ELStAM Daten (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterELStAM")
    ELSTER_EL_ST_AM("ElsterELStAM"),

    /**
     * Email Anstoß (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterEMail")
    ELSTER_E_MAIL("ElsterEMail"),

    /**
     * Uebermittlung von Steuererklaerungen z.B. Einkommensteuer, Umsatzsteuer (Jahreserklaerung) und Gewerbesteuer
     * 
     */
    @XmlEnumValue("ElsterErklaerung")
    ELSTER_ERKLAERUNG("ElsterErklaerung"),

    /**
     * Alle Externen Verfahren (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterExtern")
    ELSTER_EXTERN("ElsterExtern"),

    /**
     * Verfahren fuer den Fragebogen zur steuerlichen Erfassung
     * 
     */
    @XmlEnumValue("ElsterFSE")
    ELSTER_FSE("ElsterFSE"),

    /**
     * Datenlieferungen über ElsterFT
     * 
     */
    @XmlEnumValue("ElsterFT")
    ELSTER_FT("ElsterFT"),

    /**
     * Datenlieferungen analog zu ElsterFT über ERiC (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterFTintern")
    ELSTER_F_TINTERN("ElsterFTintern"),
    @XmlEnumValue("ElsterGeCoSYN")
    ELSTER_GE_CO_SYN("ElsterGeCoSYN"),

    /**
     * ID Nummern Recherche (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterIDRecherche")
    ELSTER_ID_RECHERCHE("ElsterIDRecherche"),

    /**
     * Verfahren Kapitalertragssteuer-Anmeldung
     * 
     */
    @XmlEnumValue("ElsterKapESt")
    ELSTER_KAP_E_ST("ElsterKapESt"),

    /**
     * Kontrollmitteilungsverfahren
     * 
     */
    @XmlEnumValue("ElsterKMV")
    ELSTER_KMV("ElsterKMV"),

    /**
     * ElsterKDialog
     * 
     */
    @XmlEnumValue("ElsterKDialog")
    ELSTER_K_DIALOG("ElsterKDialog"),

    /**
     * Kontrollmitteilungsverfahren
     * 
     */
    @XmlEnumValue("ElsterKMVZentraleAuswertung")
    ELSTER_KMV_ZENTRALE_AUSWERTUNG("ElsterKMVZentraleAuswertung"),

    /**
     * Kontrollmitteilungsverfahren
     * 
     */
    @XmlEnumValue("ElsterKMVZentral")
    ELSTER_KMV_ZENTRAL("ElsterKMVZentral"),

    /**
     * Kontrollmitteilungsverfahren
     * 
     */
    @XmlEnumValue("ElsterKMVIdNr")
    ELSTER_KMV_ID_NR("ElsterKMVIdNr"),

    /**
     * Lieferung von Protokollen von KMV (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterKMVProtokoll")
    ELSTER_KMV_PROTOKOLL("ElsterKMVProtokoll"),

    /**
     * Internes Verfahren
     * 
     */
    @XmlEnumValue("ElsterKMVIntern")
    ELSTER_KMV_INTERN("ElsterKMVIntern"),

    /**
     *  Das Kontaktformular wird im öffentlichen Bereich von Mein ELSTER angeboten. Über das Kontaktformular kann der Nutzer zwei Arten von Nachrichten übermitteln: steuerliche und nicht-steuerliche Nachrichten.
     * 
     */
    @XmlEnumValue("ElsterKontakt")
    ELSTER_KONTAKT("ElsterKontakt"),

    /**
     * Steuerkontenabfrage insbes. fuer Steuerberater bzw. ueber Portal (Online) Abfragen, Anmeldung etc. für Steuerkonto
     * 
     */
    @XmlEnumValue("ElsterKontoabfrage")
    ELSTER_KONTOABFRAGE("ElsterKontoabfrage"),

    /**
     * Datenlieferung zu Rechten zur Kontoabfrage
     * 
     */
    @XmlEnumValue("ElsterKontoablage")
    ELSTER_KONTOABLAGE("ElsterKontoablage"),

    /**
     * Datenlieferung von Kopfstelle zu Kopfstelle (Internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterKStoKS")
    ELSTER_K_STO_KS("ElsterKStoKS"),

    /**
     * Internes Verfahren
     * 
     */
    @XmlEnumValue("ElsterLanguste")
    ELSTER_LANGUSTE("ElsterLanguste"),

    /**
     * Uebermittlung von Lohnsteuerbescheinigungen bzw. Lohnersatzleistungen (Lohnsteuerbescheinigung, Protokoll zur Bescheinigung)
     * 
     */
    @XmlEnumValue("ElsterLohn")
    ELSTER_LOHN("ElsterLohn"),

    /**
     * ElsterLohn2
     * 
     */
    @XmlEnumValue("ElsterLohn2")
    ELSTER_LOHN_2("ElsterLohn2"),

    /**
     * Internes Verfahren
     * 
     */
    @XmlEnumValue("ElsterLohnIntern")
    ELSTER_LOHN_INTERN("ElsterLohnIntern"),

    /**
     * Lieferung von Protokollen (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterLohnProtokoll")
    ELSTER_LOHN_PROTOKOLL("ElsterLohnProtokoll"),

    /**
     * Datenlieferungen zum Statistischen Bundesamt  DESTATIS (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterLKStoDVE")
    ELSTER_LK_STO_DVE("ElsterLKStoDVE"),

    /**
     * Verfahren zum Austausch von Daten von den Landeskopfstellen zum ZFA
     * 
     */
    @XmlEnumValue("ElsterLKStoZF")
    ELSTER_LK_STO_ZF("ElsterLKStoZF"),

    /**
     * Internes Verfahren
     * 
     */
    @XmlEnumValue("ElsterLStZerlegung")
    ELSTER_L_ST_ZERLEGUNG("ElsterLStZerlegung"),
    @XmlEnumValue("ElsterLStZerlegungProtokoll")
    ELSTER_L_ST_ZERLEGUNG_PROTOKOLL("ElsterLStZerlegungProtokoll"),

    /**
     * maschinelle Abfrageverfahren Arbeitgeber (IDNr)
     * 
     */
    @XmlEnumValue("ElsterMAG")
    ELSTER_MAG("ElsterMAG"),

    /**
     * Mini-One-Stop-Shop
     * 
     */
    @XmlEnumValue("ElsterMOSS")
    ELSTER_MOSS("ElsterMOSS"),

    /**
     * Unformatierte Nachrichten (Bsp. Einspruch)
     * 
     */
    @XmlEnumValue("ElsterNachricht")
    ELSTER_NACHRICHT("ElsterNachricht"),
    @XmlEnumValue("ElsterPdf")
    ELSTER_PDF("ElsterPdf"),

    /**
     * ElsterOnline-Portal Daten (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterPortal")
    ELSTER_PORTAL("ElsterPortal"),

    /**
     * internes Verfahren der Finanzverwaltung
     * 
     */
    @XmlEnumValue("ElsterPortalAdapter")
    ELSTER_PORTAL_ADAPTER("ElsterPortalAdapter"),

    /**
     * Verfahren zum Themengebiet Authentifizierung
     * 
     */
    @XmlEnumValue("ElsterSignatur")
    ELSTER_SIGNATUR("ElsterSignatur"),
    @XmlEnumValue("ElsterSignaturNachricht")
    ELSTER_SIGNATUR_NACHRICHT("ElsterSignaturNachricht"),
    @XmlEnumValue("ElsterSmart")
    ELSTER_SMART("ElsterSmart"),

    /**
     * Internes Verfahren
     * 
     */
    @XmlEnumValue("ElsterSuch")
    ELSTER_SUCH("ElsterSuch"),

    /**
     * Internes Verfahren
     * 
     */
    @XmlEnumValue("ElsterSuchExtern")
    ELSTER_SUCH_EXTERN("ElsterSuchExtern"),

    /**
     * Verfahren: ElsterTransferAsynchron Datenart: ETRVerfAntragEreignis
     * 
     */
    @XmlEnumValue("ElsterTransferAsynchron")
    ELSTER_TRANSFER_ASYNCHRON("ElsterTransferAsynchron"),

    /**
     * Verfahren: ElsterTransferAsynchron
     * 
     */
    @XmlEnumValue("ElsterTransferSynchron")
    ELSTER_TRANSFER_SYNCHRON("ElsterTransferSynchron"),

    /**
     * Verfahren zum Erlangen von Belegen um die Steuererklärung vorauszufüllen
     * 
     */
    @XmlEnumValue("ElsterVaSt")
    ELSTER_VA_ST("ElsterVaSt"),

    /**
     * Informationen zur Verarbeitung (internes Verfahren)
     * 
     */
    @XmlEnumValue("ElsterVerarbeitungsMeldung")
    ELSTER_VERARBEITUNGS_MELDUNG("ElsterVerarbeitungsMeldung"),

    /**
     * Datenlieferungen zu Vollmachten/Kontingentierung
     * 
     */
    @XmlEnumValue("ElsterVollmachtDB")
    ELSTER_VOLLMACHT_DB("ElsterVollmachtDB"),
    @XmlEnumValue("ElsterGinsterVDB")
    ELSTER_GINSTER_VDB("ElsterGinsterVDB"),

    /**
     * Arbeitnehmersparzulage (ANSpZ)
     * 
     */
    @XmlEnumValue("ElsterZANS")
    ELSTER_ZANS("ElsterZANS"),

    /**
     * ElsterZebra
     * 
     */
    @XmlEnumValue("ElsterZebra")
    ELSTER_ZEBRA("ElsterZebra"),

    /**
     * Verfahren zum Austausch von Daten vom ZFA zu den Landeskopfstellen
     * 
     */
    @XmlEnumValue("ElsterZFtoLKS")
    ELSTER_Z_FTO_LKS("ElsterZFtoLKS"),

    /**
     * Verfahren zum Austausch von Daten vom ZFA zu ZIVIT
     * 
     */
    @XmlEnumValue("ElsterZfaZivit")
    ELSTER_ZFA_ZIVIT("ElsterZfaZivit"),
    @XmlEnumValue("ElsterZObEL")
    ELSTER_Z_OB_EL("ElsterZObEL"),
    @XmlEnumValue("ElsterGINSTER")
    ELSTER_GINSTER("ElsterGINSTER"),
    @XmlEnumValue("OpenZoll")
    OPEN_ZOLL("OpenZoll");
    private final String value;

    VerfahrenSType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VerfahrenSType fromValue(String v) {
        for (VerfahrenSType c: VerfahrenSType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
