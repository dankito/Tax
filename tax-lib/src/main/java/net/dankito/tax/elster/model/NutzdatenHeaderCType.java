
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NutzdatenHeaderCType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NutzdatenHeaderCType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NutzdatenTicket" type="{http://www.elster.de/elsterxml/schema/headerbasis/v3}NutzdatenBlockTicketSType"/>
 *         &lt;element name="Empfaenger" type="{http://www.elster.de/elsterxml/schema/v11}NDH_EmpfaengerCType"/>
 *         &lt;element name="Hersteller" type="{http://www.elster.de/elsterxml/schema/v11}NDH_HerstellerCType" minOccurs="0"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}DatenLieferant" minOccurs="0"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}RC" minOccurs="0"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}Zusatz" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="version" use="required" type="{http://www.elster.de/elsterxml/schema/headerbasis/v3}headerVersionSType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NutzdatenHeaderCType", propOrder = {
    "nutzdatenTicket",
    "empfaenger",
    "hersteller",
    "datenLieferant",
    "rc",
    "zusatz"
})
public class NutzdatenHeaderCType {

    @XmlElement(name = "NutzdatenTicket", required = true)
    protected String nutzdatenTicket;
    @XmlElement(name = "Empfaenger", required = true)
    protected NDHEmpfaengerCType empfaenger;
    @XmlElement(name = "Hersteller")
    protected NDHHerstellerCType hersteller;
    @XmlElement(name = "DatenLieferant")
    protected String datenLieferant;
    @XmlElement(name = "RC")
    protected RCCType rc;
    @XmlElement(name = "Zusatz")
    protected ZusatzCType zusatz;
    @XmlAttribute(name = "version", required = true)
    protected String version;

    /**
     * Gets the value of the nutzdatenTicket property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNutzdatenTicket() {
        return nutzdatenTicket;
    }

    /**
     * Sets the value of the nutzdatenTicket property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNutzdatenTicket(String value) {
        this.nutzdatenTicket = value;
    }

    /**
     * Gets the value of the empfaenger property.
     * 
     * @return
     *     possible object is
     *     {@link NDHEmpfaengerCType }
     *     
     */
    public NDHEmpfaengerCType getEmpfaenger() {
        return empfaenger;
    }

    /**
     * Sets the value of the empfaenger property.
     * 
     * @param value
     *     allowed object is
     *     {@link NDHEmpfaengerCType }
     *     
     */
    public void setEmpfaenger(NDHEmpfaengerCType value) {
        this.empfaenger = value;
    }

    /**
     * Gets the value of the hersteller property.
     * 
     * @return
     *     possible object is
     *     {@link NDHHerstellerCType }
     *     
     */
    public NDHHerstellerCType getHersteller() {
        return hersteller;
    }

    /**
     * Sets the value of the hersteller property.
     * 
     * @param value
     *     allowed object is
     *     {@link NDHHerstellerCType }
     *     
     */
    public void setHersteller(NDHHerstellerCType value) {
        this.hersteller = value;
    }

    /**
     * Der Datenlieferant im NutzdatenHeader ist der, der die Nutzdaten erzeugt hat (z.B. AG, Steuerpflichtiger, Steuerberater bzw. die Kanzlei), nicht zwangsläufig dieselbe Person, wie im TransferHeader.
     * 
     * Dieses Feld dient der Information darüber, wer in Problemfällen kontaktiert werden könnte. 
     * Ansprechpartner mit Telefon, E-Mail, etc.
     * 
     * Beispiel:
     * Bei ESt: Steuerpflichtiger oder Steuerberater/Firma  
     * 
     * Möglicher Aufbau: Name des Erstellers; Strasse; Hausnummer; Hausnummerzusatz; Adresszusatz; PLZ; Ort; Land; Telefon; E-Mail-Adresse
     * Beispiel: "Hr. YY; Teststraße; 12; a; im Hinterhof; 80333; Muenchen;Deutschland; 089/1111 1111; tester@test.de"
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatenLieferant() {
        return datenLieferant;
    }

    /**
     * Sets the value of the datenLieferant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatenLieferant(String value) {
        this.datenLieferant = value;
    }

    /**
     * Gets the value of the rc property.
     * 
     * @return
     *     possible object is
     *     {@link RCCType }
     *     
     */
    public RCCType getRC() {
        return rc;
    }

    /**
     * Sets the value of the rc property.
     * 
     * @param value
     *     allowed object is
     *     {@link RCCType }
     *     
     */
    public void setRC(RCCType value) {
        this.rc = value;
    }

    /**
     * Gets the value of the zusatz property.
     * 
     * @return
     *     possible object is
     *     {@link ZusatzCType }
     *     
     */
    public ZusatzCType getZusatz() {
        return zusatz;
    }

    /**
     * Sets the value of the zusatz property.
     * 
     * @param value
     *     allowed object is
     *     {@link ZusatzCType }
     *     
     */
    public void setZusatz(ZusatzCType value) {
        this.zusatz = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

}
