
package net.dankito.tax.elster.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Verschluesselung" type="{http://www.elster.de/elsterxml/schema/headerbasis/v3}VerschluesselungSType"/>
 *         &lt;element name="Kompression" type="{http://www.elster.de/elsterxml/schema/headerbasis/v3}KompressionSType"/>
 *         &lt;element name="TransportSchluessel" type="{http://www.elster.de/elsterxml/schema/headerbasis/v3}TransportSchluesselSType" minOccurs="0"/>
 *         &lt;element name="Erstellung" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Eric" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;any processContents='lax' maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "verschluesselung",
    "kompression",
    "transportSchluessel",
    "erstellung"
})
@XmlRootElement(name = "Datei")
public class Datei {

    @XmlElement(name = "Verschluesselung", required = true)
    @XmlSchemaType(name = "string")
    protected VerschluesselungSType verschluesselung;
    @XmlElement(name = "Kompression", required = true)
    @XmlSchemaType(name = "string")
    protected KompressionSType kompression;
    @XmlElement(name = "TransportSchluessel")
    protected String transportSchluessel;
    @XmlElement(name = "Erstellung")
    protected Datei.Erstellung erstellung;

    /**
     * Gets the value of the verschluesselung property.
     * 
     * @return
     *     possible object is
     *     {@link VerschluesselungSType }
     *     
     */
    public VerschluesselungSType getVerschluesselung() {
        return verschluesselung;
    }

    /**
     * Sets the value of the verschluesselung property.
     * 
     * @param value
     *     allowed object is
     *     {@link VerschluesselungSType }
     *     
     */
    public void setVerschluesselung(VerschluesselungSType value) {
        this.verschluesselung = value;
    }

    /**
     * Gets the value of the kompression property.
     * 
     * @return
     *     possible object is
     *     {@link KompressionSType }
     *     
     */
    public KompressionSType getKompression() {
        return kompression;
    }

    /**
     * Sets the value of the kompression property.
     * 
     * @param value
     *     allowed object is
     *     {@link KompressionSType }
     *     
     */
    public void setKompression(KompressionSType value) {
        this.kompression = value;
    }

    /**
     * Gets the value of the transportSchluessel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportSchluessel() {
        return transportSchluessel;
    }

    /**
     * Sets the value of the transportSchluessel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportSchluessel(String value) {
        this.transportSchluessel = value;
    }

    /**
     * Gets the value of the erstellung property.
     * 
     * @return
     *     possible object is
     *     {@link Datei.Erstellung }
     *     
     */
    public Datei.Erstellung getErstellung() {
        return erstellung;
    }

    /**
     * Sets the value of the erstellung property.
     * 
     * @param value
     *     allowed object is
     *     {@link Datei.Erstellung }
     *     
     */
    public void setErstellung(Datei.Erstellung value) {
        this.erstellung = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Eric" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;any processContents='lax' maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "eric"
    })
    public static class Erstellung {

        @XmlElement(name = "Eric")
        protected Datei.Erstellung.Eric eric;

        /**
         * Gets the value of the eric property.
         * 
         * @return
         *     possible object is
         *     {@link Datei.Erstellung.Eric }
         *     
         */
        public Datei.Erstellung.Eric getEric() {
            return eric;
        }

        /**
         * Sets the value of the eric property.
         * 
         * @param value
         *     allowed object is
         *     {@link Datei.Erstellung.Eric }
         *     
         */
        public void setEric(Datei.Erstellung.Eric value) {
            this.eric = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;any processContents='lax' maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "content"
        })
        public static class Eric {

            @XmlMixed
            @XmlAnyElement(lax = true)
            protected List<Object> content;

            /**
             * Gets the value of the content property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the content property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getContent().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Element }
             * {@link String }
             * {@link Object }
             * 
             * 
             */
            public List<Object> getContent() {
                if (content == null) {
                    content = new ArrayList<Object>();
                }
                return this.content;
            }

        }

    }

}
