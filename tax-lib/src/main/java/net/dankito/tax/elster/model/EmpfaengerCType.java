
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmpfaengerCType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmpfaengerCType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Ziel" type="{http://www.elster.de/elsterxml/schema/headerbasis/v3}BundeslandSType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.elster.de/elsterxml/schema/headerbasis/v3}BaseStringSType">
 *             &lt;enumeration value="L"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmpfaengerCType", propOrder = {
    "ziel"
})
public class EmpfaengerCType {

    @XmlElement(name = "Ziel", required = true)
    @XmlSchemaType(name = "string")
    protected BundeslandSType ziel;
    @XmlAttribute(name = "id", required = true)
    protected String id;

    /**
     * Gets the value of the ziel property.
     * 
     * @return
     *     possible object is
     *     {@link BundeslandSType }
     *     
     */
    public BundeslandSType getZiel() {
        return ziel;
    }

    /**
     * Sets the value of the ziel property.
     * 
     * @param value
     *     allowed object is
     *     {@link BundeslandSType }
     *     
     */
    public void setZiel(BundeslandSType value) {
        this.ziel = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
