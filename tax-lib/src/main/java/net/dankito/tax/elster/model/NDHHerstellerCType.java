
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NDH_HerstellerCType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NDH_HerstellerCType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProduktName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.elster.de/elsterxml/schema/headerbasis/v3}BaseStringSType">
 *               &lt;maxLength value="50"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ProduktVersion">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.elster.de/elsterxml/schema/headerbasis/v3}BaseStringSType">
 *               &lt;maxLength value="50"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NDH_HerstellerCType", propOrder = {
    "produktName",
    "produktVersion"
})
public class NDHHerstellerCType {

    @XmlElement(name = "ProduktName", required = true)
    protected String produktName;
    @XmlElement(name = "ProduktVersion", required = true)
    protected String produktVersion;

    /**
     * Gets the value of the produktName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProduktName() {
        return produktName;
    }

    /**
     * Sets the value of the produktName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProduktName(String value) {
        this.produktName = value;
    }

    /**
     * Gets the value of the produktVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProduktVersion() {
        return produktVersion;
    }

    /**
     * Sets the value of the produktVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProduktVersion(String value) {
        this.produktVersion = value;
    }

}
