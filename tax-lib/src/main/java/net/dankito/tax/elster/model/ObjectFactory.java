
package net.dankito.tax.elster.model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.dankito.tax.elster.model package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Zusatz_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "Zusatz");
    private final static QName _TransferHeader_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "TransferHeader");
    private final static QName _Vorgang_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "Vorgang");
    private final static QName _DatenLieferant_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "DatenLieferant");
    private final static QName _RC_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "RC");
    private final static QName _HerstellerID_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "HerstellerID");
    private final static QName _Testmerker_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "Testmerker");
    private final static QName _Verfahren_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "Verfahren");
    private final static QName _EingangsDatum_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "EingangsDatum");
    private final static QName _DatenArt_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "DatenArt");
    private final static QName _NutzdatenHeader_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "NutzdatenHeader");
    private final static QName _TransferTicket_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "TransferTicket");
    private final static QName _VersionClient_QNAME = new QName("http://www.elster.de/elsterxml/schema/v11", "VersionClient");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.dankito.tax.elster.model
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Datei }
     * 
     */
    public Datei createDatei() {
        return new Datei();
    }

    /**
     * Create an instance of {@link Datei.Erstellung }
     * 
     */
    public Datei.Erstellung createDateiErstellung() {
        return new Datei.Erstellung();
    }

    /**
     * Create an instance of {@link RCCType }
     * 
     */
    public RCCType createRCCType() {
        return new RCCType();
    }

    /**
     * Create an instance of {@link NutzdatenHeaderCType }
     * 
     */
    public NutzdatenHeaderCType createNutzdatenHeaderCType() {
        return new NutzdatenHeaderCType();
    }

    /**
     * Create an instance of {@link ElsterXmlFile }
     * 
     */
    public ElsterXmlFile createElster() {
        return new ElsterXmlFile();
    }

    /**
     * Create an instance of {@link TransferHeaderCType }
     * 
     */
    public TransferHeaderCType createTransferHeaderCType() {
        return new TransferHeaderCType();
    }

    /**
     * Create an instance of {@link DatenTeilCType }
     * 
     */
    public DatenTeilCType createDatenTeilCType() {
        return new DatenTeilCType();
    }

    /**
     * Create an instance of {@link ZusatzCType }
     * 
     */
    public ZusatzCType createZusatzCType() {
        return new ZusatzCType();
    }

    /**
     * Create an instance of {@link SigUser }
     * 
     */
    public SigUser createSigUser() {
        return new SigUser();
    }

    /**
     * Create an instance of {@link UstaUmsatzsteuersondervorauszahlungCType }
     * 
     */
    public UstaUmsatzsteuersondervorauszahlungCType createUstaUmsatzsteuersondervorauszahlungCType() {
        return new UstaUmsatzsteuersondervorauszahlungCType();
    }

    /**
     * Create an instance of {@link AdresseCType }
     * 
     */
    public AdresseCType createAdresseCType() {
        return new AdresseCType();
    }

    /**
     * Create an instance of {@link EmpfaengerCType }
     * 
     */
    public EmpfaengerCType createEmpfaengerCType() {
        return new EmpfaengerCType();
    }

    /**
     * Create an instance of {@link UstaDauerfristverlaengerungCType }
     * 
     */
    public UstaDauerfristverlaengerungCType createUstaDauerfristverlaengerungCType() {
        return new UstaDauerfristverlaengerungCType();
    }

    /**
     * Create an instance of {@link UstaDatenlieferantCType }
     * 
     */
    public UstaDatenlieferantCType createUstaDatenlieferantCType() {
        return new UstaDatenlieferantCType();
    }

    /**
     * Create an instance of {@link NDHEmpfaengerCType }
     * 
     */
    public NDHEmpfaengerCType createNDHEmpfaengerCType() {
        return new NDHEmpfaengerCType();
    }

    /**
     * Create an instance of {@link NutzdatenblockCType }
     * 
     */
    public NutzdatenblockCType createNutzdatenblockCType() {
        return new NutzdatenblockCType();
    }

    /**
     * Create an instance of {@link UstaSteuerfallCType }
     * 
     */
    public UstaSteuerfallCType createUstaSteuerfallCType() {
        return new UstaSteuerfallCType();
    }

    /**
     * Create an instance of {@link UstaAnmeldungssteuernCType }
     * 
     */
    public UstaAnmeldungssteuernCType createUstaAnmeldungssteuernCType() {
        return new UstaAnmeldungssteuernCType();
    }

    /**
     * Create an instance of {@link NDHHerstellerCType }
     * 
     */
    public NDHHerstellerCType createNDHHerstellerCType() {
        return new NDHHerstellerCType();
    }

    /**
     * Create an instance of {@link MandantCType }
     * 
     */
    public MandantCType createMandantCType() {
        return new MandantCType();
    }

    /**
     * Create an instance of {@link NutzdatenCType }
     * 
     */
    public NutzdatenCType createNutzdatenCType() {
        return new NutzdatenCType();
    }

    /**
     * Create an instance of {@link UstaUmsatzsteuervoranmeldungCType }
     * 
     */
    public UstaUmsatzsteuervoranmeldungCType createUstaUmsatzsteuervoranmeldungCType() {
        return new UstaUmsatzsteuervoranmeldungCType();
    }

    /**
     * Create an instance of {@link Datei.Erstellung.Eric }
     * 
     */
    public Datei.Erstellung.Eric createDateiErstellungEric() {
        return new Datei.Erstellung.Eric();
    }

    /**
     * Create an instance of {@link RCCType.Rueckgabe }
     * 
     */
    public RCCType.Rueckgabe createRCCTypeRueckgabe() {
        return new RCCType.Rueckgabe();
    }

    /**
     * Create an instance of {@link RCCType.Stack }
     * 
     */
    public RCCType.Stack createRCCTypeStack() {
        return new RCCType.Stack();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ZusatzCType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "Zusatz")
    public JAXBElement<ZusatzCType> createZusatz(ZusatzCType value) {
        return new JAXBElement<ZusatzCType>(_Zusatz_QNAME, ZusatzCType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferHeaderCType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "TransferHeader")
    public JAXBElement<TransferHeaderCType> createTransferHeader(TransferHeaderCType value) {
        return new JAXBElement<TransferHeaderCType>(_TransferHeader_QNAME, TransferHeaderCType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VorgangSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "Vorgang")
    public JAXBElement<VorgangSType> createVorgang(VorgangSType value) {
        return new JAXBElement<VorgangSType>(_Vorgang_QNAME, VorgangSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "DatenLieferant")
    public JAXBElement<String> createDatenLieferant(String value) {
        return new JAXBElement<String>(_DatenLieferant_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RCCType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "RC")
    public JAXBElement<RCCType> createRC(RCCType value) {
        return new JAXBElement<RCCType>(_RC_QNAME, RCCType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "HerstellerID")
    public JAXBElement<String> createHerstellerID(String value) {
        return new JAXBElement<String>(_HerstellerID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "Testmerker")
    public JAXBElement<String> createTestmerker(String value) {
        return new JAXBElement<String>(_Testmerker_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerfahrenSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "Verfahren")
    public JAXBElement<VerfahrenSType> createVerfahren(VerfahrenSType value) {
        return new JAXBElement<VerfahrenSType>(_Verfahren_QNAME, VerfahrenSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "EingangsDatum")
    public JAXBElement<String> createEingangsDatum(String value) {
        return new JAXBElement<String>(_EingangsDatum_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "DatenArt")
    public JAXBElement<String> createDatenArt(String value) {
        return new JAXBElement<String>(_DatenArt_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NutzdatenHeaderCType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "NutzdatenHeader")
    public JAXBElement<NutzdatenHeaderCType> createNutzdatenHeader(NutzdatenHeaderCType value) {
        return new JAXBElement<NutzdatenHeaderCType>(_NutzdatenHeader_QNAME, NutzdatenHeaderCType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "TransferTicket")
    public JAXBElement<String> createTransferTicket(String value) {
        return new JAXBElement<String>(_TransferTicket_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.elster.de/elsterxml/schema/v11", name = "VersionClient")
    public JAXBElement<String> createVersionClient(String value) {
        return new JAXBElement<String>(_VersionClient_QNAME, String.class, null, value);
    }

}
