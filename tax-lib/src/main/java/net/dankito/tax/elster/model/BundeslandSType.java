
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BundeslandSType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BundeslandSType">
 *   &lt;restriction base="{http://www.elster.de/elsterxml/schema/headerbasis/v3}BaseStringSType">
 *     &lt;enumeration value="BW"/>
 *     &lt;enumeration value="BY"/>
 *     &lt;enumeration value="BE"/>
 *     &lt;enumeration value="BB"/>
 *     &lt;enumeration value="HB"/>
 *     &lt;enumeration value="HH"/>
 *     &lt;enumeration value="HE"/>
 *     &lt;enumeration value="MV"/>
 *     &lt;enumeration value="NI"/>
 *     &lt;enumeration value="NW"/>
 *     &lt;enumeration value="RP"/>
 *     &lt;enumeration value="SL"/>
 *     &lt;enumeration value="SN"/>
 *     &lt;enumeration value="ST"/>
 *     &lt;enumeration value="SH"/>
 *     &lt;enumeration value="TH"/>
 *     &lt;enumeration value="EC"/>
 *     &lt;enumeration value="BF"/>
 *     &lt;enumeration value="CS"/>
 *     &lt;enumeration value="CD"/>
 *     &lt;enumeration value="CM"/>
 *     &lt;enumeration value="CN"/>
 *     &lt;enumeration value="DS"/>
 *     &lt;enumeration value="OP"/>
 *     &lt;enumeration value="TK"/>
 *     &lt;enumeration value="ZF"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BundeslandSType", namespace = "http://www.elster.de/elsterxml/schema/headerbasis/v3")
@XmlEnum
public enum BundeslandSType {


    /**
     * BW = Baden-Württemberg
     * 
     */
    BW,

    /**
     * BY = Bayern
     * 
     */
    BY,

    /**
     * BE = Berlin
     * 
     */
    BE,

    /**
     * BB = Brandenburg
     * 
     */
    BB,

    /**
     * HB = Bremen
     * 
     */
    HB,

    /**
     * HH = Hamburg
     * 
     */
    HH,

    /**
     * HE = Hessen
     * 
     */
    HE,

    /**
     * MV = Mecklenburg-Vorpommern
     * 
     */
    MV,

    /**
     * ND = Niedersachsen
     * 
     */
    NI,

    /**
     * NW = Nordrhein-Westfalen
     * 
     */
    NW,

    /**
     * RP = Rheinland-Pfalz
     * 
     */
    RP,

    /**
     * SL = Saarland
     * 
     */
    SL,

    /**
     * SN = Sachsen
     * 
     */
    SN,

    /**
     * ST = Sachsen-Anhalt
     * 
     */
    ST,

    /**
     * SH = Schleswig-Holstein
     * 
     */
    SH,

    /**
     * TH = Thüringen
     * 
     */
    TH,

    /**
     * EC = ElsterCountry "Testbundesland" - nicht mit Echtdatenlieferung kombinieren !
     * 
     */
    EC,

    /**
     * BF = Bundesamt fuer Finanzen
     * 
     */
    BF,

    /**
     * CS = Clearingstelle"
     * 
     */
    CS,

    /**
     * CD = Clearingstelle Düsseldorf"
     * 
     */
    CD,

    /**
     * CM = ZPS - Kommunikation"
     * 
     */
    CM,

    /**
     * CN = ZPS - Fachlichkeit"
     * 
     */
    CN,

    /**
     * DS = DESTATIS
     * 
     */
    DS,

    /**
     * OP = ElsterOnline Portal
     * 
     */
    OP,

    /**
     * TK = Testcenter Konsens
     * 
     */
    TK,

    /**
     * ZF = ZFA
     * 
     */
    ZF;

    public String value() {
        return name();
    }

    public static BundeslandSType fromValue(String v) {
        return valueOf(v);
    }

}
