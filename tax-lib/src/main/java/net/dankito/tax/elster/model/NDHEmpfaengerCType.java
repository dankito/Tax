
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Ob als Empfaenger ein Finanzamt oder ein Bundesland gesetzt werden muss, ist vom Fachverfahren abhaengig
 * 
 * <p>Java class for NDH_EmpfaengerCType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NDH_EmpfaengerCType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.elster.de/elsterxml/schema/headerbasis/v3>EmpfaengerSType">
 *       &lt;attribute name="id" use="required" type="{http://www.elster.de/elsterxml/schema/headerbasis/v3}EmpfaengerIDSType" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NDH_EmpfaengerCType", propOrder = {
    "value"
})
public class NDHEmpfaengerCType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "id", required = true)
    protected EmpfaengerIDSType id;

    /**
     * Bundeslandkuerzel oder Finanzamtsnummer
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link EmpfaengerIDSType }
     *     
     */
    public EmpfaengerIDSType getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpfaengerIDSType }
     *     
     */
    public void setId(EmpfaengerIDSType value) {
        this.id = value;
    }

}
