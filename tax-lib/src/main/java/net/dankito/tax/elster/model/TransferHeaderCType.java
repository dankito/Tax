
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransferHeaderCType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransferHeaderCType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}Verfahren"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}DatenArt"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}Vorgang"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}TransferTicket" minOccurs="0"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}Testmerker" minOccurs="0"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}SigUser" minOccurs="0"/>
 *         &lt;element name="Empfaenger" type="{http://www.elster.de/elsterxml/schema/v11}EmpfaengerCType" minOccurs="0"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}HerstellerID"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}DatenLieferant"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}EingangsDatum" minOccurs="0"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}Datei"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}RC" minOccurs="0"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}VersionClient" minOccurs="0"/>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}Zusatz" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="version" use="required" type="{http://www.elster.de/elsterxml/schema/headerbasis/v3}headerVersionSType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransferHeaderCType", propOrder = {
    "verfahren",
    "datenArt",
    "vorgang",
    "transferTicket",
    "testmerker",
    "sigUser",
    "empfaenger",
    "herstellerID",
    "datenLieferant",
    "eingangsDatum",
    "datei",
    "rc",
    "versionClient",
    "zusatz"
})
public class TransferHeaderCType {

    @XmlElement(name = "Verfahren", required = true)
    @XmlSchemaType(name = "string")
    protected VerfahrenSType verfahren;
    @XmlElement(name = "DatenArt", required = true)
    protected String datenArt;
    @XmlElement(name = "Vorgang", required = true)
    @XmlSchemaType(name = "string")
    protected VorgangSType vorgang;
    @XmlElement(name = "TransferTicket")
    protected String transferTicket;
    @XmlElement(name = "Testmerker")
    protected String testmerker;
    @XmlElement(name = "SigUser")
    protected SigUser sigUser;
    @XmlElement(name = "Empfaenger")
    protected EmpfaengerCType empfaenger;
    @XmlElement(name = "HerstellerID", required = true)
    protected String herstellerID;
    @XmlElement(name = "DatenLieferant", required = true)
    protected String datenLieferant;
    @XmlElement(name = "EingangsDatum")
    protected String eingangsDatum;
    @XmlElement(name = "Datei", required = true)
    protected Datei datei;
    @XmlElement(name = "RC")
    protected RCCType rc;
    @XmlElement(name = "VersionClient")
    protected String versionClient;
    @XmlElement(name = "Zusatz")
    protected ZusatzCType zusatz;
    @XmlAttribute(name = "version", required = true)
    protected String version;

    /**
     * Gets the value of the verfahren property.
     * 
     * @return
     *     possible object is
     *     {@link VerfahrenSType }
     *     
     */
    public VerfahrenSType getVerfahren() {
        return verfahren;
    }

    /**
     * Sets the value of the verfahren property.
     * 
     * @param value
     *     allowed object is
     *     {@link VerfahrenSType }
     *     
     */
    public void setVerfahren(VerfahrenSType value) {
        this.verfahren = value;
    }

    /**
     * Gets the value of the datenArt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatenArt() {
        return datenArt;
    }

    /**
     * Sets the value of the datenArt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatenArt(String value) {
        this.datenArt = value;
    }

    /**
     * Gets the value of the vorgang property.
     * 
     * @return
     *     possible object is
     *     {@link VorgangSType }
     *     
     */
    public VorgangSType getVorgang() {
        return vorgang;
    }

    /**
     * Sets the value of the vorgang property.
     * 
     * @param value
     *     allowed object is
     *     {@link VorgangSType }
     *     
     */
    public void setVorgang(VorgangSType value) {
        this.vorgang = value;
    }

    /**
     * Gets the value of the transferTicket property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferTicket() {
        return transferTicket;
    }

    /**
     * Sets the value of the transferTicket property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferTicket(String value) {
        this.transferTicket = value;
    }

    /**
     * Gets the value of the testmerker property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestmerker() {
        return testmerker;
    }

    /**
     * Sets the value of the testmerker property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestmerker(String value) {
        this.testmerker = value;
    }

    /**
     * Gets the value of the sigUser property.
     * 
     * @return
     *     possible object is
     *     {@link SigUser }
     *     
     */
    public SigUser getSigUser() {
        return sigUser;
    }

    /**
     * Sets the value of the sigUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link SigUser }
     *     
     */
    public void setSigUser(SigUser value) {
        this.sigUser = value;
    }

    /**
     * Gets the value of the empfaenger property.
     * 
     * @return
     *     possible object is
     *     {@link EmpfaengerCType }
     *     
     */
    public EmpfaengerCType getEmpfaenger() {
        return empfaenger;
    }

    /**
     * Sets the value of the empfaenger property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpfaengerCType }
     *     
     */
    public void setEmpfaenger(EmpfaengerCType value) {
        this.empfaenger = value;
    }

    /**
     * Gets the value of the herstellerID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHerstellerID() {
        return herstellerID;
    }

    /**
     * Sets the value of the herstellerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHerstellerID(String value) {
        this.herstellerID = value;
    }

    /**
     * Gets the value of the datenLieferant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatenLieferant() {
        return datenLieferant;
    }

    /**
     * Sets the value of the datenLieferant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatenLieferant(String value) {
        this.datenLieferant = value;
    }

    /**
     * Gets the value of the eingangsDatum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEingangsDatum() {
        return eingangsDatum;
    }

    /**
     * Sets the value of the eingangsDatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEingangsDatum(String value) {
        this.eingangsDatum = value;
    }

    /**
     * Gets the value of the datei property.
     * 
     * @return
     *     possible object is
     *     {@link Datei }
     *     
     */
    public Datei getDatei() {
        return datei;
    }

    /**
     * Sets the value of the datei property.
     * 
     * @param value
     *     allowed object is
     *     {@link Datei }
     *     
     */
    public void setDatei(Datei value) {
        this.datei = value;
    }

    /**
     * Gets the value of the rc property.
     * 
     * @return
     *     possible object is
     *     {@link RCCType }
     *     
     */
    public RCCType getRC() {
        return rc;
    }

    /**
     * Sets the value of the rc property.
     * 
     * @param value
     *     allowed object is
     *     {@link RCCType }
     *     
     */
    public void setRC(RCCType value) {
        this.rc = value;
    }

    /**
     * Gets the value of the versionClient property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersionClient() {
        return versionClient;
    }

    /**
     * Sets the value of the versionClient property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersionClient(String value) {
        this.versionClient = value;
    }

    /**
     * Gets the value of the zusatz property.
     * 
     * @return
     *     possible object is
     *     {@link ZusatzCType }
     *     
     */
    public ZusatzCType getZusatz() {
        return zusatz;
    }

    /**
     * Sets the value of the zusatz property.
     * 
     * @param value
     *     allowed object is
     *     {@link ZusatzCType }
     *     
     */
    public void setZusatz(ZusatzCType value) {
        this.zusatz = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

}
