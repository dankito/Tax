
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.elster.de/elsterxml/schema/v11}TransferHeader"/>
 *         &lt;element name="DatenTeil" type="{http://www.elster.de/elsterxml/schema/v11}DatenTeilCType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transferHeader",
    "datenTeil"
})
@XmlRootElement(name = "Elster")
public class ElsterXmlFile {

    @XmlElement(name = "TransferHeader", required = true)
    protected TransferHeaderCType transferHeader;
    @XmlElement(name = "DatenTeil", required = true)
    protected DatenTeilCType datenTeil;

    /**
     * Gets the value of the transferHeader property.
     * 
     * @return
     *     possible object is
     *     {@link TransferHeaderCType }
     *     
     */
    public TransferHeaderCType getTransferHeader() {
        return transferHeader;
    }

    /**
     * Sets the value of the transferHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferHeaderCType }
     *     
     */
    public void setTransferHeader(TransferHeaderCType value) {
        this.transferHeader = value;
    }

    /**
     * Gets the value of the datenTeil property.
     * 
     * @return
     *     possible object is
     *     {@link DatenTeilCType }
     *     
     */
    public DatenTeilCType getDatenTeil() {
        return datenTeil;
    }

    /**
     * Sets the value of the datenTeil property.
     * 
     * @param value
     *     allowed object is
     *     {@link DatenTeilCType }
     *     
     */
    public void setDatenTeil(DatenTeilCType value) {
        this.datenTeil = value;
    }

}
