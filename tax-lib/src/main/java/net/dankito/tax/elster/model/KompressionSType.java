
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KompressionSType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="KompressionSType">
 *   &lt;restriction base="{http://www.elster.de/elsterxml/schema/headerbasis/v3}BaseStringSType">
 *     &lt;enumeration value="GZIP"/>
 *     &lt;enumeration value="NO_BASE64"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "KompressionSType", namespace = "http://www.elster.de/elsterxml/schema/headerbasis/v3")
@XmlEnum
public enum KompressionSType {

    GZIP("GZIP"),
    @XmlEnumValue("NO_BASE64")
    NO_BASE_64("NO_BASE64");
    private final String value;

    KompressionSType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static KompressionSType fromValue(String v) {
        for (KompressionSType c: KompressionSType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
