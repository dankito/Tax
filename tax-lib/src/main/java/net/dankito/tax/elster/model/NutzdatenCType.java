
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NutzdatenCType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NutzdatenCType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Anmeldungssteuern" type="{http://www.elster.de/elsterxml/schema/v11}usta_AnmeldungssteuernCType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NutzdatenCType", propOrder = {
    "anmeldungssteuern"
})
public class NutzdatenCType {

    @XmlElement(name = "Anmeldungssteuern", required = true)
    protected UstaAnmeldungssteuernCType anmeldungssteuern;

    /**
     * Gets the value of the anmeldungssteuern property.
     * 
     * @return
     *     possible object is
     *     {@link UstaAnmeldungssteuernCType }
     *     
     */
    public UstaAnmeldungssteuernCType getAnmeldungssteuern() {
        return anmeldungssteuern;
    }

    /**
     * Sets the value of the anmeldungssteuern property.
     * 
     * @param value
     *     allowed object is
     *     {@link UstaAnmeldungssteuernCType }
     *     
     */
    public void setAnmeldungssteuern(UstaAnmeldungssteuernCType value) {
        this.anmeldungssteuern = value;
    }

}
