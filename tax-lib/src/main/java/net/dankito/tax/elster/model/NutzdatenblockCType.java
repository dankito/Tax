
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Der Nutzdatenblock enthält jeweils einen "NutzdatenHeader"- und einen "Nutzdaten"-Tag
 * 
 * <p>Java class for NutzdatenblockCType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NutzdatenblockCType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NutzdatenHeader" type="{http://www.elster.de/elsterxml/schema/v11}NutzdatenHeaderCType"/>
 *         &lt;element name="Nutzdaten" type="{http://www.elster.de/elsterxml/schema/v11}NutzdatenCType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NutzdatenblockCType", propOrder = {
    "nutzdatenHeader",
    "nutzdaten"
})
public class NutzdatenblockCType {

    @XmlElement(name = "NutzdatenHeader", required = true)
    protected NutzdatenHeaderCType nutzdatenHeader;
    @XmlElement(name = "Nutzdaten", required = true)
    protected NutzdatenCType nutzdaten;

    /**
     * Gets the value of the nutzdatenHeader property.
     * 
     * @return
     *     possible object is
     *     {@link NutzdatenHeaderCType }
     *     
     */
    public NutzdatenHeaderCType getNutzdatenHeader() {
        return nutzdatenHeader;
    }

    /**
     * Sets the value of the nutzdatenHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link NutzdatenHeaderCType }
     *     
     */
    public void setNutzdatenHeader(NutzdatenHeaderCType value) {
        this.nutzdatenHeader = value;
    }

    /**
     * Gets the value of the nutzdaten property.
     * 
     * @return
     *     possible object is
     *     {@link NutzdatenCType }
     *     
     */
    public NutzdatenCType getNutzdaten() {
        return nutzdaten;
    }

    /**
     * Sets the value of the nutzdaten property.
     * 
     * @param value
     *     allowed object is
     *     {@link NutzdatenCType }
     *     
     */
    public void setNutzdaten(NutzdatenCType value) {
        this.nutzdaten = value;
    }

}
