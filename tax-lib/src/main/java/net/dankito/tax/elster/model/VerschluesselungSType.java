
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VerschluesselungSType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VerschluesselungSType">
 *   &lt;restriction base="{http://www.elster.de/elsterxml/schema/headerbasis/v3}BaseStringSType">
 *     &lt;enumeration value="PKCS#7v1.5"/>
 *     &lt;enumeration value="PKCS#7v1.5enveloped"/>
 *     &lt;enumeration value="NO_BASE64"/>
 *     &lt;enumeration value="CMSEncryptedData"/>
 *     &lt;enumeration value="CMSEnvelopedData"/>
 *     &lt;enumeration value="EnvelopedData;RSA-OAEP;AES-128;GZip;B64"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "VerschluesselungSType", namespace = "http://www.elster.de/elsterxml/schema/headerbasis/v3")
@XmlEnum
public enum VerschluesselungSType {

    @XmlEnumValue("PKCS#7v1.5")
    PKCS_7_V_1_5("PKCS#7v1.5"),
    @XmlEnumValue("PKCS#7v1.5enveloped")
    PKCS_7_V_1_5_ENVELOPED("PKCS#7v1.5enveloped"),
    @XmlEnumValue("NO_BASE64")
    NO_BASE_64("NO_BASE64"),
    @XmlEnumValue("CMSEncryptedData")
    CMS_ENCRYPTED_DATA("CMSEncryptedData"),
    @XmlEnumValue("CMSEnvelopedData")
    CMS_ENVELOPED_DATA("CMSEnvelopedData"),

    /**
     * Neuer Verschlüsselungstag wird frühestens ab Mai 2016 unterstützt.
     * Bei der Verschlüsselungsart " EnvelopedData;RSA-OAEP;AES-128;GZip;B64" muss der symmetrische Teil mit AES128 verschlüsselt sein und der asymmetrische Teil mit RSAES-OAEP mit der Hashfunktion SHA256, der Maskengenerierungsfunktion MGF1 und als Hashfunktion der Maskengenerierungsfunktion ebenso SHA256.
     * 
     */
    @XmlEnumValue("EnvelopedData;RSA-OAEP;AES-128;GZip;B64")
    ENVELOPED_DATA_RSA_OAEP_AES_128_G_ZIP_B_64("EnvelopedData;RSA-OAEP;AES-128;GZip;B64");
    private final String value;

    VerschluesselungSType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VerschluesselungSType fromValue(String v) {
        for (VerschluesselungSType c: VerschluesselungSType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
