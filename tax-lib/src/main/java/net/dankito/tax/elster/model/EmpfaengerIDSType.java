
package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmpfaengerIDSType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EmpfaengerIDSType">
 *   &lt;restriction base="{http://www.elster.de/elsterxml/schema/headerbasis/v3}BaseStringSType">
 *     &lt;length value="1"/>
 *     &lt;enumeration value="L"/>
 *     &lt;enumeration value="F"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EmpfaengerIDSType", namespace = "http://www.elster.de/elsterxml/schema/headerbasis/v3")
@XmlEnum
public enum EmpfaengerIDSType {

    L,
    F;

    public String value() {
        return name();
    }

    public static EmpfaengerIDSType fromValue(String v) {
        return valueOf(v);
    }

}
