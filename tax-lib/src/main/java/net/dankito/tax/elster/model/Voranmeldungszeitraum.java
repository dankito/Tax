package net.dankito.tax.elster.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


@XmlEnum
public enum Voranmeldungszeitraum {

    @XmlEnumValue("01")
    Januar("01"),

    @XmlEnumValue("02")
    Februar("02"),

    @XmlEnumValue("03")
    März("03"),

    @XmlEnumValue("04")
    April("04"),

    @XmlEnumValue("05")
    Mai("05"),

    @XmlEnumValue("06")
    Juni("06"),

    @XmlEnumValue("07")
    Juli("07"),

    @XmlEnumValue("08")
    August("08"),

    @XmlEnumValue("09")
    September("09"),

    @XmlEnumValue("10")
    Oktober("10"),

    @XmlEnumValue("11")
    November("11"),

    @XmlEnumValue("12")
    Dezember("12"),

    @XmlEnumValue("41")
    I_Kalendervierteljahr("41"),

    @XmlEnumValue("42")
    II_Kalendervierteljahr("42"),

    @XmlEnumValue("43")
    III_Kalendervierteljahr("43"),

    @XmlEnumValue("44")
    IV_Kalendervierteljahr("44");
 

    private String ziffer;

    Voranmeldungszeitraum(String ziffer) {
        this.ziffer = ziffer;
    }


    public String getZiffer() {
        return ziffer;
    }
    
}
