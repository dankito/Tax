package net.dankito.tax.elster.model


class ElsterFileCreationTransactionResult(successful: Boolean,
                                          val xmlString: String,
                                          errors: List<ElsterError> = listOf(),
                                          userInfo: List<String> = listOf())
    : ElsterTransactionResult(successful, errors, userInfo) {


    constructor(successful: Boolean, error: ElsterError): this(successful, "", listOf(error))

}