package net.dankito.tax.elster.model


enum class Datenart(val art: String) {

    UStVA("UStVA")

}