package net.dankito.tax.elster.model

import com.fasterxml.jackson.annotation.JsonIgnore


open class Finanzamt(val name: String, val finanzamtsnummer: Int) {

    private constructor() : this("", -1)  // for object deserializers


    val finanzamtsnummerAsString: String
        @JsonIgnore
        get() = finanzamtsnummer.toString()


    override fun toString(): String {
        return "$name ($finanzamtsnummer)"
    }

}