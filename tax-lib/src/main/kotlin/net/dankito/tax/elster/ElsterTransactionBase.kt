package net.dankito.tax.elster

import de.elster.eric.wrapper.*
import de.elster.eric.wrapper.exception.EricException
import de.elster.eric.wrapper.exception.ResponseException
import de.elster.model.bearbeitevorgang.EricBearbeiteVorgang
import de.elster.model.bearbeitevorgang.FehlerRegelpruefungTyp
import de.elster.model.bearbeitevorgang.HinweisOderFehlerBase
import net.dankito.tax.elster.model.*
import net.dankito.tax.elster.model.Finanzamt
import net.dankito.tax.elster.service.XmlSerializer
import net.dankito.utils.io.FileUtils
import org.slf4j.LoggerFactory
import java.io.File
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*


abstract class ElsterTransactionBase(protected val eric: Eric) {

    companion object {
        val ElsterCurrencyFormat = NumberFormat.getCurrencyInstance(Locale.GERMANY)

        val ErstellungsdatumDateFormat = SimpleDateFormat("yyyyMMdd")

        const val ElsterXmlSchemaVersion = "11"

        private val log = LoggerFactory.getLogger(ElsterTransactionBase::class.java)


        init {
            ElsterCurrencyFormat.isGroupingUsed = false // don't print for example 10.000, Elster doesn't like that, print 10000 instead
        }
    }


    protected open val xmlSerializer = XmlSerializer() // TODO: inject singleton instance


    open fun doTransaction(config: ElsterTransactionConfig): ElsterTransactionResult {
        val xmlFileAsString = config.xmlFileAsString

        config.saveCreatedXmlFileTo?.let {
            saveCreatedXmlFileTo(it, xmlFileAsString)
        }


        val datenartVersion = createDatenartVersion(config.steuerfall, config.jahr)

        val validationResult = validate(datenartVersion, xmlFileAsString)

        if (validationResult.successful) {
            val certificate = CertificateParameters(EricCertificate(eric.ericApiLib, config.certificateFile.absolutePath, config.certificatePin), config.certificatePin)

            return sendAndPrint(datenartVersion, xmlFileAsString, certificate, config.pdfOutputPath)
        }

        return validationResult
    }

    protected open fun createDatenartVersion(steuerfall: Steuerfall, jahr: Steuerjahr)
            = steuerfall.datenart + "_" + jahr.jahr

    protected open fun validate(datenartVersion: String, xmlFileAsString: String): ElsterTransactionResult {
        try {
            eric.validate(datenartVersion, xmlFileAsString)

            return ElsterTransactionResult(true)
        } catch (e: Exception) {
            log.error("Could not validate Steuerfall $datenartVersion", e)

            return getTransactionResultFromException(e)
        }
    }


    protected open fun sendAndPrint(datenartVersion: String, xmlFileAsString: String, certificate: CertificateParameters,
                                    pdfOutputPath: File?): ElsterTransactionResult {

        try {
            val pdfOutputFile = pdfOutputPath?.path ?: "ericprint.pdf"

            val response = eric.sendAndPrint(
                datenartVersion, xmlFileAsString, certificate,
                PrintParameters(0, 0, 0, pdfOutputFile, null)
            )

            if (response.serverResponse.isNotEmpty()) {
                return mapServerResponse(response)
            } else {
                return ElsterTransactionResult(false)
            }
        } catch (e: Exception) {
            log.error("Could not send and print $datenartVersion. If input XML file should be printed to log set log level to DEBUG", e)
            log.debug("XML file was: $xmlFileAsString")

            return getTransactionResultFromException(e)
        }
    }

    protected open fun mapServerResponse(response: Eric.SendResponse): ElsterTransactionResult {
        val deserializedXml = deserializeElsterXmlFile(response.serverResponse)

        val responseCode = deserializedXml.transferHeader.rc.rueckgabe.code
        val successfullyTransferred = responseCode == "0"

        val nutzdatenBlock = deserializedXml.datenTeil?.nutzdatenblock

        // Bei Verwendung von ERiC kommt bei der Abgabe von bestimmten Datenlieferungen in der Serverantwort
        // kein Inhalt im Element DatenTeil auch bei Online-Verfahren.
        // Hier kann der Anwender bei einem Online-Verfahren sicher sein, dass seine Daten entgegengenommen
        // und verarbeitet wurden, wenn im TransferHeader unter <Rueckgabe> <Code> eine „0“ eingetragen ist und
        // der Inhalt des Elements „DatenTeil“ leer ist.
        val nutzdatenSuccessfullyValidated = nutzdatenBlock.isNullOrEmpty() ||
                nutzdatenBlock.get(0)?.nutzdatenHeader?.rc?.rueckgabe?.code == "0"

        val successful = successfullyTransferred and nutzdatenSuccessfullyValidated

        log.info("successfullyTransferred = $successfullyTransferred, nutzdatenSuccessfullyValidated = $nutzdatenSuccessfullyValidated")

        return ElsterTransactionResult(
            successful,
            listOf(ElsterError(responseCode.toInt(), deserializedXml.transferHeader.rc.rueckgabe.text, getHintsAndErrors(response.ericResult, "Server"))),
            mapInfo(deserializedXml.transferHeader.zusatz))
    }

    protected open fun mapInfo(zusatz: ZusatzCType?): List<String> {
        val userInfo = ArrayList<String>()

        zusatz?.let {
            userInfo.addAll(zusatz.info)
            userInfo.addAll(zusatz.elsterInfo)
        }

        return userInfo
    }


    /**
     * Zu Beachten:
     * In Hessen wird die führende 0 häufig von Steuerberatern weggelassen (bei Eingabe in
     * externem Programm berücksichtigen!).
     *
     * In Berlin gibt es nur noch das neue Format, bei dem die letzten zwei Stellen der Bundes-
     * finanzamtsnummer mit ausgewiesen werden. Darüber hinaus werden auch führende Nullen
     * beim Bezirk und bei der Unterscheidungsnummer immer gedruckt.
     *
     * In Bayern wird zunehmend bei bestimmten Finanzämtern auf die Angabe der
     * Finanzamtsnummer (FFF) verzichtet (bei Eingabe in externem Programm berücksichtigen!).
     */
    protected fun makeElsterSteuernummer(steuernummerAufBescheiden: String, finanzamt: Finanzamt): String {
        val steuernummerAufBescheidenErrorCode = eric.checkStNr(steuernummerAufBescheiden)
        if (steuernummerAufBescheidenErrorCode.errorcode == 0) { // already a valid Elster Steuernummer, no conversion needed
            return steuernummerAufBescheiden
        }

        return eric.makeElsterSteuernummer(steuernummerAufBescheiden, finanzamt.finanzamtsnummerAsString)
    }

    protected open fun createTransferHeader(config: TransferHeader): TransferHeaderCType {
        val transferHeader = TransferHeaderCType()

        transferHeader.version = ElsterXmlSchemaVersion
        transferHeader.verfahren = config.verfahren
        transferHeader.datenArt = config.datenart.art
        transferHeader.vorgang = config.vorgang
        transferHeader.testmerker = config.testMerker?.merker
        transferHeader.herstellerID = config.herstellerID
        transferHeader.datenLieferant = config.datenLieferant

        transferHeader.datei = Datei()
        transferHeader.datei.verschluesselung = config.verschluesselung
        transferHeader.datei.kompression = config.kompression
        transferHeader.datei.transportSchluessel = config.transportSchluessel

        transferHeader.rc = RCCType()
        transferHeader.rc.rueckgabe = RCCType.Rueckgabe()
        transferHeader.rc.rueckgabe.code = "0"
        transferHeader.rc.rueckgabe.text =
                "" // mandatory field; if not set to an empty string Jaxb doesn't write this element
        transferHeader.rc.stack = RCCType.Stack()
        transferHeader.rc.stack.code = "0"
        transferHeader.rc.stack.text =
                "" // mandatory field; if not set to an empty string Jaxb doesn't write this element

        transferHeader.versionClient = config.versionClient // optional

        return transferHeader
    }


    /**
     * ERic:
     * Geldbeträge müssen vom Format '0.00' oder '-0.00' mit genau 2 Nachkommastellen sein. Leerzeichen oder **Kommata** sind nicht zulässig.
     */
    open fun createEricCurrencyString(value: Double): String {
        return createElsterOnlineCurrencyString(value).replace(",", ".")
    }

    /**
     * ElsterOnline:
     * Geldbeträge müssen vom Format '0,00' oder '-0,00' mit genau 2 Nachkommastellen sein. Leerzeichen oder **Punkte** sind nicht zulässig.
     */
    open fun createElsterOnlineCurrencyString(value: Double): String {
        return ElsterCurrencyFormat.format(value).replace(" €", "")
    }


    protected open fun getTransactionResultFromException(exception: Exception): ElsterTransactionResult {
        return ElsterTransactionResult(false, getErrorFromException(exception))
    }

    protected open fun getErrorFromException(exception: Exception): ElsterError {
        return when (exception) {
            is ResponseException -> {
                val serverResponse = if (exception.serverResponse.isNullOrBlank()) null else exception.serverResponse
                ElsterError(exception.errorCode.errorcode, exception.errorMessage, getHintsAndErrors(exception.ericResult, "Validation"), serverResponse)
            }
            is EricException -> ElsterError(exception.errorCode.errorcode, exception.errorMessage)
            else -> ElsterError(-1, exception.localizedMessage)
        }
    }


    protected open fun saveCreatedXmlFileTo(xmlOutputFile: File, xmlFileAsString: String) {
        try {
            xmlOutputFile.parentFile.mkdirs()

            FileUtils().writeToTextFile(xmlFileAsString, xmlOutputFile)
        } catch (e: Exception) {
            log.error("Could not write created XML file to '$xmlOutputFile'\nXML file is:\n", e)
        }
    }


    protected open fun deserializeElsterXmlFile(xmlFile: String): ElsterXmlFile {
        return xmlSerializer.deserialize(xmlFile, ElsterXmlFile::class.java)
    }

    protected open fun serializeElsterXmlFile(xmlFile: ElsterXmlFile): String {
        return xmlSerializer.serialize(xmlFile)
    }


    protected open fun getHintsAndErrors(ericResult: String?, logCategory: String = "Validation"): List<HinweisOderFehlerBase> {
        if (ericResult.isNullOrBlank() == false) {
            try {
                val hintsAndErrors = deserializeEricBearbeiteVorgang(ericResult).fehlerRegelpruefungOrHinweis

                hintsAndErrors.forEach { hintOrError ->
                    // TODO: may add other information from HinweisOderFehlerBase
                    val message = "'${hintOrError.fachlicheId}': ${hintOrError.text}"
                    if (hintOrError is FehlerRegelpruefungTyp) {
                        log.error("$logCategory error: $message")
                    }
                    else {
                        log.warn("$logCategory hint: $message")
                    }
                }
            } catch (e: Exception) {
                log.error("Could not parse EricBearbeiteVorgang", e)
            }
        }

        return listOf()
    }

    protected open fun deserializeEricBearbeiteVorgang(xml: String): EricBearbeiteVorgang {
        return xmlSerializer.deserialize(xml, EricBearbeiteVorgang::class.java)
    }

}