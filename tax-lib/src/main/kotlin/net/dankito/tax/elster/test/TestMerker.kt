package net.dankito.tax.elster.test


/**
 * Entnommen Tabelle 6-7 Testmerker, S. 133 ERiC Entwicklerhandbuch.
 */
enum class TestMerker(val merker: String) {

    /**
     * Testfall für KapEStA und KapEStInvStG.
     */
    T_160000001("160000001"),

    /**
     * Testfall für KapEStA und KapEStInvStG.
     */
    T_160000002("160000002"),

    /**
     * Testfall für LStB, nur Validierung in dem ELSTER Annahmeserver und Protokollerstellung.
     */
    T_220000000("220000000"),

    /**
     * Testfall für LStB, Testfall als Kompletttest mit Einspeicherung in den eSpeicher des jeweiligen Landes.
     */
    T_220002000("220002000"),

    /**
     * Testfall für Steuerkontoabfrage. Nur gültig mit Teststeuernummer.
     */
    T_230000001("230000001"),

    /**
     * Testfall für das Verfahren ElsterKMV, siehe Kap. 9.7
     */
    T_240000000("240000000"),

    /**
     * Testfall für VaSt, Verfahren ElsterBRM, siehe Kap. 9.8.8
     */
    T_370000001("370000001"),

    /**
     * Testfall für VaSt, Verfahren ElsterSignatur, siehe Kap. 9.8.8
     */
    T_080000001("080000001"),

    /**
     * Testfall, der sofort nach dem Empfang im jeweiligen Rechenzentrum des Bundeslandes gelöscht wird. Es erfolgt
     * keine weitere Verarbeitung. Dientzum Test der Leitungsverbindung.Dieser Testmerker ist bei KapEStA und
     * KapEStInvStG nicht zulässig.
     */
    T_700000001("700000001"),

    /**
     * Testfall, der sofort nach Eingang in dem ELSTER Annahmeserver gelöscht wird. Es erfolgt keine weitere
     * Verarbeitung. Dient dem externen Softwarehersteller zum Testen der Datenübertragung.
     */
    T_700000004("700000004"),

    /**
     * Testfall. Mit diesem Testmerker ist es möglich eine Server-Testantwort mit mehreren gefüllten Tags <ElsterInfo>
     * zu erhalten. Der versandte Steuerfall wird in dem ELSTER Annahmeserver direkt ausgesteuert. Ziel dieses
     * Testmerkers ist es, das Verhalten einer Serverantwort mit gefülltem Element <ElsterInfo>40 zu testen.
     */
    T_010000001("010000001"),

    /**
     * Testfall für nPA oder eAT. Bewirkt die Verwendung eines Testkontos. Dieser Testmerker wird nicht im XML
     * angegeben, sondern an die eID-Client-URL angehängt siehe Kap. 5.2.4.
     */
    T_520000000("520000000");

}