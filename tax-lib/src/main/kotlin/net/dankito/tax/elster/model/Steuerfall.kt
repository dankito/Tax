package net.dankito.tax.elster.model


enum class Steuerfall(val datenart: String) {

    UmsatzsteuerVoranmeldung("UStVA");

}