package net.dankito.tax.elster

import de.elster.eric.wrapper.Eric
import de.elster.eric.wrapper.FinanzamtLand
import de.elster.eric.wrapper.LogCallback
import de.elster.model.finanzamtsdaten.EricHoleFinanzamtsdaten
import net.dankito.tax.elster.model.*
import net.dankito.tax.elster.service.XmlSerializer
import java.io.File


open class ElsterClient @JvmOverloads constructor(logFileFolder: File? = null) : AutoCloseable {

    protected open val ericFactory = EricFactory()

    protected open val eric: Eric = ericFactory.createEric(logFileFolder)

    protected open val settings: EricSettings = EricSettings(eric)

    protected open val xmlSerializer = XmlSerializer() // TODO: inject singleton instance


    override fun close() {
        ericFactory.releaseEric(eric)
    }


    /**
     * Sets log level to TRACE.
     *
     * (And no: ERiC has no option to set log level to other then INFO or TRACE.)
     */
    open fun setLogLevelToDetailed() {
        settings.setLogLevelToDetailed()
    }

    /**
     * Sets log level to INFO.
     *
     * (And no: ERiC has no option to set log level to other then INFO or TRACE.)
     */
    open fun setLogLevelToNormal() {
        settings.setLogLevelToNormal()
    }

    @JvmOverloads
    /**
     * Registers a [LogCallback] that gets notified of all messages that ERiC logs.
     *
     * Only one log callback can be registered. Calling this method twice results in unregistering the first callback.
     *
     * When callback isn't needed anymore, call [unregisterLogCallback].
     *
     * Be aware: If you want to have a finer log level than INFO call call [setLogLevelToDetailed]. But logging at TRACE level really logs a lot of messages.
     *
     * **Caution:**
     * - It's not allowed to all other ERiC API-Functions in [LogCallback.log].
     * - As [LogCallback.log] gets called synchronously the method should execute very fast to not block ERiC processing.
     */
    open fun registerLogCallback(callback: LogCallback, alsoWriterToEricLogFile: Boolean = true) {
        eric.registerLogCallback(callback, alsoWriterToEricLogFile)
    }

    /**
     * Unregisters the last [LogCallback] registered with [registerLogCallback].
     */
    open fun unregisterLogCallback() {
        eric.unregisterLogCallback()
    }


    open fun createAndValidateUmsatzsteuerVoranmeldungXmlFile(data: UmsatzsteuerVoranmeldung): ElsterFileCreationTransactionResult {
        return UmsatzsteuerVoranmeldungElsterTransaction(eric).createAndValidateUmsatzsteuerVoranmeldungXmlFile(data)
    }

    open fun createUmsatzsteuerVoranmeldungXmlFile(data: UmsatzsteuerVoranmeldung): String {
        return UmsatzsteuerVoranmeldungElsterTransaction(eric).createUmsatzsteuerVoranmeldungXmlFile(data)
    }

    open fun makeUmsatzsteuerVoranmeldung(data: UmsatzsteuerVoranmeldung): ElsterTransactionResult {
        return UmsatzsteuerVoranmeldungElsterTransaction(eric).makeUmsatzsteuerVoranmeldung(data)
    }


    open fun getFinanzämter(): Map<Bundesland, List<Finanzamt>> {
        val finanzamtLaender = eric.finanzamtLaender

        val bundeslaender = mapFinanzamtLaenderToBundeslaender(finanzamtLaender)

        return bundeslaender.associateBy( { it }, { getFinanzamterForBundesland(it) } )
    }

    protected open fun getFinanzamterForBundesland(bundesland: Bundesland): List<Finanzamt> {
        return eric.getFinanzaemter(bundesland.elsterFinanzamtLandId).map { Finanzamt(it.name, it.id) }
    }

    protected open fun mapFinanzamtLaenderToBundeslaender(finanzamtLaender: List<FinanzamtLand>): List<Bundesland> {
        return finanzamtLaender.map { mapFinanzamtLandToBundesland(it) }
    }

    protected open fun mapFinanzamtLandToBundesland(finanzamtLand: FinanzamtLand): Bundesland {
        return Bundesland(finanzamtLand.land, finanzamtLand.id, getFinanzamtsNummerType(finanzamtLand.land))
    }

    protected open fun getFinanzamtsNummerType(federalState: String): FinanzamtsNummerType {
        return when {
            FinanzamtsNummerType.FederalStatesWhoseTaxNumbersStartsWithTwoDigits.contains(federalState) -> FinanzamtsNummerType.TwoDigits
            FinanzamtsNummerType.FederalStatesWhoseTaxNumbersStartsWithThreeDigits.contains(federalState) -> FinanzamtsNummerType.ThreeDigits
            FinanzamtsNummerType.FederalStatesWhoseTaxNumbersStartsWithZeroFollowedByTwoDigits.contains(federalState) -> FinanzamtsNummerType.TwoDigitsWithALeadingZero
            else -> FinanzamtsNummerType.ThreeDigits // to make compiler happy
        }
    }


    open fun getFinanzamtsdaten(finanzamt: Finanzamt): FinanzamtsDaten {
        return getFinanzamtsdaten(finanzamt.finanzamtsnummer)
    }

    open fun getFinanzamtsdaten(bundesfinanzamtsnummer: Int): FinanzamtsDaten {
        val finanzamtsdatenXml = eric.getFinanzamtsdaten(bundesfinanzamtsnummer)

        val daten = deserializeFinanzamtsdaten(finanzamtsdatenXml).finanzamtsdaten
        val mainBranch = daten.hauptstelle?.let { Finanzamt(it.name.replace("Außenstelle v. ", ""), it.buFaNr.toInt())}

        return FinanzamtsDaten(daten.name ?: "", daten.buFaNr.toInt(), daten.adresseListe?.adresse ?: listOf(),
            daten.kontaktListe?.kontakt ?: listOf(), daten.bankverbindungListe?.bankverbindung ?: listOf(),
            daten.oeffnungszeitListe?.oeffnungszeit ?: listOf(), daten.bemerkungListe?.bemerkung ?: listOf(), mainBranch)
    }

    protected open fun deserializeFinanzamtsdaten(xml: String): EricHoleFinanzamtsdaten {
        return xmlSerializer.deserialize(xml, EricHoleFinanzamtsdaten::class.java)
    }

}