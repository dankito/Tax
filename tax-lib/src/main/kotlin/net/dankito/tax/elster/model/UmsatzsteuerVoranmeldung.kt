package net.dankito.tax.elster.model

import java.io.File


class UmsatzsteuerVoranmeldung(val jahr: Steuerjahr,
                               val zeitraum: Voranmeldungszeitraum,
                               val finanzamt: Finanzamt,
                               val steuernummerAufBescheiden: String,
                               val steuerpflichtiger: Steuerpflichtiger,
                               val certificateFile: File,
                               val certificatePin: String,
                               val nettoEinnahmenZu19ProzentInEuroOhneCentBetrag: Int,
                               val nettoEinnahmenZu7ProzentInEuroOhneCentBetrag: Int,
                               val nettoEinnahmenZuAnderenProzentsätzenInEuroOhneCentBetrag: Int? = null,
                               val umsatzsteuerDerNettoEinnahmenZuAnderenProzentsätzenInEuroUndCentBetrag: Double? = null,
                               val abziehbareVorsteuerbetraegeAusRechnungenVonAnderenUnternehmernEuroUndCentBetrag: Double,
                               // Summe aller Umsatzsteuern (= Summe der Mehrwertsteuer aller Einnahmen) Minus
                               // Summe Vorsteuer (= Summe der Mehrwersteuer aller Ausgaben)
                               val verbleibendeUmsatzsteuervorauszahlungEuroUndCentBetrag: Double,
                               val herstellerID: String,
                               val pdfOutputPath: File? = null,
                               val saveCreatedXmlFileTo: File? = null,
                               val isBerichtigteAnmeldung: Boolean = false
)