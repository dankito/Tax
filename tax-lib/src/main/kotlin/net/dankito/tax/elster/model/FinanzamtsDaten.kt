package net.dankito.tax.elster.model

import de.elster.model.finanzamtsdaten.AdresseType
import de.elster.model.finanzamtsdaten.BankverbindungType
import de.elster.model.finanzamtsdaten.KontaktType


open class FinanzamtsDaten(
    name: String,
    bundesfinanzamtsnummer: Int,
    val addresses: List<AdresseType>,
    val contactList: List<KontaktType>,
    val bankDetails: List<BankverbindungType>,
    val openingHours: List<String>,
    val remarks: List<String>,
    val mainBranch: Finanzamt?
) : Finanzamt(name, bundesfinanzamtsnummer)