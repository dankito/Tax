package net.dankito.tax.elster

import net.dankito.tax.elster.model.Steuerfall
import net.dankito.tax.elster.model.Steuerjahr
import java.io.File


class ElsterTransactionConfig(val steuerfall: Steuerfall,
                              val jahr: Steuerjahr,
                              val certificateFile: File,
                              val certificatePin: String,
                              val xmlFileAsString: String,
                              val pdfOutputPath: File? = null,
                              val saveCreatedXmlFileTo: File? = null)