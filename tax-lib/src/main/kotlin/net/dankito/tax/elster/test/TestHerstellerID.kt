package net.dankito.tax.elster.test


/**
 * Entnommen Kapitel 6.5.3 Test-HerstellerID, S. 132 ERiC Entwicklerhandbuch.
 */
enum class TestHerstellerID(val herstellerID: String) {

    /**
     * Bis zur Zuteilung einer eigenen HerstellerID kann der Softwarehersteller die TestHerstellerID = 74931 beim
     * Versenden von Testfällen verwenden. Diese Testfälle werden am ELSTER Annahmeserver gelöscht, siehe Kap. 6.5.5.
     * Bei Echtfällen ist die zugeteilte HerstellerID zu verwenden. Eine HerstellerID kann auf der ELSTER Webseite
     * (Ressourcen) beantragt werden.
     */
    T_74931("74931")

}