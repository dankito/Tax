package net.dankito.tax.elster

import java.io.File


class Paths {

    companion object {

        fun getSteuerfallXml(steuerfallName: String) =
            File("data/steuerfaelle/$steuerfallName.xml")

    }

}