package net.dankito.tax.elster.model


open class Bundesland(val name: String, val elsterFinanzamtLandId: Int, val type: FinanzamtsNummerType) {

    internal constructor() : this("", -1, FinanzamtsNummerType.TwoDigits) // for object deserializers


    override fun toString(): String {
        return name
    }

}