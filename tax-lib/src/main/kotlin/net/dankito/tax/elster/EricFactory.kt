package net.dankito.tax.elster

import com.sun.jna.Platform
import de.elster.eric.wrapper.Eric
import net.dankito.utils.resources.ResourceFilesExtractor
import org.slf4j.LoggerFactory
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


open class EricFactory {

    companion object {

        private const val DefaultLogFileFolder = "logs"

        private val EricDefaultLogFilename = "eric.log"

        private val LogFileDateTimeFormat = SimpleDateFormat("yyyy.MM.dd_HH-mm-ss")


        private var didAlreadyExtractLibraries = false


        private val log = LoggerFactory.getLogger(EricFactory::class.java)

    }


    protected lateinit var logFileFolder: File // TODO: save for each created ERiC instance

    protected val applicationStartupTime = Date() // TODO: save for each created ERiC instance


    @JvmOverloads
    open fun createEric(logFileFolder: File? = null): Eric {
        this.logFileFolder = logFileFolder ?: File(DefaultLogFileFolder)
        this.logFileFolder.mkdirs()

        log.info("ERiC log files folder is ${this.logFileFolder.absolutePath}")

        val libFolder = ensureEricFilesAreExtractedFromJar()

        return Eric(libFolder.absolutePath, this.logFileFolder.path)
    }

    open fun releaseEric(eric: Eric) {
        eric.close()

        // prepend application start up time to log file so that we know which log file was created by which app run
        val logFile = File(logFileFolder, EricDefaultLogFilename)
        if (logFile.exists()) {
            val logFilenameWithApplicationStartupTime = LogFileDateTimeFormat.format(applicationStartupTime) + "_" +
                    EricDefaultLogFilename

            val logFileWithApplicationStartupTime = File(logFileFolder, logFilenameWithApplicationStartupTime)
            logFile.renameTo(logFileWithApplicationStartupTime)
        }
    }


    protected open fun ensureEricFilesAreExtractedFromJar(): File {
        val platformLibFolder = getPlatformLibFolder()
        val sourceFolder = "lib/$platformLibFolder" // do not use File() as in Windows this would produce lib\platformLibFolder
        val ericDestinationFolder = File("lib", platformLibFolder)

        if (didAlreadyExtractLibraries == false) {
            log.info("platformLibFolder = $platformLibFolder, extracting from ${File(sourceFolder)} ($sourceFolder) to " +
                    ericDestinationFolder.absolutePath)

            val resourceFilesExtractor = ResourceFilesExtractor()

            try {
                resourceFilesExtractor.extractAllFilesFromFolder(ElsterClient::class.java,
                    File(sourceFolder), ericDestinationFolder, true)

                didAlreadyExtractLibraries = true
            } catch (e: Exception) { log.error("Could not call extractAllFilesFromFolder()", e) }
        }

        return ericDestinationFolder
    }

    protected open fun getPlatformLibFolder(): String {

        val platformName = when {
            Platform.isLinux() -> "Linux"
            Platform.isWindows() -> "Windows"
            Platform.isMac() -> "Darwin"
            else -> throw IllegalStateException(
                "Sorry, your platform ${System.getProperty("os.name")} is not supported." +
                        "${System.lineSeparator()}Supported platforms are: Linux, Windows and MacOs"
            )
        }

        val archSuffix = when {
            Platform.is64Bit() -> "x86_64"
            Platform.isLinux() -> "i686" // from here on: determine suffixes for 32 bit OSes
            Platform.isWindows() -> "x86"
            Platform.isMac() -> "i386"
            else -> "" // Exception that platform is unsupported already thrown above
        }

        return platformName + "-" + archSuffix
    }

}