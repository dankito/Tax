package net.dankito.tax.elster.model


enum class Steuerjahr(val jahr: String) {

    Jahr2019("2019"),


    Jahr2020("2020")

}