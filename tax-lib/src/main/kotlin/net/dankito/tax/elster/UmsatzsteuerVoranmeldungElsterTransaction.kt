package net.dankito.tax.elster

import de.elster.eric.wrapper.Eric
import net.dankito.tax.elster.model.*
import net.dankito.tax.elster.test.TestHerstellerID
import net.dankito.tax.elster.test.TestMerker
import java.util.*


open class UmsatzsteuerVoranmeldungElsterTransaction(eric: Eric) : ElsterTransactionBase(eric) {

    companion object {
        val TransactionSteuerfall = Steuerfall.UmsatzsteuerVoranmeldung
    }


    open fun makeUmsatzsteuerVoranmeldung(data: UmsatzsteuerVoranmeldung): ElsterTransactionResult {
        val xmlString = createUmsatzsteuerVoranmeldungXmlFile(data)

        val configWithXmlFile = ElsterTransactionConfig(TransactionSteuerfall, data.jahr, data.certificateFile,
            data.certificatePin, xmlString, data.pdfOutputPath, data.saveCreatedXmlFileTo)

        return super.doTransaction(configWithXmlFile)
    }


    open fun createAndValidateUmsatzsteuerVoranmeldungXmlFile(data: UmsatzsteuerVoranmeldung): ElsterFileCreationTransactionResult {
        val xmlString = createUmsatzsteuerVoranmeldungXmlFile(data)

        val validationResult = validate(createDatenartVersion(TransactionSteuerfall, data.jahr), xmlString)

        return ElsterFileCreationTransactionResult(validationResult.successful, xmlString, validationResult.errors,
            validationResult.userInfo)
    }

    open fun createUmsatzsteuerVoranmeldungXmlFile(data: UmsatzsteuerVoranmeldung): String {

        val xmlFile = ElsterXmlFile()

        val datenart = Datenart.UStVA
        val datenlieferantName = data.steuerpflichtiger.name

        val elsterSteuernummer = makeElsterSteuernummer(data.steuernummerAufBescheiden, data.finanzamt)

        val herstellerID = data.herstellerID
        val versionClient = "MeinSteuerprogramm 1.0" // optional
        val nutzdatenTicketNummer = "234234234" // muss nur innerhalb einer XML Datei eindeutig sein (bei einer Einzellieferung immer gegeben) -> es kann ein konstanter Wert verwendet werden

        xmlFile.transferHeader = createTransferHeader(TransferHeader(VerfahrenSType.ELSTER_ANMELDUNG,
            datenart, VorgangSType.SEND_AUTH, herstellerID, datenlieferantName,
            VerschluesselungSType.CMS_ENCRYPTED_DATA, KompressionSType.GZIP, "",
            versionClient,
            if (herstellerID == TestHerstellerID.T_74931.herstellerID) TestMerker.T_700000004 else null )) // for test herstellerIDs set TestMerker

        xmlFile.datenTeil = DatenTeilCType()

        val nutzdatenblock = NutzdatenblockCType()
        xmlFile.datenTeil.nutzdatenblock.add(nutzdatenblock)

        val nutzdatenHeader = NutzdatenHeaderCType()
        nutzdatenHeader.version = ElsterXmlSchemaVersion
        nutzdatenblock.nutzdatenHeader = nutzdatenHeader

        nutzdatenHeader.nutzdatenTicket = nutzdatenTicketNummer

        val nutzdatenHeaderEmpfaenger = NDHEmpfaengerCType()
        nutzdatenHeaderEmpfaenger.id = EmpfaengerIDSType.F
        nutzdatenHeaderEmpfaenger.value = data.finanzamt.finanzamtsnummerAsString
        nutzdatenHeader.empfaenger = nutzdatenHeaderEmpfaenger

        val nutzdaten = NutzdatenCType()
        nutzdatenblock.nutzdaten = nutzdaten

        val anmeldungssteuern = UstaAnmeldungssteuernCType()
        anmeldungssteuern.art = datenart.art
        anmeldungssteuern.version = "202001" // see in UstaAnmeldungssteuern's class comment: attribute name="version"

        anmeldungssteuern.datenLieferant = mapSteuerpflichtigerToDatenlieferant(data.steuerpflichtiger)

        anmeldungssteuern.erstellungsdatum = ErstellungsdatumDateFormat.format(Date())

        val steuerfall = UstaSteuerfallCType()
        anmeldungssteuern.steuerfall = steuerfall

        val unternehmer = mapSteuerpflichtigerToUnternehmer(data.steuerpflichtiger)
        steuerfall.unternehmer = unternehmer

        steuerfall.umsatzsteuervoranmeldung =
                createUmsatzsteuervoranmeldungSteuerfall(data, elsterSteuernummer, herstellerID)

        nutzdaten.anmeldungssteuern = anmeldungssteuern


        return serializeElsterXmlFile(xmlFile)
    }

    protected open fun createUmsatzsteuervoranmeldungSteuerfall(data: UmsatzsteuerVoranmeldung, elsterSteuernummer: String,
                                                         herstellerID: String):
            UstaUmsatzsteuervoranmeldungCType {

        val umsatzsteuervoranmeldung = UstaUmsatzsteuervoranmeldungCType()

        umsatzsteuervoranmeldung.jahr = data.jahr.jahr
        umsatzsteuervoranmeldung.zeitraum = data.zeitraum.ziffer
        umsatzsteuervoranmeldung.steuernummer = elsterSteuernummer
        // pattern: ([0-9]{5})(\*([^\p{C}*]{0,85})\*([^\p{C}*]{0,85})\*([^\p{C}*]{0,85})\*([^\p{C}*]{0,85})\*([^\p{C}*]{0,85}))?
        // Kz 09 baut sich aus folgenden Informationen zusammen: HerstellerID*Name des Berater*Berufsbezeichnung*TelNr Berater/Vorwahl*TelNr Berater/Anschluss*Name des Mandanten
        umsatzsteuervoranmeldung.kz09 = herstellerID // Pflichtfeld

        if (data.isBerichtigteAnmeldung) {
            umsatzsteuervoranmeldung.kz10 = mapToElsterBoolean(data.isBerichtigteAnmeldung)
        }


        // 4 - Lieferungen und sonstige Leistungen (steuerpflichtige Umsätze)
        umsatzsteuervoranmeldung.kz81 = data.nettoEinnahmenZu19ProzentInEuroOhneCentBetrag.toString() // zum Steuersatz von 19 Prozent
        umsatzsteuervoranmeldung.kz86 = data.nettoEinnahmenZu7ProzentInEuroOhneCentBetrag.toString() // zum Steuersatz von 7 Prozent

        data.nettoEinnahmenZuAnderenProzentsätzenInEuroOhneCentBetrag?.let {
            umsatzsteuervoranmeldung.kz35 = it.toString()
        }
        data.umsatzsteuerDerNettoEinnahmenZuAnderenProzentsätzenInEuroUndCentBetrag?.let {
            umsatzsteuervoranmeldung.kz36 = createEricCurrencyString(it)
        }

        // 8 - Abziehbare Vorsteuerbeträge
        // Vorsteuerbeträge aus Rechnungen von anderen Unternehmern
        umsatzsteuervoranmeldung.kz66 = createEricCurrencyString(
            data.abziehbareVorsteuerbetraegeAusRechnungenVonAnderenUnternehmernEuroUndCentBetrag )

        // 9 - Andere Steuerbeträge, Sondervorauszahlung und Berechnung
        // Verbleibende Umsatzsteuer-Vorauszahlung beziehungsweise verbleibender Überschuss
        // Pflichtfeld
        umsatzsteuervoranmeldung.kz83 = createEricCurrencyString(
            data.verbleibendeUmsatzsteuervorauszahlungEuroUndCentBetrag)

        return umsatzsteuervoranmeldung
    }

    protected open fun mapSteuerpflichtigerToDatenlieferant(steuerpflichtiger: Steuerpflichtiger): UstaDatenlieferantCType {
        val datenlieferant = UstaDatenlieferantCType()

        datenlieferant.name = steuerpflichtiger.name
        datenlieferant.strasse = steuerpflichtiger.strasse + " " + steuerpflichtiger.hausnummer
        datenlieferant.plz = steuerpflichtiger.plz
        datenlieferant.ort = steuerpflichtiger.ort
        datenlieferant.telefon = steuerpflichtiger.telefon
        datenlieferant.email = steuerpflichtiger.email

        return datenlieferant
    }

    protected open fun mapSteuerpflichtigerToUnternehmer(steuerpflichtiger: Steuerpflichtiger): AdresseCType {
        val unternehmer = AdresseCType()

        unternehmer.bezeichnung = steuerpflichtiger.unternehmensname
        unternehmer.vorname = steuerpflichtiger.vorname
        unternehmer.name = steuerpflichtiger.nachname
        unternehmer.str = steuerpflichtiger.strasse
        unternehmer.hausnummer = steuerpflichtiger.hausnummer
        unternehmer.plz = steuerpflichtiger.plz
        unternehmer.ort = steuerpflichtiger.ort
        unternehmer.telefon = steuerpflichtiger.telefon
        unternehmer.email = steuerpflichtiger.email
        unternehmer.land = steuerpflichtiger.land

        return unternehmer
    }

    protected open fun mapToElsterBoolean(boolean: Boolean): String {
        return if (boolean) "1" else "0"
    }

}