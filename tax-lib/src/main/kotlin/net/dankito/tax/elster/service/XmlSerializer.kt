package net.dankito.tax.elster.service

import java.io.StringReader
import java.io.StringWriter
import javax.xml.bind.JAXBContext


open class XmlSerializer {

    companion object {

        const val ElsterEncoding = "UTF-8"

    }


    open fun serialize(objectToSerialize: Any): String {
        val jaxbContext = JAXBContext.newInstance(objectToSerialize.javaClass)
        val marshaller = jaxbContext.createMarshaller()
        val stringWriter = StringWriter()

        marshaller.setProperty("jaxb.encoding", ElsterEncoding)

        marshaller.setProperty("jaxb.formatted.output", true) // to make it better human readable

        marshaller.marshal(objectToSerialize, stringWriter)

        return stringWriter.toString()
    }


    open fun <T> deserialize(xml: String, objectClass: Class<T>): T {
        val jaxbContext = JAXBContext.newInstance(objectClass)
        val unmarshaller = jaxbContext.createUnmarshaller()
        val stringReader = StringReader(xml)

        return unmarshaller.unmarshal(stringReader) as T
    }


}