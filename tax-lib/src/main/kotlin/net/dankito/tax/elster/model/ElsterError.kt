package net.dankito.tax.elster.model

import de.elster.model.bearbeitevorgang.HinweisOderFehlerBase


class ElsterError(
    val errorCode: Int,
    val message: String,
    val hintsAndErrors: List<HinweisOderFehlerBase> = listOf(),
    val serverResponse: String? = null
) {

    override fun toString(): String {
        return "($errorCode) $message"
    }

}