package net.dankito.tax.elster.model


open class ElsterTransactionResult(val successful: Boolean,
                                   val errors: List<ElsterError> = listOf(),
                                   val userInfo: List<String> = listOf()) {


    constructor(successful: Boolean, error: ElsterError): this(successful, listOf(error))


    override fun toString(): String {
        return "successful? $successful, ${errors.size} errors, ${userInfo.size} user info"
    }

}