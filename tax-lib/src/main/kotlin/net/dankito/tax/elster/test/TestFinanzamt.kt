package net.dankito.tax.elster.test

import net.dankito.tax.elster.model.Finanzamt


enum class TestFinanzamt(val finanzamt: Finanzamt) {

    Bayern_9198(Finanzamt("Testfinanzamt OF-Bereich München", 9198))

}