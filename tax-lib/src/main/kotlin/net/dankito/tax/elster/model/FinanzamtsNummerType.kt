package net.dankito.tax.elster.model


/**
 *  Aufbau einer Steuernummer
 *
 *  In der unteren Tabelle wird die Darstellung einer Steuernummer in dem Standardschema der Länder und in dem dazugehörigen vereinheitlichten Bundesschema gezeigt. Mit Hilfe der Tabelle kann eine Steuernummer in das gewünschte Format umgewandelt werden. Bei einer Steuernummer aus dem Land Hessen ist darauf zu achten, dass nach dem Standardschema der Länder die führende "0" nicht vergessen wird.
 *  Bei der Darstellung eines Schemas werden folgende Bezeichnungen verwendet: F = Ziffer der Finanzamtsnummer, B= Ziffer der Bezirksnummer, U = Ziffer aus der persönlichen Unterscheidungsnummer, P = Prüfziffer
 *  Bundesland 	Standardschema der Länder	Vereinheitlichtes Bundesschema
 *
 *  Baden-Württemberg	FF/BBB/UUUUP	28FF0BBBUUUUP
 *  Bayern	FFF/BBB/UUUUP	9FFF0BBBUUUUP
 *  Berlin	FF/BBB/UUUUP	11FF0BBBUUUUP
 *  Brandenburg	FFF/BBB/UUUUP	3FFF0BBBUUUUP
 *  Bremen	FF/BBB/UUUUP	24FF0BBBUUUUP
 *  Hamburg	FF/BBB/UUUUP	22FF0BBBUUUUP
 *  Hessen	0FF/BBB/UUUUP	26FF0BBBUUUUP
 *  Mecklenburg-Vorpommern	FFF/BBB/UUUUP	4FFF0BBBUUUUP
 *  Niedersachsen	FF/BBB/UUUUP	23FF0BBBUUUUP
 *  Nordrhein-Westfalen	FFF/BBBB/UUUP	5FFF0BBBBUUUP
 *  Rheinland-Pfalz	FF/BBB/UUUUP	27FF0BBBUUUUP
 *  Saarland	FFF/BBB/UUUUP	1FFF0BBBUUUUP
 *  Sachsen	FFF/BBB/UUUUP	3FFF0BBBUUUUP
 *  Sachsen-Anhalt	FFF/BBB/UUUUP	3FFF0BBBUUUUP
 *  Schleswig-Holstein	FF/BBB/UUUUP	21FF0BBBUUUUP
 *  Thüringen	FFF/BBB/UUUUP	4FFF0BBBUUUUP
 *
 *  (https://www.elster.de/eportal/helpGlobal?themaGlobal=wo_ist_meine_steuernummer)
 */
enum class FinanzamtsNummerType {

    TwoDigits,

    TwoDigitsWithALeadingZero,

    ThreeDigits;


    companion object {

        val FederalStatesWhoseTaxNumbersStartsWithTwoDigits = listOf(
            "Baden-Württemberg", "Berlin", "Bremen", "Hamburg",
            "Niedersachsen", "Rheinland-Pfalz", "Schleswig-Holstein"
        )

        val FederalStatesWhoseTaxNumbersStartsWithZeroFollowedByTwoDigits = listOf("Hessen")

        val FederalStatesWhoseTaxNumbersStartsWithThreeDigits = listOf(
            "Bayern", "Brandenburg", "Mecklenburg-Vorpommern", "Nordrhein-Westfalen",
            "Saarland", "Sachsen", "Sachsen-Anhalt", "Thüringen",
            // unnatural Finanzamt Laender
            "Nordrhein-Westfalen (51)", "Nordrhein-Westfalen (52)", "Nordrhein-Westfalen (53)",
            "Bayern (Zuständigkeit LfSt - München)", "Bayern (Zuständigkeit LfSt - Nürnberg)"
        )

    }

}