package net.dankito.tax.elster.model

import net.dankito.tax.elster.test.TestMerker


class TransferHeader(val verfahren: VerfahrenSType,
                     val datenart: Datenart,
                     val vorgang: VorgangSType,
                     val herstellerID: String,
                     val datenLieferant: String,
                     val verschluesselung: VerschluesselungSType,
                     val kompression: KompressionSType,
                     val transportSchluessel: String = "",
                     val versionClient: String? = null,
                     val testMerker: TestMerker? = null
)