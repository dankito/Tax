package net.dankito.tax.elster.model

import java.beans.Transient


class Steuerpflichtiger(
    /**
     * May only be set for companies. Mandatory for companies.
     */
    val unternehmensname: String?,
    /**
     * May only be set for natural persons. Mandatory for natural persons.
     */
    val vorname: String?,
    /**
     * May only be set for natural persons. Mandatory for natural persons.
     */
    val nachname: String?,

    val strasse: String,
    val hausnummer: String,
    val plz: String,
    val ort: String,

    val telefon: String? = null,
    val email: String? = null,

    /**
     * Die Angabe des Landes bei der Anschrift des Unternehmers ist nur bei Auslandsanschriften erlaubt.
     * Bei Auslandsanschriften ist darüber hinaus stets auch die Auslandspostleitzahl und der Ort anzugeben.
     */
    val land: String? = null
) {

    private constructor() : this(null, "", "", "", "", "", "") // for object deserializers


    val name: String
        @Transient get() {
            return unternehmensname ?: "$vorname $nachname"
        }

}