package net.dankito.tax.elster

import de.elster.eric.wrapper.Eric


open class EricSettings(protected val eric: Eric) {

    /**
     * Voreinstellungswert: „nein“
    Der Wert ist global für alle ERiC-Instanzen gültig.
    Beschreibung:
    „nein“: Es wird bis Log-Level = „Info“ protokolliert und die
    ERiC Protokolldatei ist rollierend, siehe auch Kap. 4.1.3
    und API-Referenz eric_types.h.
    „ja“: Es wird bis Log-Level = „Trace“ protokolliert. Das
    ERiC Protokoll kann sehr groß werden. Das Rollieren der
    ERiC Protokolldatei ist abgeschalten, siehe auch Kap.
    4.1.3 und API-Referenz eric_types.h.
     */
    open fun setLogLevelToDetailed() {
        setLogLevel(true)
    }

    /**
     * Voreinstellungswert: „nein“
    Der Wert ist global für alle ERiC-Instanzen gültig.
    Beschreibung:
    „nein“: Es wird bis Log-Level = „Info“ protokolliert und die
    ERiC Protokolldatei ist rollierend, siehe auch Kap. 4.1.3
    und API-Referenz eric_types.h.
    „ja“: Es wird bis Log-Level = „Trace“ protokolliert. Das
    ERiC Protokoll kann sehr groß werden. Das Rollieren der
    ERiC Protokolldatei ist abgeschalten, siehe auch Kap.
    4.1.3 und API-Referenz eric_types.h.
     */
    open fun setLogLevelToNormal() {
        setLogLevel(false)
    }

    protected open fun setLogLevel(detailed: Boolean) {
        eric.setSetting("log.detailed", getBooleanSettingsValue(detailed))
    }


    /**
     * Voreinstellungswert: „nein“
    Beschreibung:
    Eine erfolgreiche Prüfung einer gültigen Test-
    Steueridentifikationsnummern mit der API-Funktion
    EricPruefeIdentifikationsMerkmal() ist nur mit der
    Einstellung basis.test_id_erlaubt=„ja“ möglich, siehe Kap.
    6.5.2
     */
    open fun setAllowTestId(allow: Boolean) {
        eric.setSetting("basis.test_id_erlaubt", getBooleanSettingsValue(allow))
    }


    /**
     * Voreinstellungswert: „30“
    Erlaubter Wertebereich: 0 – (231-1)
    Beschreibung:
    Der ganzzahlige Wert gibt die max.
    Verbindungswartezeit in Sekunden an.
    Der Wert 0 bedeutet "keine Beschränkung".
     */
    open fun setConnectionTimeoutInSeconds(seconds: Int) {
        eric.setSetting("transfer.connect_timeout", seconds.toString())
    }

    /**
     * Voreinstellungswert: „0“
    Erlaubter Wertebereich: 0 – (231-1)
    Beschreibung:
    Der ganzzahlige Wert gibt die max. Antwortwartezeit in
    Sekunden an.
    Der Wert 0 bedeutet "keine Beschränkung".
     */
    open fun setResponseTimeoutInSeconds(seconds: Int) {
        eric.setSetting("transfer.response_timeout", seconds.toString())
    }


    /**
     * Voreinstellungswert: „20“
    Erlaubter Wertebereich: 1 - 1000
    Beschreibung:
    Im Rückgabepuffer wird die Anzahl der Fehlermeldungen
    durch die Einstellung
    „validieren.fehler_max“ begrenzt. Weitere
    Fehlermeldungen werden abgeschnitten.
     */
    open fun setMaxErrors(maxErrors: Int) {
        eric.setSetting("validieren.fehler_max", maxErrors.toString())
    }

    /**
     * Voreinstellungswert: „20“
    Erlaubter Wertebereich: 1 - 1000
    Beschreibung:
    Im Rückgabepuffer wird die Anzahl der
    Hinweismeldungen durch die Einstellung
    „validieren.hinweise_max“ begrenzt. Weitere
    Hinweismeldungen werden abgeschnitten.
     */
    open fun setMaxNotifications(maxErrors: Int) {
        eric.setSetting("validieren.hinweise_max", maxErrors.toString())
    }


    /**
     * Voreinstellungswert: „nein“
    Beschreibung:
    ERiC kommuniziert mit dem DOI-Netz, wenn der Wert
    „ja“ gesetzt ist. Detaillierte Informationen zum DOI-Netz
    siehe Kap. 5.3.11.
     */
    open fun setCommunicateWithDoiNet(allow: Boolean) {
        eric.setSetting("transfer.netz.doi", getBooleanSettingsValue(allow))
    }


    /**
     * (IP-)Adresse oder Hostname des Proxys
     */
    open fun setProxyHost(host: String) {
        eric.setSetting("http.proxy_host", host)
    }

    /**
     * Gültige Werte: 0 – 65535
    Proxy-Port muss angegeben werden, falls
    http.proxy_host gesetzt wurde, andernfalls schlägt der
    Versand mit dem Rückgabecode
    "ERIC_TRANSFER_ERR_PROXYPORT_INVALID" fehl.
     */
    open fun setProxyPort(port: Int) {
        eric.setSetting("http.proxy_port", port.toString())
    }

    /**
     * Benutzername für Proxy-Authentifizierung
     */
    open fun setProxyUsername(username: String) {
        eric.setSetting("http.proxy_username", username)
    }

    /**
     * Passwort für Proxy-Authentifizierung
     */
    open fun setProxyPassword(password: String) {
        eric.setSetting("http.password", password)
    }

    /**
     * Ist optional; Gültige Werte:
    Any
    Basic
    Digest
    NTLM
    DigestIE
    Standard: Any
    Mehrere Werte sind durch Komma getrennt anzugeben.
    Die Groß-, Kleinschreibung des Wertes wird ignoriert.
     */
    open fun setProxyAuthentication(authentication: String) { // TODO: create an enum for valid authentication values
        eric.setSetting("http.proxy_auth", authentication)
    }


    protected fun getBooleanSettingsValue(enable: Boolean) = if (enable) "ja" else "nein"

}