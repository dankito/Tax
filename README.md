# Download new ERiC version

Im [Elster Entwicklerbereich](https://www.elster.de/elsterweb/infoseite/entwickler) unter 'ELSTER Rich Client (ERiC)' herunterladen:
- ERiC-<version>-common.zip: .xsd Schemata, Vordrucke, Tutorial etc.
- ERiC-<version>-Linux-x86_64.jar: ERiC und plugins für Linux

# Generate classes from .xsd schema files

XJC is included in the bin directory in the JDK starting with Java SE 6.

Execute in project base folder:

```
$JAVA_HOME/bin/xjc -d tax-lib/src/main/java -p net.dankito.tax.elster.model -no-header <ERiC common folder>/Schnittstellenbeschreibungen/Anmeldungssteuern/UStVA_2020/Schema/elster11_UStA_202001_extern.xsd
```

-d \<dir>: generated files will go into this directory

-p \<package>: specifies the target package

-no-header: suppress generation of a file header with timestamp

# Update ERiC wrapper

Copy the files from ERiC-<version>-Linux-x86_64.jar/ERiC-<version>/Linux-x86_64/Beispiel/ericdemo-java/src/de/elster/eric/wrapper/ to tax-lib/src/main/java/de/elster/eric/wrapper .

Copy back these methods Eric.java:
- Eric.getFinanzamtLaender()
- Eric.getFinanzaemter(int)
- Eric.checkStNr(String)
- EricMakeElsterStnr(String steuernrBescheid, String bundesfinanzamtsnr)

Copy back this method to jna/EricapiLibrary.java:
- EricMakeElsterStnr(String steuernrBescheid, String landesnr, String bundesfinanzamtsnr, PointerByReference steuernrPuffer)

# Test certificates

Test certificates can be used for testing, but only if 'testMerker' is set in Elster XML [net.dankito.tax.elster.model.TransferHeader].
  
They can be downloaded from [Elster Entwicklerbereich](https://www.elster.de/elsterweb/infoseite/entwickler) -> Schnittstellenbeschreibungen-> Test-Zertifikate.